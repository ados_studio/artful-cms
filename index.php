<?php

$php_os = (strtoupper(substr(PHP_OS,0,3))==='WIN');
if(!defined('PATH_SEPARATOR')) define('PATH_SEPARATOR',($php_os)?';':':');
if(!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR',($php_os)?'\\':'/');
if(!defined('DIR_SEPARATOR')) define('DIR_SEPARATOR',DIRECTORY_SEPARATOR);
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
unset ($php_os);

require_once( dirname(__FILE__).DS.'system'.DS.'config.inc.php' );

Encore::setOption('localize',Encore::getOption('default_localize','ua'));
Encore::registerModule('init', 'InitFront', MODULES_PATH.'initfront'.DS.'initfront.class.php');

Encore::init();
