<?php

require_once( dirname(dirname(__FILE__)).'/encore/config.inc.php' );

define ('MODULES_PATH', dirname(__FILE__).DS.'modules'.DS);
define ('TEMPLATES_PATH', dirname(__FILE__).DS.'templates'.DS);
define ('SYSTEM_PATH', dirname(__FILE__).DS);
define ('CONFIGS_PATH', dirname(__FILE__).DS.'configs'.DS);

//*****************************************************************//

Encore::setOption('sitename','');
Encore::setOption('new_user_email_activation',TRUE);

Encore::setOption('NOT_FOUNT_PAGE','404');
Encore::setOption('ACCESS_FORBIDEN_PAGE','403');

//*****************************************************************//

Encore::setOption('default_email','');
//Encore::setOption('smtp_host','');
//Encore::setOption('smtp_port','');
//Encore::setOption('smtp_login','');
//Encore::setOption('smtp_pass','');
//Encore::setOption('smtp_timeout','');

//*****************************************************************//

Encore::setOption('log_level', 0); // 0 - Error, 1 - Warning, 2 - Debug
Encore::setOption('log_file', dirname(__FILE__).DS.'logs'.DS.'log.txt');

//*****************************************************************//

Encore::setOption('db_host', '');
Encore::setOption('db_user', '');
Encore::setOption('db_pass', '');
Encore::setOption('db_name', '');
Encore::setOption('db_prefix', '');
Encore::setOption('db_charset', 'utf8');

//*****************************************************************//

Encore::setOption('charset','utf-8');

Encore::setOption('languages', array('ua'));
Encore::setOption('default_localize','ua');
Encore::setOption('multilanguages',FALSE);
Encore::setOption('localize_path',SYSTEM_PATH.'localize'.DS);
Encore::setOption('localize_system','ua');
if(!Encore::existsOption('localize')) {
	Encore::setOption('localize','ua');
}

Encore::setOption('cache_path',SYSTEM_PATH.'cache'.DS);

Encore::setOption('PAGINATION_LIMIT',10);
Encore::setOption('PAGINATION_MARGIN',3);

