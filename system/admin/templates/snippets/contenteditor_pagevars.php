<?php
	if(!isset($args['id'])&& !isset($args['type'])) return ;
	$last_localize = Encore::getOption('localize');
	$localize = (isset($_POST['localize']) && !AString::from($_POST['localize'])->isEmpty()) ? Encore::getModule('DB')->real_escape_string($_POST['localize']) : Encore::getOption('default_localize');
	Encore::setOption('localize', $localize);
	$ret = '';
	$pm = Encore::getModule('PageManager');
	$id = $args['id'];
	$type = $args['type'];
	$cvars = $pm->getPageVars($id);
	$pagetype = $pm->getPageType($type);
	$schema = (array)Encore::fromJson($pagetype['schema']);

	foreach($schema as $varname => $varschema) {
		$vc = (array)$varschema;
		if(!isset($vc['type'])) continue;
		$options = array(
			'name' => 'PV_'.$varname,
			'value' => (isset($cvars[$varname])) ? $cvars[$varname] : null,
			'classname' => 'default',
			'events' => 'onchange="Forbiz.app.contenteditor.api.parseVars();"'
		);
		$options = array_merge($options,$vc);
		unset($options['type']);
	$ret .= '<label class="default">'.Encore::tr($varname,"admin").'</label>' . $this->getSnippet('inputs'.DS.$vc['type'],$options) . '<br/>';
	}

	Encore::setOption('localize',$last_localize);
	return $ret . '<input id="input_pagevars" type="hidden" value="'.htmlspecialchars(Encore::toJson($cvars)).'"/>';
?>