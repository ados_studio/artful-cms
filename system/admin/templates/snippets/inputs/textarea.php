<?php
	if(!isset($args['name'])) return;
	$value = (isset($args['value'])) ? $args['value']: '';
	$class = (isset($args['classname'])) ? ' class="'.$args['classname'].'" ' : '';
	$events = (isset($args['events'])) ? $args['events'] : '';
	
	return '<div id="ct_'.$args['name'].'"><textarea id="input_'.$args['name'].'" name="'.$args['name'].'" '.$class.' '.$events.'>'.htmlspecialchars($value).'</textarea></div>';

