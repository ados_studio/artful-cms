<?php	
	function selectsOfPage($pages, $value, $level = 0) {		
		$margin = AString::from('')->fill('+&nbsp;&nbsp;',$level);
		$ret = '';
		foreach($pages as $key => $page) {
			$title = ((empty($page['pagetitle'])) ? $page['alias']: $page['pagetitle'] ).' ['.$page['id'].']';
			$active = ($key==$value) ? ' selected ': ''; 
			$ret .= '<option value="'.$key.'" '.$active.'>'.$margin.$title.'</option>';
			if(isset($page['childs']) && count($page['childs'] > 0)) {
				$ret .= selectsOfPage($page['childs'],$value,$level+1);
			} 
		}
		return $ret;
	}
	$root = (isset($args['root'])) ? $args['root'] : 0 ;
	$class = (isset($args['classname'])) ? ' class="'.$args['classname'].'" ' : '';
	$events = (isset($args['events'])) ? $args['events'] : '';
	$value = (isset($args['value'])) ? $args['value']: '';
	
	
	$pm = Encore::getModule('PageManager');
	$pages = $pm->getPages($root);
	
	$options = selectsOfPage($pages, $value);
	
	return "<select id=\"input_{$args['name']}\" name=\"{$args['name']}\" {$class} {$events} >$options</select>";
?>




