<?php
	if(!isset($args['name'])) return;
	
	$value = (isset($args['value'])) ? $args['value']: '';
	$events = (isset($args['events'])) ? $args['events']: '';
	$class = (isset($args['classname'])) ? ' class="'.$args['classname'].'" ' : '';
	$values = (isset($args['values'])) ? $args['values']: array(); 

	$ret = '<select '.$class.' id="input_'.$args['name'].'" name="'.$args['name'].'" '.$events.'><option disabled>Виберіть значення</option>';
	if($values != null) {
		foreach($values as $var) {
			$selected = ($var == $value ) ? ' selected ': '';
			$ret .= '<option value="'.$var.'" '.$selected.' >'.Encore::tr($var,'frontend').'</option>';
		}
	}

	return $ret . '</select>';