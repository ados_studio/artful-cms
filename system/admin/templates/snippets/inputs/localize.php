<?php
if(!isset($args['name'])) return;

$value = (isset($args['value'])) ? $args['value']: Encore::getOption('default_localize');
$class = (isset($args['classname'])) ? ' class="'.$args['classname'].'" ' : '';
$events = (isset($args['events'])) ? $args['events'] : '';
$disabled = (isset($args['disabled'])&& $args['disabled']==true)?' disabled ': ' ';
$options = '';
$langs = Encore::getOption('languages');

foreach($langs as $lang) {
	$active = ($value==$lang)?' selected ':' ';
	$options .= '<option '.$active.' value="'.$lang.'">'.$lang.'</option>';
}
return '<select '.$disabled.' id="input_'.$args['name'].'" name="'.$args['name'].'" '.$class.' '.$events.' >'.$options.'</select>';
