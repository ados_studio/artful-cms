<?php
if(!isset($args['name'])) return;
		  $value = (isset($args['value'])) ? $args['value']: '';
$class = (isset($args['classname'])) ? ' class="'.$args['classname'].'" ' : '';
$events = (isset($args['events'])) ? $args['events'] : '';
return '<div id="ct_'.$args['name'].'" class="image_input"><input id="input_'.$args['name'].'" '.$class.' name="'.$args['name'].'" value="'.htmlspecialchars($value).'" '.$class.' '.$events.' /><img src="/admin/img/btn/openfile.png" onclick="window.parent.Forbiz.dialog.openFile( null,\''.Encore::tr('Open File', 'admin').'\',null,null, function() { Ext.get(\'input_'.$args['name'].'\').dom.value = window.parent.Forbiz.dialog.data.fileName; Ext.get(\'input_'.$args['name'].'\').dom.onchange.call(); } );" '.$events.'/></div>';
