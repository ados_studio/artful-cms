<?php
	if(!isset($args['name'])) return;
	Encore::requireModules('Pagetypes snippet', 'PageManager');
	$value = (isset($args['value'])) ? AString::from($args['value'])->replace('.art',''): '';
	$events = (isset($args['events'])) ? $args['events']: '';
	$pm = Encore::getModule('PageManager');
	$types = $pm->getPageTypes();

	$ret = '<select id="input_'.$args['name'].'" name="'.$args['name'].'"'.$value.' '.$events.'><option disabled>Виберіть тип</option>';
	if($types != null) {
		foreach($types as $ptype) {
			$selected = ($ptype['id'] == $value ) ? ' selected ': '';
			$ret .= '<option value="'.$ptype['id'].'" '.$selected.' >'.$ptype['name'].'</option>';
		}
	}

	return $ret . '</select>';
