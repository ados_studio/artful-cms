<?php
	if(!isset($args['name'])) return;
	$value = (isset($args['value'])) ? $args['value']: '';
	$class = (isset($args['classname'])) ? ' class="'.$args['classname'].'" ' : '';
	$events = (isset($args['events'])) ? $args['events'] : '';
	return '<input id="input_'.$args['name'].'" name="'.$args['name'].'" value="'.htmlspecialchars($value).'" '.$class.' '.$events.' />';
