<?php
	$id = $this->getPlaceholder('id');
	$title_key = 'ChangePassword' ;
?>
<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.savePassword();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="useradmin.api.pageUsers();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
	<h1><?php echo(Encore::tr($title_key,'admin'));?></h1>
</div>
<form id="userform" class="default">
	<input id="userid" name ="userid" type="hidden" value="<?php echo($id);?>" />
	<label class="default">Пароль</label><input id="userpassword" name="userpassword" class="default" type="password"/> <br/>
	<label class="default">Перевірка</label><input id="userpasswordprotect" name="userpasswordprotect" class="default" type="password"/> <br/>
	<label class="default">Повідомлення на E-Mail</label><input type="checkbox" id="emailmessage" name="emailmessage" class="default" checked/> <br/>
</form>
<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.savePassword();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="useradmin.api.pageUsers();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
</div>