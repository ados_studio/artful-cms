<?php
	$id = $this->getPlaceholder('id');
	$title_key = ($id == 0) ? 'CREATE_USER' : 'EDIT_USER' ;
?>

<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.saveUser();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="useradmin.api.pageUsers();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
	<h1><?php echo(Encore::tr($title_key,'admin'));?></h1>
</div>
<form id="userform" class="default">
	<input id="input_id" name ="id" type="hidden" value="<?php echo($id);?>" />
	<label class="default">Логін</label><input id="input_login" name="userlogin" class="default"  value="<?php echo($this->getPlaceholder('login'));?>"/> <br/>
	<label class="default">Ім'я</label><input id="input_name" name="username" class="default"  value="<?php echo($this->getPlaceholder('name'));?>"/> <br/>
	<label class="default">E-Mail</label><input id="input_email" name="useremail" class="default"  value="<?php echo($this->getPlaceholder('email'));?>"/> <br/>
	<div class="hr"></div>
	<label class="default">Членство в групах</label> <br/>
	<select class="default" id="input_groups" name="usergroups" multiple="" size="10" style="width:700px;"  onchange="useradmin.api.updateUserVars();">
		<?php echo($this->getPlaceholder('groups'));?>
	</select>
	<div class="hr"></div>
	<label class="default">Атрибути користувача</label> <br/>
	<div id="uservars_ct">
		<?php echo($this->getPlaceholder('user_vars'));?>
	</div>
	<div class="hr"></div>	
	<label class="default">Права доступу</label> <br/>
	<input type="button" value="додати доступ до магазину" onclick="medit.api.append('shopedit','/');" />
	<textarea id="user_privileges" name="input_privileges"><?php echo($this->getPlaceholder('user_privileges'));?></textarea>
	
</form>
<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.saveUser();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="useradmin.api.pageUsers();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
</div>