<?php
	$types = Encore::getModule('PageManager')->getPageTypes();	
	$toptions = '';
	$active = ' selected ';
	foreach($types as $type) {
		$toptions .= '<option '.$active.'value='.$type['id'].'>'.$type['name'].' ['.$type['id'].'] - '.$type['template'].'.php</option>';
		$active = '';
	}
?>
<div id="right_content">
	<div id="action_title">	
		<div id="action_zone">
			<div class="btn" onclick="Forbiz.app.contenteditor.api.createType();"><?php echo(Encore::tr('Create pagetype','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.editType();"><?php echo(Encore::tr('Edit','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.deleteType();"><?php echo(Encore::tr('Delete','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
		</div>
		<h1><?php echo(Encore::tr('PAGE_TYPES_LIST','admin'));?></h1>
	</div>
	<form class="default">
		<select id="pagetypes" size="20" style="width: 90%;">
			<?php echo($toptions);?>
		</select>	
	</form>
	
	<div id="action_title">	
		<div id="action_zone">
			<div class="btn" onclick="Forbiz.app.contenteditor.api.createType();"><?php echo(Encore::tr('Create pagetype','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.editType();"><?php echo(Encore::tr('Edit','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.deleteType();"><?php echo(Encore::tr('Delete','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
		</div>		
	</div>
</div>