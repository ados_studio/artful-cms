<?php
	$id = $this->getPlaceholder('id');
	$title_key = ($id == 0) ? 'CREATE_GROUP' : 'EDIT_GROUP' ;
?>

<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.saveGroup();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="useradmin.api.pageGroups();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
	<h1><?php echo(Encore::tr($title_key,'admin'));?></h1>
</div>
<form id="" class="default">
	<input id="input_id" name ="id" type="hidden" value="<?php echo($id);?>" />
	<label class="default">Назва групи</label><input id="input_groupname" name="groupname" class="default"  value="<?php echo($this->getPlaceholder('name'));?>"/> <br/>
	<label class="default">Схема</label> <textarea id="schema" class="contenteditor"><?php echo($this->getPlaceholder('schema'));?></textarea>
	<label class="default">Привілеї</label> <textarea id="privileges" class="contenteditor"><?php echo($this->getPlaceholder('privileges'));?></textarea>
</form>

<div id="bottom_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.saveGroup();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="useradmin.api.pageGroups();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
</div>