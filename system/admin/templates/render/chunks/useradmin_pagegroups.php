<?php
	$um = Encore::getModule('UserManager');
	$groups = '';
	$groupslist = $um->getGroupsList();
	$active = ' selected ';
	foreach( $groupslist as $group) {
	    $groups .= '<option '.$active.'value='.$group['id'].'>'.$group['name'].' ['.$group['id'].']</option>';
		$active = '';
	}
	
?>
<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.createGroup();"><?php echo(Encore::tr('Create','admin'));?></div>
		<div class="btn" onclick="useradmin.api.editGroup();"><?php echo(Encore::tr('Edit','admin'));?></div>
		<div class="btn" onclick="useradmin.api.deleteGroup();"><?php echo(Encore::tr('Delete','admin'));?></div>
		<div class="btn" onclick="useradmin.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
	<h1><?php echo(Encore::tr('EDIT_GROUPS','admin'));?></h1>
</div>
<form id="">
	<select id="pagetypes" size="30" style="width: 100%;">
		<?php echo($groups);?>
	</select>
</form>

<div id="bottom_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.createGroup();"><?php echo(Encore::tr('Create','admin'));?></div>
		<div class="btn" onclick="useradmin.api.editGroup();"><?php echo(Encore::tr('Edit','admin'));?></div>
		<div class="btn" onclick="useradmin.api.deleteGroup();"><?php echo(Encore::tr('Delete','admin'));?></div>
		<div class="btn" onclick="useradmin.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>	
</div>