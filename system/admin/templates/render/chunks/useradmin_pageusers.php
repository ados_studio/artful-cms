<?php
	$um = Encore::getModule('UserManager');
	$users = '';
	$userslist = $um->getUsersList();
	$active = ' selected ';
	foreach( $userslist as $user) {
	    $users .= '<option '.$active.'value='.$user['id'].'>'.$user['name'].' ['.$user['id'].']</option>';
		$active = '';
	}

?>
<div id="action_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.createUser();"><?php echo(Encore::tr('Create','admin'));?></div>
		<div class="btn" onclick="useradmin.api.editUser();"><?php echo(Encore::tr('Edit','admin'));?></div>
		<div class="btn" onclick="useradmin.api.changePassword();"><?php echo(Encore::tr('ChangePassword','admin'));?></div>
		<div class="btn" onclick="useradmin.api.deleteUser();"><?php echo(Encore::tr('Delete','admin'));?></div>
		<div class="btn" onclick="useradmin.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
	<h1><?php echo(Encore::tr('EDIT_USERS','admin'));?></h1>
</div>
<form id="">
	<select id="pagetypes" size="30" style="width: 100%;">
		<?php echo($users);?>
	</select>
</form>

<div id="bottom_title">
	<div class="action_zone">
		<div class="btn" onclick="useradmin.api.createUser();"><?php echo(Encore::tr('Create','admin'));?></div>
		<div class="btn" onclick="useradmin.api.editUser();"><?php echo(Encore::tr('Edit','admin'));?></div>
		<div class="btn" onclick="useradmin.api.changePassword();"><?php echo(Encore::tr('ChangePassword','admin'));?></div>
		<div class="btn" onclick="useradmin.api.deleteUser();"><?php echo(Encore::tr('Delete','admin'));?></div>
		<div class="btn" onclick="useradmin.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
</div>