<?php
$anons = $this->getSnippet('inputs'.DS.'htmleditor', array('name'=>'preview', 'value'=> $this->getPlaceholder('preview')));
	$content = $this->getSnippet('inputs'.DS.'htmleditor', array('name'=>'content', 'classname'=>'contenteditor', 'value'=> $this->getPlaceholder('content') ));
	$typeinput = $this->getSnippet('inputs'.DS.'pagetypes', array('name'=>'type', 'value'=> $this->getPlaceholder('type'), 'events'=>'onchange="Forbiz.app.contenteditor.api.updateVars();"'));
	$pagevars = $this->getSnippet('contenteditor_pagevars', array('id'=>$this->getPlaceholder('id'),'type'=>$this->getPlaceholder('type')));
?>
<div id="right_content">
<div id="action_title">
	<div id="action_zone">
		<div class="btn" onclick="Forbiz.app.contenteditor.api.savePage();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="Forbiz.app.contenteditor.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
	<h1><?php echo(Encore::tr('EDIT_PAGE','admin'));?></h1>
</div>
<form id="editor_form" class="default">
	<input id="input_id" name ="id" type="hidden" value="<?php echo($this->getPlaceholder('id'));?>" />
	<label class="default">Мова сторінки</label><?php echo($this->getSnippet('inputs'.DS.'localize', array('name'=>'localize','disabled'=>(((integer)$this->getPlaceholder('id'))==0),'value'=>$this->getPlaceholder('localize',''),'events'=>' onchange="Forbiz.app.contenteditor.api.switchLocalize();"'))); ?><br/>
	<label class="default">Директорія сторінки</label><input readonly="readonly" id="input_parent" name="parent" class="default" value="<?php echo($this->getPlaceholder('parent'));?>" /> <br/>
	<label class="default">Псевдонім</label><input id="input_alias" name="alias" class="default"  value="<?php echo($this->getPlaceholder('alias'));?>"/> <br/>
	<label class="default">Індекс сортування</label><input id="input_sort" name="sort" class="default"  value="<?php echo($this->getPlaceholder('sort'));?>"/> <br/>
	<div class="hr"></div>
	<label class="default">Опублікований</label><input id="input_published" type="checkbox" name="published" class="default"  <?php echo($this->getPlaceholder('published'));?>/> <br/>
	<label class="default">Каталог</label><input id="input_folder" type="checkbox" name="folder" class="default" <?php echo($this->getPlaceholder('folder'));?>/> <br/>
	<label class="default">Контроль доступа</label><input id="input_accessable" type="checkbox" name="accessable" class="default" <?php echo($this->getPlaceholder('accessable'));?>/> <br/>
	<label class="default">Підтримка Шаблонізації</label><input id="input_executable" type="checkbox" name="executable" class="default" <?php echo($this->getPlaceholder('executable'));?>/> <br/>
	<div class="hr"></div>
	<label class="default">Назва сторінки</label><input id="input_pagetitle" name="pagetitle" class="default"  value="<?php echo(htmlspecialchars($this->getPlaceholder('pagetitle')));?>"/> <br/>
	<label class="default">Розширений заголовок</label><input id="input_longtitle" name="longtitle" class="default" value="<?php echo(htmlspecialchars($this->getPlaceholder('longtitle')));?>"/> <br/>
	<label class="default">Поле TITLE</label><input id="input_seotitle" name="seotitle" class="default" value="<?php echo(htmlspecialchars($this->getPlaceholder('seotitle')));?>"/> <br/>
	<label class="default">Поле DESCRIPTION</label><input id="input_description" name="description" class="default" value="<?php echo(htmlspecialchars($this->getPlaceholder('description')));?>"/> <br/>
	<label class="default">Поле KEYWORDS</label><input id="input_keys" name="keys" class="default"  value="<?php echo(htmlspecialchars($this->getPlaceholder('keys')));?>"/> <br/>
	<label class="default">Тип сторінки</label><?php echo($typeinput);?> <br/>
	<div class="hr"></div>
	<div id="ctpagevars">
		<?php echo($pagevars);?>
	</div>
	<div class="hr"></div>
	<label class="default" onblur="">Анонс</label><br/>
	<?php echo($anons)?>
	<label class="default">Контент</label><br/>
	<?php echo($content)?>

</form>
<div id="action_bottom">
	<div id="action_zone">
		<div class="btn" onclick="Forbiz.app.contenteditor.api.savePage();"><?php echo(Encore::tr('Save','admin'));?></div>
		<div class="btn" onclick="Forbiz.app.contenteditor.api.welcomePage();"><?php echo(Encore::tr('Cancel','admin'));?></div>
	</div>
</div>
</div>