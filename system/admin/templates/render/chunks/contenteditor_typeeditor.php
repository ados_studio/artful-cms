<?php
	$tplist = array();

	$dir = SYSTEM_PATH.'templates'.DS.'src'.DS;
	if ($dh = opendir($dir)) {
    	while (($file = readdir($dh)) !== false) {
      		if ($file !== '.' AND $file !== '..') {
           		$current_file = $dir.DS.$file;
				if (is_file($current_file) && AString::from($file)->right(4) == '.art') {
              		$tplist[] = AString::from($file);
           		}
       		}
    	}
   	}
	$tpopt = '';
	$template = AString::from($this->getPlaceholder('template'))->append('.art');
	foreach ($tplist as $tpitem) {
		$active = ($tpitem==$template) ? ' selected ': '';
		$tpopt .= '<option value="'.AString::from($tpitem)->replace('.art','').'" '.$active.'>'.$tpitem.'</option>';
	}

?>

<div id="right_content">
	<div id="action_title">
		<div id="action_zone">
			<div class="btn" onclick="Forbiz.app.contenteditor.api.saveType();"><?php echo(Encore::tr('Save','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.pageTypes();"><?php echo(Encore::tr('Cancel','admin'));?></div>
		</div>
		<h1><?php echo(Encore::tr('CREATE_PAGETYPE','admin'));?></h1>
	</div>
	<form class="default">
		<input id="input_id" type="hidden" value="<?php echo($this->getPlaceholder('id'));?>"/>
		<label class="default">Ім'я:</label> <input id="name" class="default"  value="<?php echo($this->getPlaceholder('name'));?>"/> <br/>
		<label class="default">Шаблон:</label> <select id="template" class="default"><?php echo($tpopt);?></select> <br/>
		<label class="default">Схема</label> <textarea id="schema" class="contenteditor"><?php echo($this->getPlaceholder('schema'));?></textarea>
	</form>
	<div id="action_title">
		<div id="action_zone">
			<div class="btn" onclick="Forbiz.app.contenteditor.api.saveType();"><?php echo(Encore::tr('Save','admin'));?></div>
			<div class="btn" onclick="Forbiz.app.contenteditor.api.pageTypes();"><?php echo(Encore::tr('Cancel','admin'));?></div>
		</div>
	</div>
</div>