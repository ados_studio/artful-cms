<?php
	$render = Encore::getModule('Render');
	$render->setPlaceholder('chunk_welcome',$render->getChunk('contenteditor_welcome')); 
?>

<!DOCTYPE html> 
<html lang="ua"> 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">     
    <title>Content Editor | Forbiz CMS</title>
    
    <?php echo(Encore::getModule('Render')->getHtmlHeadRecords());?>
    
    <link rel="stylesheet" href="/admin/ext/resources/css/ext-all.css" type="text/css" />  
    <link rel="stylesheet" href="/admin/ext/resources/css/xtheme-gray.css" type="text/css" />    
    <link rel="stylesheet" href="/admin/applications.css" type="text/css" />       
    <link rel="stylesheet" href="/admin/applications/forbizfilemanager/style.css" type="text/css" />
         
    <script type="text/javascript" src="/admin/ext/adapter/ext/ext-base.js"></script>    
    <script type="text/javascript" src="/admin/ext/ext-all.js"></script> 
    
  	<script type="text/javascript" src="/admin/applications/forbizfilemanager/forbizfilemanager.js"></script>
</head>
<body>
	<div id="viewport">
		
	</div>
</body>
</html>
