<?php

Class Application extends EncoreModule {
	abstract public function init();
	
	public function show403() {
		require_once (ADMIN_TEMPLATES_PATH.'render'.DS.'403.php');
	}
	
	public function show404() {
		require_once (ADMIN_TEMPLATES_PATH.'render'.DS.'404.php');
	}
}