<?php

class ForbizFileManager extends EncoreModule {
	
	public function init () {
		Encore::requireModules('FileManager App', 'FileManager,Mandator');
		Encore::initDictionary('admin');
		
		Encore::sessionStart();
		
		
		
		$mandator = Encore::getModule('Mandator');
		$mandator->init();
		if($mandator->hasPermission('application','/filemanager/')){
			if(isset($_POST['action'])) {
				
			} else {
				$render = Encore::getModule('Render');
				$pm = Encore::getModule('PageManager');
				$render->init();
				$render->renderPage(array(
					'template' => 'forbizfilemanager',
					'vars' => array()
				));
			}
			
		} else {
			$render = Encore::getModule('Render');
			$render->init();
			$render->renderPage(array(
				'template' => '403',
				'vars' => array()
			));
		}		
	}
	
	protected function t() {
		
	}
	
	
}
