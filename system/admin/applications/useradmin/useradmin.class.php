<?php

class FAccess extends EncoreModule {
	
	public function init() {
		Encore::requireModules('FAccess','Mandator,DB');
		Encore::initDictionary('admin');

		Encore::sessionStart();

		$mandator = Encore::getModule('Mandator');
		$usermanager = Encore::getModule('UserManager');
		$mandator->init();
		$render = Encore::getModule('Render');
		$render->init();

		if ( $mandator->hasPermission('application','/useradmin/') ) {
			if ( isset($_POST['action']) && !empty($_POST['action']) ) {
				$ret = '{"success":false,"result":"Unknown action!"}';
				switch($_POST['action']) {
					case 'getpage':
						
						
						if(isset($_POST['page'])) {
							switch($_POST['page']) {
								case 'groupeditor':
									$id = (isset($_POST['id'])) ? $_POST['id'] : 0;
									if ( $id != 0 ) {
										$group = $usermanager->getGroupById($id);
										if($group != null) {
											$render->setPlaceholder('name', $group['name']);
											$render->setPlaceholder('schema', $group['schema']);
											$render->setPlaceholder('privileges', Encore::toJson($mandator->getGroupPermissions($id)));											
										}
									}					
									$render->setPlaceholder('id', $id);
									$ret = $render->getChunk('useradmin_pagegroupeditor');
									break;
								case 'usereditor':
									$id = (isset($_POST['id'])) ? $_POST['id'] : 0;
									$groups = $usermanager->getGroupsList();
									$activegroups = array();
									
									if ( $id != 0 ) {
										$user = $usermanager->getUser($id);

										
										if( $user != null ) {
											$render->setPlaceholder('login', $user['login']);
											$render->setPlaceholder('name', $user['name']);
											$render->setPlaceholder('email', $user['email']);
											$activegroups = $usermanager->getUserGroups($id);
										}
										$render->setPlaceholder('user_privileges', Encore::toJson($mandator->getUserPermissions($id)));
										$render->setPlaceholder('user_vars',$this->getUserVarsForm($id, $activegroups));
									}
									$render->setPlaceholder('id',$id);
									$render->setPlaceholder('groups', Encore::makeSelectList( $groups,'id','name', $activegroups));
									
									$ret = $render->getChunk('useradmin_pageusereditor');
									break;
								case 'groupslist':
									$ret = $render->getChunk('useradmin_pagegroups');
									break;
								case 'welcomepage':
									$ret = $render->getChunk('useradmin_welcome');
									break;
								case 'userslist' :
									$ret = $render->getChunk('useradmin_pageusers');
									break;
								case 'password' :
									if(isset($_POST['id'])) {
										$render->setPlaceholder('id', $_POST['id']);
										$ret = $render->getChunk('useradmin_pagepassword');
									} else {
										$ret = '{"success":false,"result":"User id not found!"}';
									}
									break;
								default:
									$ret = '{"success":false,"result":"Page not found!"}';
							}
						} else {
							$ret = '{"success":false,"result":"Page argument not found!"}';
						}
						break;
					case 'savegroup':
						$ret = $this->saveGroup();
						break;
					case 'deletegroup':
						$ret = $this->deleteGroup();
						break;
					case 'deleteuser':
						$ret = $this->deleteUser();
						break;
					case 'saveuser':
						$ret = $this->saveUser();
						break;
					case 'savepassword':
						$ret = $this->savePassword();
						break;
					case 'getuservarsform':
						$ret = $this->getUserVarsForm($_POST['id'], (array)Encore::fromJson($_POST['groups']));
						break;
					default:
						
				}
				echo($ret);
			} else {
				$render->renderPage(array(
					'template' => 'useradmin',
					'vars' => array()
				));
			}

		} else {
			$render = Encore::getModule('Render');
			$render->init();
			$render->renderPage(array(
				'template' => '403',
				'vars' => array()
				));				
		}
	}

	protected function saveGroup() {
		$ret = '{"success":false,"result":"Group data isn\'t complite"}';
		if( Encore::isSetKeys($_POST, array('id','name','schema','privileges')) ) {
			$group = array(
				'name' => $_POST['name'],
				'schema'    => $_POST['schema']
			);
			$um = Encore::getModule('UserManager');
			$mandator = Encore::getModule('Mandator');
			if($_POST['id'] == 0 ) {
				$gid = $um->createGroup($group);
				if ( $gid != null ) {					
					if ( $mandator->updateGroupPermissions( $gid, (array)Encore::fromJSON($_POST['privileges']) ) ) {
						$ret = '{"success":true}';
					} else {
						$ret = '{"success":false,"result":"Ca\'nt update group privileges!"}';
					}
				} else {
					$ret = '{"success":false,"result":"Ca\'nt create group!"}';
				}
			} else {
				if ( $um->updateGroup($group, $_POST['id']) ) {
					if ( $mandator->updateGroupPermissions( $_POST['id'], (array)Encore::fromJSON($_POST['privileges']) ) ) {
						$ret = '{"success":true}';
					} else {
						$ret = '{"success":false,"result":"Ca\'nt update group privileges!"}';
					}
				} else {
					$ret = '{"success":false,"result":"Ca\'nt update group!"}';
				}
			}
		}
		return $ret;
	}

	protected function deleteGroup() {
		$ret = '{"success":false,"result":"Group id not found!"}';
		if(isset($_POST['id'])) {
			$um = Encore::getModule('UserManager');
			if ( $um->deleteGroup($_POST['id']) ) {
				$ret = '{"success":true}';
			} else {
				$ret = '{"success":false,"result":"UserManager can\'t remove group!"}';
			}
		}
		return $ret;
	}


	protected function saveUser() {
		$ret = '{"success":false,"result":"User data isn\'t complite"}';
		if( Encore::isSetKeys($_POST, array('id','name','login','privileges','groups','vars')) && Validator::is_login($_POST['login']) && Validator::is_email($_POST['email']) ) {
			$ret = array( "success" => true, "result" => "Unknown error!" );
			$um = Encore::getModule('UserManager');
			if ($_POST['id'] == 0 ) {
				$test = $um->getUserByLogin($_POST['login']);
				if($test != null) {
					$ret['success'] = false;
					$ret['result'] = 'Логін вже зайнятий';
				}

				$test = $um->getUserByEmail($_POST['email']);
				if($test != null) {
					$ret['success'] = false;
					$ret['result'] = 'Емейл вже зайнятий';
				}
			}

			if($ret['success'] == true) {
				$ret['success'] = false;
				$mandator = Encore::getModule('Mandator');
				$usergroups = (array)Encore::fromJson($_POST['groups']);
				$privileges_obj = (array)Encore::fromJson($_POST['privileges']);
				$privileges = array();
				foreach($privileges_obj as $pr) {
				    $privileges[] = (array)$pr;
				}
				
				$vars = (array)Encore::fromJson($_POST['vars']);
				$id = (integer)$_POST['id'];
				$password = false;
				$user = array(
					'name' => $_POST['name'],
					'login' => $_POST['login'],
					'email'=> $_POST['email'],
					'active'=>true
				);
				if($id == 0 ){
					$password = Encore::generateUniqueKey(8);
					$user['password'] = $password;
					$ret['result'] = $password;
					if($um->createUser($user)) {
						$id = Encore::getModule('DB')->last_id();
						if($um->updateUserGroups($id,$usergroups)) {
							if($um->updateUserAttributes($id,$vars)) {
								if($mandator->updateUserPermissions($id,$privileges)) {
									$ret['success'] = true;								
								} else {
									$ret['result'] = 'Error on update permissions!';
								}
							} else {
								$ret['result'] = 'Error on update user attributes!';
							}
						} else {
							$ret['result'] = 'Error on update user groups!';
						}
					} else {
						$ret['result'] = 'Error on create user!';
					}
				} else {
					$user['id'] = $id;
					if($um->updateUser($user)) {
						if($um->updateUserGroups($id,$usergroups)) {
							if($um->updateUserAttributes($id,$vars)) {
								if($mandator->updateUserPermissions($id,$privileges)) {
									$ret['success'] = true;
								} else {
									$ret['result'] = 'Error on update permissions!';
								}
							} else {
								$ret['result'] = 'Error on update user attributes!';
							}
						} else {
							$ret['result'] = 'Error on update user groups!';
						}
					} else {
						$ret['result'] = 'Error on update user!';
					}
				}
			}
			$ret = Encore::toJson($ret);
		}
		return $ret;
	}

	protected function deleteUser() {
		$ret = '{"success":false,"result":"Group id not found!"}';
		if(isset($_POST['id']) && $_POST['id'] > 3 ) {
			$um = Encore::getModule('UserManager');
			if ( $um->deleteUser($_POST['id']) ) {
				$ret = '{"success":true}';
			} else {
				$ret = '{"success":false,"result":"UserManager can\'t remove group!"}';
			}
		}
		return $ret;
	}

	protected function savePassword() {
		Encore::initDictionary('email');
		$ret = '{"success":false,"result":"Can\'t update password!"}';
		$um = Encore::getModule("UserManager");
		if(isset($_POST['userid']) && isset($_POST['userpassword'])) {
			if($um->updatePassword($_POST['userid'], $_POST['userpassword'])) {
				$ret = '{"success":true}';
				if($ret == true && isset($_POST['useremail'])) {
					$messenger = Encore::getModule('Messenger');
					$messenger->mailToUser($_POST['userid'],Encore::tr('PASSWORD_UPDATE_TITLE','email'), AString::from(Encore::tr('PASSWORD_UPDATE','email'))->arg($_POST['userpassword']));
				}
			}
		}
		return $ret;
	}

	protected function getUserVarsForm($id,$groups) {
		if ( !(is_array($groups) && count($groups) > 0) ) return '';
		$db = Encore::getModule('DB');
		$um = Encore::getModule('UserManager');
		$ret = '';
		$schemas = $db->select('SELECT `schema` FROM `{pr}groups` WHERE `id` IN ('.AString::from('')->join($groups,',').')');
		$userfields = array();
		$uservars = ($id != 0) ? $um->getUserAttributes($id) : array();
		if($schemas != null) {			
			foreach( $schemas as $schema) {				
				$userfields = array_merge($userfields, (array)Encore::fromJson($schema['schema']));				
			}			
		}
		$render = Encore::getModule('Render');		
		foreach($userfields as $key=>$fd) {
			$field = (array)$fd;
			$ret .= '<label class="default">'.$key.'</label>';
		    $ret .= $render->getSnippet('inputs'.DS.$field['type'], array(
				'value' => (isset( $uservars[$key])) ? $uservars[$key] : null,
				'name' => 'var_'.$key,
				'classname' => 'default'				
			) );
			$ret .= '<br/>';
		}
		
		return $ret;
	}
}
