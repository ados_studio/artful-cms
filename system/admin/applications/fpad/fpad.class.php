<?php

class FPad extends EncoreModule {
	public function init() {
		Encore::requireModules('FPad', 'Mandator,FileManager');
		Encore::initDictionary('admin');
		
		Encore::sessionStart();
				
		$mandator = Encore::getModule('Mandator');
		$mandator->init();
		
		if( $mandator->hasPermission('application','/fpad/') ) {
			if(isset($_POST['action'])) {
				$ret = null;
				switch($_POST['action']) {
					case 'loadfile': 
						$fn = AString::from(dirname(SYSTEM_PATH).$_POST['filename'])->replace('/',DS)->replace(DS.DS,DS);
						if($mandator->hasPermission('readfile',$_POST['filename'])) {
							echo(file_get_contents($fn));						
						}
						break;
					case 'savefile':
						$fn = AString::from(dirname(SYSTEM_PATH).$_POST['filename'])->replace('/',DS)->replace(DS.DS,DS);
						if($mandator->hasPermission('writefile',$_POST['filename'])) {
							file_put_contents($fn, $_POST['content']);
							echo(Encore::toJson(array('success'=>true)));
						}
						break;
					default:
						$ret = array('success'=>false);
				}
				//echo(Encore::toJson($ret));
			} else {
				$render = Encore::getModule('Render');
				$pm = Encore::getModule('PageManager');
				$render->init();
				$render->renderPage(array(
					'template' => 'fpad',
					'vars' => array()
				));
			}
		} else {
			$render = Encore::getModule('Render');
			$render->init();
			$render->renderPage(array(
				'template' => '403',
				'vars' => array()
			));
		}	
		
		
	}
}
