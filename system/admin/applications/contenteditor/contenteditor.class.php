<?php

class ContentEditor extends EncoreModule {
	public function init(){
		Encore::requireModules('ContentEditor App', 'DB,Mandator,Render');
		Encore::initDictionary('admin');

		Encore::sessionStart();

		$mandator = Encore::getModule('Mandator');
		$mandator->init();
		if($mandator->hasPermission('application','/contenteditor/')){

			if(isset($_POST['action'])) {
				$ret = null;
				switch($_POST['action']) {
					case 'getchilds':
						$ret = $this->getChilds();
						break;
					case 'save':
						$ret = array( 'success'=> $this->savePage());
						break;
					case 'saveType':
						$ret = array( 'success'=>$this->savePageType());
						break;
					case 'delete':
						$ret = $this->deletePage();
						break;
					case 'deleteType':
						$ret =  array( 'success'=>$this->deletePageType());
						break;
					case 'getpagelocales':
						$ret = $this->getPageLocales;
						break;
					default:
						$ret = array('success' => false );
				}

				echo(Encore::toJson($ret));
			} else {
				$render = Encore::getModule('Render');
				$pm = Encore::getModule('PageManager');
				$render->init();
				if(isset($_POST['page'])) {
					switch($_POST['page']) {
						case 'editor':
							$id = (isset($_POST['id']))?$_POST['id']:0;
							$render->setPlaceholder('id', $id);
							$render->setPlaceholder('localize',(isset($_POST['localize']) && !AString::from($_POST['localize'])->isEmpty())?$_POST['localize']:Encore::getOption('default_localize'));
							$render->setPlaceholder('parent',(isset($_POST['parent']))?$_POST['parent']:0);
							if($id!=0) {
								$page = Encore::getModule('PageManager')->getForcePageById($id,$_POST['localize']);
							}
							$render->setPlaceholder('pagetitle', (isset($page['pagetitle']))?$page['pagetitle']:'');
							$render->setPlaceholder('longtitle', (isset($page['longtitle']))?$page['longtitle']:'');
							$render->setPlaceholder('seotitle', (isset($page['seotitle']))?$page['seotitle']:'');
							$render->setPlaceholder('alias', (isset($page['alias']))?$page['alias']:'');
							$render->setPlaceholder('description', (isset($page['description']))?$page['description']:'');
							$render->setPlaceholder('keys', (isset($page['keys']))?$page['keys']:'');
							$render->setPlaceholder('type', (isset($page['type']))?$page['type']:'');

							$render->setPlaceholder('sort', (isset($page['sort']))?$page['sort']:0);

							$render->setPlaceholder('published', (isset($page['published'])&&$page['published']==1)?'checked':'');
							$render->setPlaceholder('folder', (isset($page['folder'])&&$page['folder']==1)?'checked':'');
							$render->setPlaceholder('accessable', (isset($page['accessable'])&&$page['accessable']==1)?'checked':'');
							$render->setPlaceholder('executable', (isset($page['executable'])&&$page['executable']==1)?'checked':'');

							$render->setPlaceholder('preview', (isset($page['preview']))?$page['preview']:'');
							$render->setPlaceholder('content', (isset($page['content']))?$page['content']:'');

							echo ($render->getChunk('contenteditor_editor'));
							break;
						case 'welcome':
							echo ($render->getChunk('contenteditor_welcome'));
							break;
						case 'types':
							echo ($render->getChunk('contenteditor_types'));
							break;
						case 'edittype':
							$id = (isset($_POST['id']))? $_POST['id']: 0;
							$render->setPlaceholder('id', $id );
							$tpl = '';
							$schema = '';
							$name = '';
							if($id!=0) {
								$atype = $pm->getPageType((integer)$id);
								$name = $atype['name'];
								$tpl = $atype['template'];
								$schema = $atype['schema'];
							}

							$render->setPlaceholder('name', $name );
							$render->setPlaceholder('template', $tpl  );
							$render->setPlaceholder('schema', $schema  );
							echo ($render->getChunk('contenteditor_typeeditor'));
							break;
						case 'pageVars':

							echo($render->getSnippet('contenteditor_pagevars', array('id'=>$_POST['id'] , 'type' => $_POST['type'] )));
							break;
						default:
							echo ('Page not found!');
					}

				} else {
					$render->renderPage(array(
						'template' => 'contenteditor',
						'vars' => array()
					));
				}
			}
		} else {
			$render = Encore::getModule('Render');
			$render->init();
			$render->renderPage(array(
				'template' => '403',
				'vars' => array()
			));
		}
	}

	protected function getChilds() {
		$ret = null;
		if(isset($_POST['node'])) {
			$pm = Encore::getModule('PageManager');
			$childs = $pm->getPages($_POST['node'], 1);
			if ( $childs != null ) {
				$ret = array();
				foreach($childs as $child) {
					$ret[] = array(
						'id' => $child['id'],
						'text' => ((empty($child['pagetitle'])) ? $child['alias'] : $child['pagetitle']).' <b>['.$child['id'].']</b>',
						'leaf' => ($child['folder']==0)
					);
				}
			}

		}
		return $ret;
	}

	protected function savePage() {
		$ret = true;
		$db = Encore::getModule('DB');
		$page = array(
			'id' => (integer)$_POST['id'],
			'parent' => (integer)$_POST['parent'],
			'alias' => $db->real_escape_string( $_POST['alias'] ),
			'pagetitle' => $db->real_escape_string($_POST['pagetitle']),
			'longtitle' => $db->real_escape_string($_POST['longtitle']),
			'seotitle' => $db->real_escape_string($_POST['seotitle']),
			'description' => $db->real_escape_string($_POST['description']),
			'keys' => $db->real_escape_string($_POST['keys']),
			'type' => (integer)$_POST['type'],
			'published' => ($_POST['published']=='true'),
			'folder' => ($_POST['folder']=='true'),
			'accessable' => ($_POST['accessable']=='true'),
			'executable' => ($_POST['executable']=='true'),
			'preview' => $db->real_escape_string($_POST['preview']),
			'content' => $db->real_escape_string($_POST['content']),
			'sort' => $db->real_escape_string($_POST['sort'])
		);
		$pm = Encore::getModule('PageManager');
		$last_locaize = Encore::getOption('localize');
		Encore::setOption('localize',$db->real_escape_string($_POST['localize']));
		if($page['id']==0){
			$page['id'] = $pm->createPage($page);
			$pm->updatePageVars($page['id'],(array)Encore::fromJson($_POST['vars']));
		} else {
			$pm->updatePage($page);
			$pm->updatePageVars($page['id'],(array)Encore::fromJson($_POST['vars']));
		}
		Encore::setOption('localize',$last_locaize);
		return $ret;
	}

	protected function deletePage() {
		$pm = Encore::getModule('PageManager');
		$pm->deletePage((integer)$_POST['id']);
		return array('success' => true );
	}

	protected function savePageType() {
		$ret = false;
		$pm = Encore::getModule('PageManager');
		$db = Encore::getModule('DB');
		$pageType = array(
			'id' => (integer)$_POST['id'],
			'name' => $db->real_escape_string($_POST['name']),
			'template' => $db->real_escape_string($_POST['template']),
			'schema' => $db->real_escape_string($_POST['schema'])
		);
		if($_POST['id']==0) {
			$ret = $pm->createPageType($pageType);
		} else {
			$ret = $pm->updatePageType($pageType);
		}
		return $ret;
	}

	protected function deletePageType() {
		$ret = false;
		if(isset($_POST['id'])){
			$pm = Encore::getModule('PageManager');
			$ret = $pm->deletePageType($_POST['id']);
		}
		return $ret;
	}

	protected function getPageLocales() {
		$ret = array('success'=>false, locales=>array());
		$id = (integer)$_POST['id'];
		$ret['locales'] = Encore::getModule('PageManager')->getPageLocales($id);
		$ret['success'] = ($ret['locales']!=null&&count($ret['locales'] >0));
		return $ret;
	}
}
