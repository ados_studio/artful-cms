<?php

define ('ADMIN_TEMPLATES_PATH', dirname(__FILE__).DS.'templates'.DS);

Encore::setOption('APP_PATH', dirname(dirname(dirname(__FILE__))).DS.'admin'.DS.'applications'.DS,'ADMIN');

Encore::setOption('RENDER_PATH',ADMIN_TEMPLATES_PATH.'render'.DS,'RENDER');
Encore::setOption('CHUNKS_PATH',ADMIN_TEMPLATES_PATH.'render'.DS.'chunks'.DS,'RENDER');
Encore::setOption('SNIPPETS_PATH',ADMIN_TEMPLATES_PATH.'snippets'.DS,'RENDER');

Encore::setOption('localize','ru');
