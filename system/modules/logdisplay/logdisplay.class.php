<?php

class logDisplay extends EncoreModule {
	
	public function init() {
		Encore::connect('beforeLog', array($this,'display'));
	}
	
	public function display($arg) {
		if($arg['level'] > Encore::getOption('log_level',0)) return;
		echo("<hr />LOG DISPLAY >>> Log level: {$arg['level']} > {$arg['section']} - {$arg['owner']}<br />{$arg['message']}<br /><hr />");
		$arg['accept'] = !Encore::getOption('log_blocking', false, 'logdisplay');
	}
}