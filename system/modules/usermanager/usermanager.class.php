<?php

class UserManager extends EncoreModule {

	public function __construct() {
		$this->registerSignals(array('afterCreateUser','afterUpdateUser','afterDeleteUser','afterCreateGroup','afterUpdateGroup','afterDeleteGroup'));
	}

	public function init() {
		Encore::requireModules('UserManager', 'UserAuth');
	}

	public function getUser ($id) {
		$db = Encore::getModule('DB');
		$sid = (integer) ($id);
		$ret = $db->select("SELECT `id`,`login`,`name`,`email` FROM `{pr}users` WHERE `id`='$sid' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}

	public function getUserByEmail ($email) {
		$db = Encore::getModule('DB');
		$semail = $db->real_escape_string($email);
		$ret = $db->select("SELECT `id`,`login`,`name`,`email` FROM `{pr}users` WHERE `email`='$semail' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}

	public function getUserByLogin ($login) {
		$db = Encore::getModule('DB');
		$login = $db->real_escape_string($login);
		$ret = $db->select("SELECT `id`,`login`,`name`,`email` FROM `{pr}users` WHERE `login`='$login' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}

	public function getUserByName ($name) {
		$db = Encore::getModule('DB');
		$name = $db->real_escape_string($name);
		$ret = $db->select("SELECT `id`,`login`,`name`,`email` FROM `{pr}users` WHERE `name`='$name' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}

	public function getGroupById ($id) {
		$db = Encore::getModule('DB');
		$gid = (integer) $id;
		$ret = $db->select("SELECT `id`,`name`,`schema` FROM `{pr}groups` WHERE `id`='$gid' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}

	public function getGroupByName ($name) {
		$db = Encore::getModule('DB');
		$gname = $db->real_escape_string($name);
		$ret = $db->select("SELECT `id`,`name`,`schema` FROM `{pr}groups` WHERE `name`='$gname' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}

	public function createUser ($user) {
		$ret = false;

		if(is_array($user)) {
			$db = Encore::getModule('DB');
			if(!isset($user['active'])) {
				$user['active'] = false;
			} else {
				$user['active'] = ($user['active']==true);
			}
			if(!isset($user['activefrom'])) {
				$user['activefrom'] = date('Y-m-d H:i:s',time());
			}
			$salt = Encore::getOption('SALT','***','USERAUTH');
			$user['password'] = md5(md5($user['password']).$salt);

			$user['login'] = $db->real_escape_string($user['login']);
			$user['name'] = $db->real_escape_string($user['name']);
			$user['email'] = $db->real_escape_string($user['email']);
			$ret = $db->exec("INSERT `{pr}users`(`login`,`name`,`email`,`password`,`active`,`activefrom`) VALUES ('{$user['login']}','{$user['name']}','{$user['email']}','{$user['password']}','{$user['active']}','{$user['activefrom']}')");
			if($ret) {
				$this->emit('afterCreateUser', $user);
				$ret = $db->last_id();
			}
		}
		return $ret;
	}

	public function updateUser ($user, $id=null) {
		$ret = false;
	    if ($id!=null) {
	    	$user['id'] = $id;
	    }
		if(is_array($user)) {
			$db = Encore::getModule('DB');
			$ret = $db->exec("UPDATE `{pr}users` SET `login`='{$user['login']}', `name`='{$user['name']}', `email`='{$user['email']}' WHERE `id`='{$user['id']}'");
			if($ret) {
				$this->emit('afterUpdateUser', $user);
			}
		}
		return $ret;
	}

	public function deleteUser ($id) {
		$db = Encore::getModule('DB');
		$ret = $db->exec("DELETE FROM `{pr}users` WHERE `id` = '".(integer)$id."'");
		if($ret) {
			$ret = $db->exec("DELETE FROM `{pr}usergroups` WHERE `user` = '".(integer)$id."'");
			if($ret) {
				$ret = $db->exec("DELETE FROM `{pr}uservars` WHERE `user` = '".(integer)$id."'");
				if ($ret) {
					$this->emit('afterDeleteUser', array('id'=>$id));
				}
			}
		}
		return $ret;
	}

	public function createGroup (array $group) {
		$db = Encore::getModule('DB');
		$ret = $db->exec("INSERT `{pr}groups`(`name`,`schema`) VALUES ('".$db->real_escape_string($group['name'])."','".$db->real_escape_string($group['schema'])."')");
		if($ret){
			$this->emit('afterCreateGroup');
			$ret = $db->last_id();
		}
		return $ret;
	}

	public function updateGroup ($group, $id=null) {
		$db = Encore::getModule('DB');
		$gr = array();

		if(is_array($group)) {
			if(!isset($group['name']) || !isset($group['schema']) ) return false;
			$gr['name'] = $group['name'];
			$gr['schema'] = $group['schema'];
			if($id==null) {
				if(!isset($group['id'])) return false;
				$gr['id'] = $group['id'];
			} else {
				$gr['id'] = $id;
			}
		} else {
			if($id == null) return false;
			$gr['id'] = $id;
		}

		$ret = $db->exec("UPDATE `{pr}groups` SET `name`='".$gr['name']."', `schema`='".$gr['schema']."' WHERE `id` = '".$gr['id']."'");
		if ($ret) {
			$this->emit('afterUpdateGroup');  // TODO emit with arguments
		}
		return $ret;
	}

	public function deleteGroup ($id) {
		$db = Encore::getModule('DB');
		$ret = $db->exec("DELETE FROM `{pr}groups` WHERE `id` = '".(integer)$id."'");
		$ret = $db->exec("DELETE FROM `{pr}usergroups` WHERE `group` = '".(integer)$id."'");
		if($ret) {
			$this->emit('afterDeleteGroup', array('id'=>(integer)$id) );
		}
		return $ret;
	}

	public function updatePassword ($id, $password) {
		$db = Encore::getModule('DB');
		$sid = (integer)($id);
		$salt = Encore::getOption('SALT','***','USERAUTH');
		$spass = md5(md5($password).$salt);
		return $db->exec("UPDATE `{pr}users` SET `password`='$spass' WHERE `id`='$sid'");
	}

	public function getGroupsList () {
		$db = Encore::getModule('DB');
		return $db->select('SELECT `id`,`name` FROM `{pr}groups`');
	}

	public function getUsersList () {
		$db = Encore::getModule('DB');
		return $db->select('SELECT `id`,`login`,`name` FROM `{pr}users`');
	}

	public function updateUserGroups($uid, array $groups) {
		$db = Encore::getModule('DB');
		$db->exec('DELETE FROM `{pr}usergroups` WHERE `user` = \''.(integer)$uid.'\'');
		$ret = true;
		if (count($groups > 0 )) {
			$groupvals = array();
			foreach($groups as $gid) {
				$groupvals[] = '("'.(integer)$uid.'","'.(integer)$gid.'")';
				$ret = $db->exec('INSERT INTO `{pr}usergroups`(`user`,`group`) VALUES '. AString::from('')->join($groupvals,' , '));
			}
		}
		return $ret;
	}

	public function updateGroupUsers($gid, array $users) {
		$db = Encore::getModule('DB');
		$db->exec('DELETE FROM `{pr}usergroups` WHERE `group` = \''.(integer)$gid.'\'');
		$ret = true;
		if (count($users > 0 )) {
			$groupvals = array();
			foreach($users as $uid) {
				$groupvals[] = '("'.(integer)$uid.'","'.(integer)$gid.'")';
				$ret = $db->exec('INSERT INTO `{pr}usergroups`(`user`,`group`) VALUES '. AString::from('')->join($groupvals,' , '));
			}
		}
		return $ret;
	}

	public function appendUserToGroup($uid, $gid) {
		$ret = false;
		if( !$this->userInGroup($uid, $gid) ) {
			$db = Encore::getModule('DB');
			$ret = $db->exec('INSERT INTO `{pr}usergroups`(`user`,`group`) VALUE ("'.(integer)$uid.'","'.(integer)$gid.'")');
		}
		return $ret;
	}

	public function getUserAttribute ($uid, $attr) {
		$db = Encore::getModule('DB');
		$ret = $db->select('SELECT `value` FROM `{pr}uservars` WHERE `user` = "'.(integer)$uid.'" AND `name` = "'.$db->real_escape_string($attr).'"');
		return ($ret == NULL) ? NULL : $ret[0]['value'];
	}

	public function setUserAttribute ($uid, $attr, $value) {
		$db = Encore::getModule('DB');
		$db->exec('DELETE FROM `{pr}uservars` WHERE `user` = "'.(integer)$uid.'" AND `name` = "'.$db->real_escape_string($attr).'"');
		return $db->exec('INSERT INTO `{pr}uservars`(`user`,`name`,`value`) VALUE ("'.(integer)$uid.'","'.$db->real_escape_string($attr).'","'.$db->real_escape_string($value).'")');
	}

	public function issetUserAttribute ($uid, $attr) {
		$db = Encore::getModule('DB');
		$ret = $db->select('SELECT `value` FROM `{pr}uservars` WHERE `user` = "'.(integer)$uid.'" AND `name` = "'.$db->real_escape_string($attr).'"');
		return ($ret == NULL);
	}

	public function getUserAttributes ($uid) {
		$vars = Encore::getModule('DB')->select('SELECT `name`,`value` FROM `{pr}uservars` WHERE `user`="'.(integer)$uid.'"');
		$ret = array();
		foreach( $vars as $var) {
		    $ret[$var['name']] = $var['value'];
		}
		return $ret;
	}

	public function updateUserAttributes ($uid, array $attribytes) {
		$db = Encore::getModule('DB');
		$db->exec('DELETE FROM `{pr}uservars` WHERE `user`="'.(integer)$uid.'"');
		if (count($attribytes) < 1 ) return true;
		$attrs = array();
		foreach($attribytes as $name=>$value) {
		    $attrs[] = '("'.(integer)$uid.'","'.$db->real_escape_string($name).'","'.$db->real_escape_string($value).'")';
		}

		return $db->exec('INSERT INTO `{pr}uservars`(`user`,`name`,`value`) VALUES '. AString::from('')->join($attrs,','));
	}


	public function userInGroup ($uid, $gid) {
		$db = Encore::getModule('DB');
		return ($db->select("SELECT `user` FROM `{pr}usergroups` WHERE `user` = '".(integer)$uid."' AND `group` = '".(integer)$gid."' ") != null);
	}

	public function getUserGroups ($uid ) {
		$db = Encore::getModule('DB');
		$res = $db->select('SELECT `group` FROM `{pr}usergroups` WHERE `user` = \''.(integer)$uid.'\'');
		$ret = array();
		if($res != null ) {
			foreach($res as $rr) {
		    	$ret[] = $rr['group'];
			}
		}
		return  $ret;
	}

	public function getGroupUsers ($gid) {
		$db = Encore::getModule('DB');
		$res = $db->select('SELECT `user` FROM `{pr}usergroups` WHERE `group` = \''.(integer)$gid.'\'');
		$ret = array();
		if($res != null ) {
			foreach($res as $rr) {
		    	$ret[] = $rr['user'];
			}
		}
		return  $ret;
	}

	public function getUserAttributesSchema($uid) {
		$db = Encore::getModule('DB');
		$scs = $db->select('SELECT `{pr}groups`.`schema` FROM `{pr}groups`, `{pr}usergroups` WHERE `{pr}groups`.`id` = `{pr}usergroups`.`group` AND `{pr}usergroups`.`user` = '.(integer)$uid);
		$ret = array();
		foreach( $scs as $scd ) {
			$varschema = (array)Encore::fromJson($scd['schema']);
			foreach ($varschema as $varname=>$vartype) {
				$ret[$varname] = (array) $vartype;
			}
		}
		return $ret;
	}
}
