<?php

class Mandator extends EncoreModule {
	protected $permissions = array();
	protected $is_admin = false;
	protected $is_auth = false;
	protected $user_id = null;

	public function __construct() {
		Encore::requireModules('Mandator','UserManager');
		//$this->registerSignals(array(''));
		$um = Encore::getModule('UserManager');
		$um->connect('afterDeleteUser', array($this,'onUserDeleted'));
		$um->connect('afterDeleteGroup', array($this,'onGroupDeleted'));
	}

	public function init() {
		Encore::requireModules('Mandator', 'DB,UserAuth');

		$this->permissions = $this->getFullUserPermission(3);  // GET NOBODY PRIVILEGES
		
		 
		if(Encore::sessionStarted()) {
			$userdata = Encore::getModule('UserAuth')->getUserData();
			$user = $userdata['id'];
			if($user == 1) $this->is_admin = true;

			$this->permissions = array_merge($this->getFullUserPermission($user),$this->getFullUserPermission(2));
		}
	}

	public function isAdmin() {
		return $this->is_admin;
	}

	public function hasPermission($action, $path) {		
		if ( $this->is_admin ) return true;
		$ret = false;
		foreach($this->permissions as $mandat) {			
			if($mandat['action'] == $action) {
				$ret = $this->in_path($mandat['path'], $path);
				if($ret) break;
			}
		}
		
		return $ret;
	}
	
	public function getCurrentUserPermissions($need_action = null) {   
		if($need_action==null) return $this->permisions;
		$ret = array();
		
		foreach ($this->permissions as $action=>$path) {
			if($need_action==$action) {
				$ret[$action] = $path;
			}
		}	
		return $ret;
	}

	public function hasUserPermission($user, $action, $path) {
		$permissions = $this->getFullUserPermission($user, $action);
		$ret = false;
		if($permissions != null ) {
			foreach( $permissions as $mandat) {
			    $ret = $this->in_path($mandat['path'], $path);
			    if($ret) break;
			}
		}
		return $ret;
	}

	public function getFullUserPermission ($user, $need_action = null) {
		$ret = $this->getUserPermissions($user, $need_action);
		$groups = Encore::getModule('UserManager')->getUserGroups($user);
		if (count($groups) > 0 ) $ret = array_merge( $ret, $this->getGroupsPermissions($groups));
		return $ret;
	}

	public function getUserPermissions($user, $need_action = null) {
		$db = Encore::getModule('DB');
		$query = 'SELECT `action`,`path` FROM `{pr}user_privileges` WHERE `user` = "'.(integer)$user.'"';
		if($need_action != null ) $query .= ' AND `action` = "'.$need_action.'"';
		return $db->select($query);
	}
	
	public function getGroupPermissions($group, $need_action = null) {
		$db = Encore::getModule('DB');
		$query =  'SELECT `action`,`path` FROM `{pr}group_privileges` WHERE `group` = "' . (integer) $group .'"';
		if($need_action != null ) $query .= ' AND `action` = "'.$need_action.'"';
		return $db->select($query);
	}

	public function getGroupsPermissions(array $groups, $need_action = null) {
		$db = Encore::getModule('DB');
		$query = 'SELECT `action`,`path` FROM `{pr}group_privileges` WHERE `group` IN (' . AString::from('')->join($groups,',').')';
		if($need_action != null ) $query .= ' AND `action` = "'.$need_action.'"';
		return $db->select($query);
	}

	public function updateUserPermissions($userid, array $privileges) {
		$userid = (integer) $userid;
		$db = Encore::getModule('DB');
		$db->exec('DELETE FROM `{pr}user_privileges` WHERE `user` = "'.$userid.'"');
		if ( count( $privileges ) < 1 ) return true;
		$queryargs = array();
		foreach ( $privileges as $mandat) {
			$queryargs[] = '("'.$userid.'","'.$db->real_escape_string($mandat['action']).'","'.$db->real_escape_string($mandat['path']).'")';
		}
		$query = 'INSERT INTO `{pr}user_privileges`(`user`,`action`,`path`) VALUES '. AString::from('')->join($queryargs, ',');
		return $db->exec($query);
	}

	

	public function updateGroupPermissions($groupid, array $privileges) {		
		$groupid = (integer)$groupid;
		$db = Encore::getModule('DB');
		$db->exec('DELETE FROM `{pr}group_privileges` WHERE `group` = "'.$groupid.'"');
		if ( count ($privileges) < 1 ) return true;
		$queryargs = array();
		
		foreach ($privileges as $mandat) {
		    $queryargs[] = ' ("'.$groupid.'","'.$db->real_escape_string($mandat['action']).'","'.$db->real_escape_string($mandat['path']).'") ';
		}

		$query = 'INSERT INTO `{pr}group_privileges`(`group`,`action`,`path`) VALUES ' . AString::from('')->join($queryargs,',');
		return $db->exec($query);
	}

	public function onUserDeleted($args) {
		$this->updateUserPermissions($args['id'], array());
	}

	public function onGroupDeleted($args) {
		$this->updateGroupPermissions($args['id'], array());
	}

	protected  function in_path($search, $path) {
		return ( AString::from($path)->indexOf($search) == 0);
	}
}