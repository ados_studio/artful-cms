<?php


class InitFront extends EncoreModule {

	public function init() {
		error_reporting(E_ALL);
		
		Encore::sessionStart();
		Encore::requireModules('FRONTINIT', 'UrlParser, PageManager, Render');
		
		$pagemanager = Encore::getModule('PageManager');
		
		$pagemanager->init();		
		$render = Encore::getModule('Render');
		$render->init();
		Encore::getModule('Mandator')->init();
		$op = Encore::getModule('UrlParser')->getScriptArguments();
		$page = $pagemanager->getCurrentPage();
		
		if( Encore::getOption('localize') != Encore::getOption('default_localize') && $page == null ) {
			Encore::setOption('localize', Encore::getOption('default_localize')) ;
			$page = $pagemanager->getCurrentPage();
		}

		Encore::initDictionary('frontend');
		
		if ( !isset($op['html']) || $page == null || $page['published'] != true ) {
			header("HTTP/1.0 404 Not Found");
			$op['html'] = Encore::getOption('NOT_FOUNT_PAGE','404');
			$page = $pagemanager->getPageByAlias($op['html']);
			$page['vars'] = $pagemanager->getPageVars($page['id']);
		}
		
		$parents = $pagemanager->getPageParents($page['id']);
		
		if ( $page['accessable']) {			
			$pids = array();
			foreach($parents as $parent) {
				$pids[] = $parent['page_id'];
			}
			$pids[] = $page['id'];
                       
			if ( !Encore::getModule('Mandator')->hasPermission('viewpage',AString::from('')->join($pids, '/')) ) {
				$op['html'] = Encore::getOption('ACCESS_FORBIDEN_PAGE','403');
				$page = $pagemanager->getPageByAlias($op['html']);
				$page['vars'] = $pagemanager->getPageVars($page['id']);                               
			}
		}
		
		$parent_id = null;
		$parent_alias = null;
		if ($parents != null){			
			$first_parent = current($parents);
			$parent_page = $pagemanager->getPageById($first_parent['parent']);
			if ($parent_page != null){				
				$parent_id = $parent_page['id'];
				$parent_alias = $parent_page['alias'];
			}
		}
		          
		$render->renderPage($page, array(
			'site_name' => Encore::getOption('site_name'),
			'site_slogan' => Encore::getOption('site_slogan'),
			'parent_id' => $parent_id,
			'parent_alias' => $parent_alias
			));
	}
}
