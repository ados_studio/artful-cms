<?php

class DB_MYSQLI extends EncoreModule {
	
	public $mysqli = null;
	
	protected function dbconect () {
		
		$this->mysqli = new mysqli(
				Encore::getOption('db_host'), 
				Encore::getOption('db_user'), 
				Encore::getOption('db_pass'),
				Encore::getOption('db_name')
		); 
		
		$this->mysqli->query('SET CHARACTER SET \''.Encore::getOption('db_charset').'\'');
		if(mysqli_connect_errno()) {
			throw AException(Encore::tr('ER_DB_CONNECT'),0);
		}			
	}
	
	public function select($query) {		
		$ret = null;
		if($this->mysqli == null ) $this->dbconect();		
		if ($result = $this->mysqli->query(AString::from($query)->replace('{pr}', Encore::getOption('db_prefix','')))) {
			$ret = array();
			while( $row = $result->fetch_assoc() ){
				foreach( $row as $key => $rowitem ) {
					$row[$key] = stripslashes($row[$key]);
				}
				$ret[] = $row;
			}
			$result->close();
		} else {
			Encore::log('DBMYSQLI','SELECT', AString::from(Encore::tr('ER_DB_SELECT'))->arg($this->mysqli->error)->arg($query),0);
		};
		return $ret;
	}
	
	public function exec($query) {
		$ret = null;
		if($this->mysqli == null ) $this->dbconect();
		if ($result = $this->mysqli->query(AString::from($query)->replace('{pr}', Encore::getOption('db_prefix','')))) {
			$ret = $result;
		} else {
			Encore::log('DBMYSQLI','SELECT', AString::from(Encore::tr('ER_DB_EXEC'))->arg($this->mysqli->error)->arg($query),1);
		};
		return $ret; 
	}
	
	public function real_escape_string($str) {		
		if($this->mysqli == null ) $this->dbconect();		
		return $this->mysqli->real_escape_string($str);
	}
	
	public function last_id() {
		return $this->mysqli->insert_id;
	}
	
	public static function parseTime($timestring) {
		$tsa = AString::from($timestring)->split(' ');
		$dsa = AString::from($tsa[0])->split('-');
		$tsa = AString::from($tsa[1])->split(':');
		return mktime($tsa[0],$tsa[1],$tsa[2],$dsa[1],$dsa[2],$dsa[0]);
	}
	
	public function init () {
		
	}
}