<?php

class UrlParser extends EncoreModule {
	protected $args = array();
	
	public function init(){ 
		$scriptOptions = Array();		
		$qarray = AString::from($_SERVER['QUERY_STRING'])->replace('q=','')->split('/');		
		foreach( $qarray as $qitem) {
			$aitem = AString::from($qitem);
			$var = '';
			$key = '';				
			if( $aitem->length() > 4 && $aitem->right(5)->toString()=='.html') {				
				$vals = $aitem->split('.');
				$key = 'html';
				$var = $vals[0];		
			} else {			
				$vals = $aitem->split('_');			
				$key = $vals[0];
				$var = (count($vals) > 1) ? $vals[1] : null;				
			}	
			//$scriptOptions[$key] = urldecode($var);
			$scriptOptions[$key] = $var;
		}
		$this->args = $scriptOptions;
	}
	
	public function getScriptArguments() {
		return $this->args;	
	}
	
	public function parseUrl($uri) {
		$ret = $uri;
		$uri_protocol = 'http';
		$uri_domain = '';
		$uri_file = '';
		$uri_args = '';
		$parts = AString::from($uri)->split('?');
		$site_parts = AString::from($parts[0])->split('://');
		
		if ( isset($site_parts[1]) && $site_parts[1] != '') {
			$uri_protocol = $site_parts[0];
			$parts[0] = AString::from($site_parts[1]);
		} 
		$site_parts = AString::from($parts[0])->split('/');
		$uri_domain = AString::from($site_parts[0])->replace('www.','');
		
		if ( $uri_domain == $_SERVER['HTTP_HOST'] ) {
			$ret = $uri_protocol.'://'.$uri_domain;
			$spc = count($site_parts); 
			if ( $spc > 1 ) {
				if($site_parts[1]=='') {
					$uri_file = 'index.html';
				} else {
					$uri_file = new AString();
					$separator = Encore::getOption('filepath_separator','-','URLPARSER');
					$sp = '';
					for ( $i = 1; $i < $spc; $i++ ){												
						$uri_file = $uri_file->append($sp)->append($site_parts[$i]);
						$sp = $separator;
					}
					$uri_file = $uri_file->replace('.php', '.html');
					if($uri_file->right(5) != '.html') $uri_file = $uri_file->append('.html');
				}
			} else {
				$uri_file = 'index.html';
			}
			
			if ( isset($parts[1]) ) {
				$params = AString::from($parts[1])->split('&');
				foreach ( $params as $param ) {
					$ret .= '/'.AString::from($param)->replace('=','_'); 
				}
			}
			
			
			$ret.='/'.$uri_file;
		} else {
			// TODO outlinkink!
		}
		return $ret;		
	}
	
	public function makeUrl($pagename, $args = array(), $protocol = 'http', $autosort = true) {
		$uri_file = AString::from($pagename);		
		$uri_file = $uri_file->replace('.php', '.html');
		if ( $uri_file->right(5) != '.html' ) $uri_file = $uri_file->append('.html');
		if ( $uri_file->left(1) == '/' ) $uri_file = $uri_file->right($uri_file->length());
		$uri_file = '/'.$uri_file->replace('/', Encore::getOption('filepath_separator','-','URLPARSER'));
		
		$uri_args = '';
		if(is_array($args)) {
			if($autosort) ksort($args);
			foreach($args as $key=>$var) {
				if ( $key == 'html' ) continue;
				$uri_args .= '/'.$key;
				if($var != '' || $var != null) $uri_args .= '_'.urlencode($var); 
			}
		}
		return $protocol.'://'.$_SERVER['HTTP_HOST'].$uri_args.$uri_file;	
	} 
}
