<?php


class initAdminModule extends EncoreModule {
	public function init() {
		Encore::sessionStart();

		Encore::getModule('Mandator')->init();
		Encore::getModule('Render')->init();


		if(isset($_POST['action'])) {
			$ret = array(
				'success' => false,
				'message' => Encore::tr('ER_INVALID_ACTION','admin'),
				'result' => null
			);
			switch ($_POST['action']) {
				case 'getuserdirs':
					$ret = $this->getUserDirs();
					break;
				case 'getuserfiles':
					$ret = $this->getUserFiles();
					break;
				case 'uploadfile' :
					$ret = $this->uploadFile();
					break;
			}
			echo( Encore::toJson($ret));
			return;
		}

		if(isset($_POST['login'])){
			if(Encore::sessionStarted()) {
				readfile(SYSTEM_PATH.DS.'admin'.DS.'desktop_config.json' );
			} else {
				echo('{ "status":"faild", "result": "Login incorect!" }');
			}
		} else {
			$astr = AString::from(file_get_contents( ADMIN_TEMPLATES_PATH.'desktop.html' ));
			$config = '';
			if(Encore::sessionStarted()) {
				$config = 'var res = '.file_get_contents(SYSTEM_PATH.DS.'admin'.DS.'desktop_config.json' ).'; Forbiz.config = res.result;';
			}
			$astr = $astr->replace('[[+config]]',$config);
			echo($astr);
		}
	}

	protected function getUserDirs() {
		if(!isset($_POST['node'])) return;
		$fm = Encore::getModule('FileManager');
		$ret = array();
		if($_POST['node'] == '[root]') {
			$mandator = Encore::getModule('Mandator');
			$places = $mandator->getCurrentUserPermissions('readfiles');
			if($fm->getStartPath()!=null) {
				$places[]['path'] = $fm->getStartPath();
			}


			if($mandator->hasPermission('readfiles',$fm->getBasePath())) {
				$places[]['path'] = DS;
			}

			$places = array_unique($places);
			foreach ($places as $place) {
				$nottop = false;
				foreach ($places as $pl) {
					$result = AString::from($place['path'])->indexOf($pl['path']);
					if($pl['path']!=$place['path'] &&  is_int($result) && $result == 0) {
						$nottop = true;
						break;
					}
				}

				if (!$nottop) {
					$ret[] = array('text' => $place['path'], 'id'=> $place['path'], 'leaf'=>false, 'icon'=>'/admin/img/icons/16/remote-place.png');
				}
			}
		} else {
			$dirs = $fm->getDirList($_POST['node'],true);

			foreach($dirs as $dir) {
				$ret[] = array('text' => $dir['filename'], 'id'=> $dir['fullpath'], 'leaf'=>false);
			}
		}

		return $ret;
	}

	protected function getUserFiles() {
		if(!isset($_POST['path'])) return;
		$mandator = Encore::getModule('Mandator');
		$ret = array();
		if($mandator->hasPermission('readfiles', $_POST['path'])) {
			$fm = Encore::getModule('FileManager');
			$files = $fm->getFileList($_POST['path'], true);
			foreach ($files as $file) {
				$ret[] = array(
					'text' => $file['filename'],
					'id'=> $file['fullpath'],
					'link' => AString::from($file['fullpath'])->replace(DS,'/')->toString(),
					'leaf'=>!is_dir($file['fullpath']),
					'mime'=>$this->getMimeType( $fm->fromBase($file['fullpath']) ) ,
					'md5'=>md5($file['fullpath'])
				);
			}
		}
		return $ret;
	}

	protected function getMimeType($filename) {
		$ret = 'file';
		switch (true) {
			case is_dir($filename) :
				$ret = 'folder';
				break;
			case preg_match('/\.png$|\.gif$|\.jpg$|\.jpeg$|\.bmp$/is',$filename) :
				$ret = 'image';
				break;
			case preg_match('/\.js$|\.json$/is',$filename) :
				$ret = 'javascript';
				break;
			case preg_match('/\.php$/is',$filename) :
				$ret = 'php';
				break;
			case preg_match('/\.htm$|\.html$|\.xhtml$/is',$filename) :
				$ret = 'html';
				break;
			case preg_match('/\.css$/is',$filename):
				$ret = 'css';
				break;
			case preg_match('/\.xml$/is', $filename):
				$ret = 'xml';
				break;
			case preg_match('/\.sql$/is',$filename):
				$ret = 'sql';
				break;
			case preg_match('/.swf$/is',$filename):
				$ret = 'flash';
				break;
			case preg_match('/.txt$/is',$filename):
				$ret = 'text';
				break;
		}

		return $ret;
	}

	protected function uploadFile() {
		$ret = array();
		$mandator = Encore::getModule('Mandator');
		if($mandator->hasPermission('writefiles',$_POST['path'])) {
			$ret['success'] = Encore::getModule('FileManager')->upload('upfile', $_POST['path']);
		} else {
			$ret['success'] = false;
			$ret['message'] = Encore::tr('ER_ACCESS_DENIED','admin');
		}
		return $ret;
	}
}