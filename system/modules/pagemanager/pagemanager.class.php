<?php

class PageManager extends EncoreModule {
	protected $current_page = null;


	public function init () {
		
		Encore::requireModules('PageManager', 'UrlParser, DB, Mandator');
		$args = Encore::getModule('UrlParser')->getScriptArguments();
		
		if ( isset($args['html']) ) {
			$alias = $args['html'];
			$this->current_page = $this->getPageByAlias($alias,true);
		}
	}

	public function currentPageId () {
		return ( $this->current_page == null ) ? null : $this->current_page['id'];
	}

	public function getCurrentPage () {
		return $this->current_page;
	}

	public static function getPageById ($id, $vars = false) {
		$db = Encore::getModule('DB');
		$localize = Encore::getOption('localize', Encore::getOption('default_localize'));
		$sid = (integer)($id);
		$fields = '`{pr}pages`.`id`,`{pr}pages`.`alias`,`{pr}pages`.`sort`,`{pr}pages`.`published`,`{pr}pages`.`accessable`,`{pr}pages`.`publishedon`,`{pr}pages`.`folder`,'
		         .'`{pr}pages`.`updatedon`, `{pr}pagetypes`.`id` AS `type`,`{pr}pagetypes`.`template`,`{pr}pagetypes`.`schema`,`{pr}content`.`preview`,`{pr}content`.`content`,'
		         .'`{pr}content`.`pagetitle`,`{pr}content`.`longtitle`,`{pr}content`.`seotitle`,`{pr}content`.`description`,`{pr}content`.`keys`,`{pr}pages`.`executable`';
		$query = "SELECT $fields FROM `{pr}pages`,`{pr}pagetypes`,`{pr}content` WHERE"
		        ." `{pr}pages`.`type` = `{pr}pagetypes`.`id` AND `{pr}content`.`page` = `{pr}pages`.`id` AND `{pr}content`.`localize` = '$localize'"
		        ." AND `{pr}pages`.`id` = '$sid' LIMIT 1";

		$page = $db->select($query);
		if ( $page != null ) {
			$page = $page[0];
			$page['schema'] = Encore::fromJson($page['schema']);
			if ( $vars ) {
				$page['vars'] = array();
				$query = "SELECT `{pr}contentvars`.`name`,`{pr}contentvars`.`value` FROM `{pr}pages`,`{pr}contentvars` WHERE"
					." `{pr}contentvars`.`page` = \"".$id."\" AND `{pr}contentvars`.`localize` = '$localize'";
				$pagevars = $db->select($query);
				if ( $pagevars != null ) {
					foreach ( $pagevars as $var ) {
						$page['vars'][$var['name']] = $var['value'];
					}
				}
			}
		}

		return $page;
	}

	public static function getForcePageById($id, $localize = null) {
		if($localize == null) {
			$localize = Encore::getOption('default_localize');
		}
		$db = Encore::getModule('DB');
		$sid = (integer)$id;
		$page = $db->select('SELECT `{pr}pages`.`id`,`{pr}pages`.`alias`,`{pr}pages`.`sort`,`{pr}pages`.`published`,`{pr}pages`.`accessable`,`{pr}pages`.`publishedon`,`{pr}pages`.`folder`,'
							.'`{pr}pages`.`updatedon`,`{pr}pages`.`executable`,`{pr}pagetypes`.`id` as `type`,`{pr}pagetypes`.`template`,`{pr}pagetypes`.`schema` FROM `{pr}pages`,`{pr}pagetypes`'
							.' WHERE `{pr}pages`.`type` = `{pr}pagetypes`.`id` AND `{pr}pages`.`id` = '.$sid.' LIMIT 1');
		if($page != null) {
			$page = $page[0];
			$content = $db->select('SELECT `{pr}content`.`preview`,`{pr}content`.`content`,`{pr}content`.`pagetitle`,`{pr}content`.`longtitle`,`{pr}content`.`seotitle`,`{pr}content`.`description`,`{pr}content`.`keys`'
									.' FROM `{pr}content` WHERE `{pr}content`.`page` = '.$sid.' AND `{pr}content`.`localize` = "'.$db->real_escape_string($localize).'" LIMIT 1');
			if($content != null) {
				$content = $content[0];
				foreach($content as $field=>$value) {
					$page[$field] = $value;
				}
			}
		}
		return $page;
	}

	public static function getPageByAlias ($alias, $vars = false) {		
		$db = Encore::getModule('DB');
		$localize = Encore::getOption('localize', 'ua');		
		$salias = $db->real_escape_string($alias);
		
		$fields = '`{pr}pages`.`id`,`{pr}pages`.`alias`,`{pr}pages`.`sort`,`{pr}pages`.`published`,`{pr}pages`.`accessable`,`{pr}pages`.`publishedon`,`{pr}pages`.`folder`,'
		         .'`{pr}pages`.`updatedon`, `{pr}pagetypes`.`id` AS `type`,`{pr}pagetypes`.`template`,`{pr}pagetypes`.`schema`,`{pr}content`.`preview`,`{pr}content`.`content`,'
		         .'`{pr}content`.`pagetitle`,`{pr}content`.`longtitle`,`{pr}content`.`seotitle`,`{pr}content`.`description`,`{pr}content`.`keys`,`{pr}pages`.`executable`';
		$query = "SELECT $fields FROM `{pr}pages`,`{pr}pagetypes`,`{pr}content` WHERE"
		        ." `{pr}pages`.`type` = `{pr}pagetypes`.`id` AND `{pr}content`.`page` = `{pr}pages`.`id` AND `{pr}content`.`localize` = '$localize'"
		        ." AND `{pr}pages`.`alias` = '$salias' LIMIT 1";
		$page = $db->select($query);

		if ( $page != null && count($page)>0) {
			$page = $page[0];
			$page['schema'] = Encore::fromJson($page['schema']);
			if ( $vars ) {
				$page['vars'] = array();
				$query = "SELECT `{pr}contentvars`.`name`,`{pr}contentvars`.`value` FROM `{pr}pages`,`{pr}contentvars` WHERE"
			    	    ." `{pr}contentvars`.`page` = {$page['id']} AND `{pr}contentvars`.`localize` = '$localize'";
				$pagevars = $db->select($query);
				if ( $pagevars != null ) {
					foreach ( $pagevars as $var ) {
						$page['vars'][$var['name']] = $var['value'];
					}
				}
			}
		}
		return $page;
	}

	public static function getPagesList($parent, $filter = '', $order = '', $limit = '') {
		$db = Encore::getModule('DB');
		$parent = (integer)$parent;
		$localize = Encore::getOption('localize');

		$fields = '`{pr}pages`.`id`,`{pr}pages`.`alias`,`{pr}pages`.`sort`,`{pr}pages`.`published`,`{pr}pages`.`accessable`,`{pr}pages`.`publishedon`,`{pr}pages`.`folder`,'
		         .'`{pr}pages`.`updatedon`, `{pr}pagetypes`.`id` AS `type`,`{pr}pagetypes`.`template`,`{pr}pagetypes`.`schema`,`{pr}content`.`preview`,`{pr}content`.`content`,'
		         .'`{pr}content`.`pagetitle`,`{pr}content`.`longtitle`,`{pr}content`.`seotitle`,`{pr}content`.`description`,`{pr}content`.`keys`,`{pr}pages`.`executable`';
		$query = "SELECT $fields FROM `{pr}pages`,`{pr}content`,`{pr}pagetypes`,`{pr}pagestree` WHERE `{pr}pages`.`type` = `{pr}pagetypes`.`id` AND `{pr}content`.`page` = `{pr}pages`.`id` AND `{pr}content`.`localize` = '$localize' AND `{pr}pages`.`id` = `{pr}pagestree`.`page_id` AND `{pr}pagestree`.`level` = 1 AND `{pr}pagestree`.`parent` = ".(integer)$parent;
		if (!empty($filter)) {
			$query .= ' AND '.$filter;
		}
		$query .= ' ORDER BY `{pr}pages`.`sort` ';
		if (!empty($order)) {
			$query .= ','.$order;
		}
		if(!empty($limit)) {
			$query .= ' LIMIT ' .$limit;
		}
		$items = $db->select($query);
		$ret = array();
		if($items != null && count($items) > 0 ) {
			foreach($items as $item) {
				$item['childs'] = null;
				$ret[$item['id']] = $item;
			}
		}
		return $ret;
	}

	public static function getPages($parent, $level = 0, $filter = '', $order = '', $limit = '') {
		$db = Encore::getModule('DB');
		$level = (integer)$level;
		$parent = (integer)$parent;
		$localize = Encore::getOption('localize');

		$fields = '`{pr}pages`.`id`,`{pr}pages`.`alias`,`{pr}pages`.`sort`,`{pr}pages`.`published`,`{pr}pages`.`accessable`,`{pr}pages`.`publishedon`,`{pr}pages`.`folder`,'
		         .'`{pr}pages`.`updatedon`, `{pr}pagetypes`.`id` AS `type`,`{pr}pagetypes`.`template`,`{pr}pagetypes`.`schema`,`{pr}content`.`preview`,`{pr}content`.`content`,'
		         .'`{pr}content`.`pagetitle`,`{pr}content`.`longtitle`,`{pr}content`.`seotitle`,`{pr}content`.`description`,`{pr}content`.`keys`,`{pr}pages`.`executable`,`tree`.`parent`,`{pr}pagestree`.`level`';

		$query = "SELECT $fields FROM (`{pr}pagestree`,`{pr}pages`,`{pr}pagetypes`,`{pr}content`) "
				."INNER JOIN `{pr}pagestree` as `tree` ON (`tree`.`page_id` = `{pr}pages`.`id` AND `tree`.`level` = 1)"
		        ."WHERE `{pr}pages`.`type` = `{pr}pagetypes`.`id` AND `{pr}content`.`page` = `{pr}pages`.`id` AND `{pr}content`.`localize` = '$localize'"
		        ." AND `{pr}pagestree`.`page_id` = `{pr}pages`.`id` AND `{pr}pagestree`.`parent` = '$parent'";
		if ($level > 0) {
			$query .= " AND `{pr}pagestree`.`level` <= '$level'";
		}
		if (!empty($filter)) {
			$query .= ' AND '.$filter;
		}

		$query .= ' ORDER BY `tree`.`level` ';
		if (!empty($order)) {
			$query .= ','.$order;
		} else {
			$query .= ', `sort` ';
		}

		if(!empty($limit)) {
			$query .= ' LIMIT ' .$limit;
		}

		$items = $db->select($query);

		$ret = array();
		$hash = array();
		$maxi = count($items);

  		for ($i = 0; $i< $maxi; $i++) {
			$hash[$items[$i]['id']] = $items[$i];
		}

		foreach ($hash as $id=>$item ) {
			if($hash[$id]['parent'] == $parent) {
				$ret[$id] = &$hash[$id];
			} else {
				if( !isset($hash[$hash[$id]['parent']]['childs']) ) {
					$hash[$hash[$id]['parent']]['childs'] = array();
				}
				$hash[$hash[$id]['parent']]['childs'][$id] = &$hash[$id];
			}
		}

		return $ret;
	}

	public static function getPagesCount ($parent, $level, $filter) {
		$db = Encore::getModule('DB');
		$level = (integer)$level;
		$parent = (integer)$parent;

		$query = "SELECT COUNT(`id`) as `count` FROM (`{pr}pages`,`{pr}pagestree`) "
		        ."INNER JOIN `{pr}pagestree` as `tree` ON (`tree`.`page_id` = `{pr}pages`.`id` AND `tree`.`level` = 1) "
		        ."WHERE `{pr}pagestree`.`page_id` = `{pr}pages`.`id` AND `{pr}pagestree`.`parent` = '$parent'" ;
		if ($level > 0) {
			$query .= " AND `{pr}pagestree`.`level` <= '$level'";
		}
		if (!empty($filter)) {
			$query .= ' AND '.$filter;
		}
		$result = $db->select($query);
		return ($result == null)? 0 : $result[0]['count'];
	}

	public static function getPagesVars($parent, $level = 0, $filter = '', $order = '', $limit = '') {
		// TODO
	}

	public static function getPageParents($page_id) {
		$db = Encore::getModule('DB');
		$page_id = $db->real_escape_string($page_id);
		$res = $db->select("SELECT `page_id`,`parent`,`level` FROM `{pr}pagestree` WHERE `page_id`='$page_id' ORDER BY `level`");
		$ret = null;
		if($res!=null){
			$ret = array();
			foreach ($res as $row ) {
				$ret[$row['parent']] = $row;
			}
		}
		return $ret;
	}
	
	public static function getPageParentId($page_id) {
		$db = Encore::getModule('DB');
		$page_id = $db->real_escape_string($page_id);
		$res = $db->select("SELECT `page_id`,`parent`,`level` FROM `{pr}pagestree` WHERE `page_id`='$page_id' ORDER BY `level` LIMIT 1");
		$ret = ($res==null) ? null : $res[0]['parent'];		
		return $ret;
	}

	public static function createPage($page) {
		$db = Encore::getModule('DB');
		$localize = Encore::getOption('localize');
		$author = Encore::getModule('UserAuth')->getUserData();
		$author = $author['id'];
		$publishedon = date('Y-m-d H:i:s',time());
		if(!isset($page['sort'])) $page['sort'] = 0;

		$db->exec("INSERT INTO `{pr}pages`(`alias`,`sort`,`type`,`published`,`accessable`,`publishedon`, `updatedon`,`author`,`executable`,`folder`)"
		  ." VALUES ('{$page['alias']}','{$page['sort']}','{$page['type']}','{$page['published']}','{$page['accessable']}','$publishedon','$publishedon','$author','{$page['executable']}','{$page['folder']}' )");

		$page['id'] = $db->last_id();
		$db->exec("INSERT INTO `{pr}content`(`page`,`localize`,`preview`,`content`,`pagetitle`,`longtitle`,`seotitle`,`description`,`keys`)"
		 ." VALUES ('{$page['id']}','$localize','{$page['preview']}','{$page['content']}','{$page['pagetitle']}','{$page['longtitle']}','{$page['seotitle']}','{$page['description']}','{$page['keys']}' )");

		$parents = self::getPageParents($page['parent']);
		if(count($parents)>0){
			foreach($parents as $parent) {
				$db->exec("INSERT INTO `{pr}pagestree`(`page_id`,`parent`,`level`) VALUES ('{$page['id']}','{$parent['parent']}','".(string)((integer)$parent['level']+1)."')");
			}
		} else {
			$db->exec("INSERT INTO `{pr}pagestree`(`page_id`,`parent`,`level`) VALUES ('{$page['id']}','0','".(string)(count($parents)+1)."')");
		}
		if($page['parent']!=0) {
			$db->exec("INSERT INTO `{pr}pagestree`(`page_id`,`parent`,`level`) VALUES ('{$page['id']}','{$page['parent']}','1')");
		}


		$type = self::getPageType($page['type']);

		if(isset($type['schema'])) {
			$schema = (array)Encore::fromJson($type['schema']);
			foreach($schema as $key=>$varitem) {
				$db->exec("INSERT INTO `{pr}contentvars`(`page`,`name`,`value`,`localize`) VALUES ('{$page['id']}','$key','','$localize')");
			}
		}

		return $page['id'];
	}

	public static function updatePage($page) {
		$db = Encore::getModule('DB');
		$localize = Encore::getOption('localize');
		if(!isset($page['sort'])) $page['sort'] = 0;
		$db->exec("UPDATE `{pr}pages` SET `alias`='{$page['alias']}',`sort`='{$page['sort']}', `type`='{$page['type']}',`published`='{$page['published']}',`accessable`='{$page['accessable']}',"
		 ."`executable`='{$page['executable']}',`folder`='{$page['folder']}',`updatedon`='".date('Y-m-d H:i:s',time())."' "
		 ." WHERE `id`='{$page['id']}'");

		$res = $db->select('SELECT `localize` FROM `{pr}content` WHERE `{pr}content`.`page` = '.(integer)$page['id'].' AND `{pr}content`.`localize` = "'.$localize.'" LIMIT 1');
		if($res != null & count($res == 1)) {
			$db->exec("UPDATE `{pr}content` SET `preview`='{$page['preview']}',`content`='{$page['content']}',"
			."`pagetitle`='{$page['pagetitle']}',`longtitle`='{$page['longtitle']}',`seotitle`='{$page['seotitle']}',`description`='{$page['description']}',`keys`='{$page['keys']}' "
			." WHERE `page`='{$page['id']}' AND `localize`='$localize'");
		} else {
			$db->exec("INSERT INTO `{pr}content`(`page`,`localize`,`preview`,`content`,`pagetitle`,`longtitle`,`seotitle`,`description`,`keys`)"
			." VALUES ('{$page['id']}','$localize','{$page['preview']}','{$page['content']}','{$page['pagetitle']}','{$page['longtitle']}','{$page['seotitle']}','{$page['description']}','{$page['keys']}' )");

		}

		/// VARS BY TYPE ??????????????????????????????????
	}

	public static function deletePage($id) {
		$db = Encore::getModule('DB');
		$ra = array($id);
		$childs = self::getPages($id);
		if(count($childs)>0) {
			foreach($childs as $child){
				$ra[] = $child['id'];
			}
		}
		$rin = AString::from('')->join($ra,',');
		$db->exec("DELETE FROM `{pr}contentvars` WHERE `page` IN ($rin)");
		$db->exec("DELETE FROM `{pr}content` WHERE `page` IN ($rin)");
		$db->exec("DELETE FROM `{pr}pagestree` WHERE `page_id` IN ($rin)");
		$db->exec("DELETE FROM `{pr}pages` WHERE `id` IN ($rin)");

	}

	public static function getPageLocales($id) {
		$ret = null;
		$db = Encore::getModule('DB');
		$result = $db->select("SELECT `{pr}content`.`localize` FROM `{pr}content` WHERE `{pr}content`.`page` = ".(integer)$id);
		if($result!=null&&count($result > 0)) {
			$ret = array();
			foreach($result as $val) {
				$ret[] = $val['localize'];
			}
		}
		return $ret;
	}

	public static function getPageVars($id) {
		$ret = array();
		$localize = Encore::getOption('localize');
		$db = Encore::getModule('DB');
		$result = $db->select("SELECT `name`,`value` FROM `{pr}contentvars` WHERE `localize`='{$localize}' AND `page`='".(integer)$id."'");
		if($result!=null) {
			foreach ($result as $row) {
				$ret[$row['name']] = $row['value'];
			}
		}
		return $ret;
	}

	public static function updatePageVars($id, $vars) {
		$ret = false;
		if(is_array($vars)) {
			$db = Encore::getModule('DB');
			$localize = Encore::getOption('localize');
			$db->exec("DELETE FROM `{pr}contentvars` WHERE `localize` = '$localize' AND `page`='".(integer)$id."'");
			foreach ($vars as $name=>$value) {
				$val = $db->real_escape_string($value);
				$db->exec("INSERT INTO `{pr}contentvars` (`page`,`localize`,`name`,`value`) VALUES ('".(integer)$id."','$localize','$name','$val') ");
			}
		}
		return $ret;
	}

	public static function getPageType($type_id) {
		$db = Encore::getModule('DB');
		$ret = $db->select("SELECT `id`,`name`,`template`,`schema` FROM `{pr}pagetypes` WHERE `id`='$type_id' LIMIT 1");
		return ($ret == null) ? null : $ret[0];
	}


	public static function getPageTypes() {
		$ret = null;
		$db = Encore::getModule('DB');
		return $db->select('SELECT `id`,`name`,`template`,`schema` FROM `{pr}pagetypes`');
	}

	public static function createPageType($pt) {
		$db = Encore::getModule('DB');
		return $db->exec("INSERT INTO `{pr}pagetypes`(`name`,`template`,`schema`) VALUES ('{$pt['name']}','{$pt['template']}','{$pt['schema']}')");
	}

	public static function updatePageType($pt) {
		$db = Encore::getModule('DB');
		return $db->exec("UPDATE `{pr}pagetypes` SET `name` = '{$pt['name']}',`template` = '{$pt['template']}',`schema` = '{$pt['schema']}' WHERE `id` = {$pt['id']}");
	}

	public static function deletePageType($id) {
		$db = Encore::getModule('DB');
		$block = $db->select("SELECT `{pr}pages`.`id` FROM `{pr}pages` WHERE `type` = '".(integer)$id."'");
		$ret = false;
		if($block==null) {
			$ret = $db->exec("DELETE FROM `{pr}pagetypes` WHERE `id`='".(integer)$id."'");
		}
		return $ret;
	}

	public function selectsOfPage($pages, $value, $level = 0, $disabling  = 0) {
		$margin = AString::from('')->fill('+&nbsp;&nbsp;',$level);
		$ret = '';
		foreach($pages as $key => $page) {
			$title = ((empty($page['pagetitle'])) ? $page['alias']: $page['pagetitle'] );
			$active = ($key==$value) ? ' selected="" ': '';
			$disabled = ( $level < $disabling ) ? ' disabled="" ':'';
			$ret .= '<option value="'.$key.'" '.$active.$disabled.'>'.$margin.$title.'</option>';
			if(isset($page['childs']) && count($page['childs'] > 0)) {
				$ret .= $this->selectsOfPage($page['childs'], $value,$level+1 );
			}
		}
		return $ret;
	}
}