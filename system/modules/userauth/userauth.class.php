<?php


class UserAuth extends EncoreModule {
	protected $current_user_data = null;
	protected $error_message = null;
	protected $salt = '***';
	
	public function init() {
		Encore::requireModules('UserAuth', 'DB');
		
		$this->salt = Encore::getOption('SALT','***','USERAUTH');
		
		Encore::connect('sessionLogin', array($this,'login'));
		Encore::connect('sessionLogout', array($this,'logout'));
		Encore::connect('beforeSessionStart', array($this,'initUserData'));
	}
	
	public function login($arg) {
		$db = Encore::getModule('DB');
		$login = $db->real_escape_string($_POST['login']);
		$res = $db->select("SELECT `id`,`login`,`password`,`name`,`email` FROM `{pr}users` WHERE `login` = '$login' LIMIT 1");
		if( $res!=null ) {
			if(isset($_POST['password']) && $res[0]['password']==md5(md5($_POST['password']).$this->salt)){				
				$session_key = $db->real_escape_string($arg['session_key']);
				$sres = $db->select("SELECT `session_id`,`user`,`expire` FROM `{pr}sessions` WHERE `user` = '{$res[0]['id']}' AND `expire` > '".date('Y-m-d H:i:s', time())."'");				
				$ok = true;
				$expire = date('Y-m-d H:i:s', time()+Encore::getOption('session_expire',3600));
				if($sres==null) {					
					if(!$db->exec("INSERT INTO `{pr}sessions`(`session_id`,`user`,`expire`) VALUES ('$session_key','{$res[0]['id']}','$expire')")){
						throw new AException(Encore::tr('ER_CREATE_USER_SESSION'));
					};				
				} else {					
					if(Encore::getOption('MULTIAUTH',false,'USERAUTH')) {
						$sok = true;
						if(isset($_COOKIE['session_key'])) {
							foreach($sres as $session_item ) {
								if($session_item['session_id'] == $arg['session_key']) {
									$sok = false;
									break;
								}
							}
						} 
						if($sok) {
							if(!$db->exec("INSERT INTO `{pr}sessions`(`session_id`,`user`,`expire`) VALUES ('$session_key','{$res[0]['id']}','$expire')")){
								throw new AException(Encore::tr('ER_CREATE_USER_SESSION'));
							};
						}
					} else {
						if(Encore::getOption('PARANOID',false,'USERAUTH')) {
							$this->error_message = Encore::tr('ER_MULTIAUTH');
							$arg['flag'] = false;
							Encore::log('Login method','USERAUTH MODULE',AString::from('WARNING_USER_MULTIAUTH')->arg('$login')->arg($_SERVER['REMOTE_ADDR']),2);
							$ok = false;
						} else {
							if(!(isset($_SESSION['session_key'])&& $_SESSION['session_key'] == $sres[0]['session_id'])) {
								$arg['session_key'] = $sres[0]['session_id'];													
								$db->exec("UPDATE `{pr}sessions` SET `expire`='$expire' WHERE `session_id` = '{$sres[0]['session_id']}'");
								$session_cookie_key = self::getOption('session_cookie_key','session_key');
								$expire = self::getOption('session_expire',3600);	
								setcookie($session_cookie_key,$sres[0]['session_id'],(time()+$expire),'/');
							} else {
								if(!$db->exec("INSERT INTO `{pr}sessions`(`session_id`,`user`,`expire`) VALUES ('$session_key','{$res['id']}','$expire')")){
									throw new AException(Encore::tr('ER_CREATE_USER_SESSION'));
								};
							}							
						}						
					}
				}
				if($ok) { 
					Encore::log('Login method','USERAUTH MODULE',AString::from('DEBUG_USER_LOG')->arg('$login'),2);
					$this->current_user_data = $res[0];
					unset($this->current_user_data['password']);
				}								
			} else {
				$arg['flag'] = false;
				$this->error_message = Encore::tr('ER_FAILURE_PASSWORD');
			}
		} else {
			$arg['flag'] = false;
			$this->error_message = Encore::tr('ER_UNKNOWN_USERNAME');
		}
	}	
	
	public function logout($arg) {
		$db = Encore::getModule('DB');
		$session_key = $db->real_escape_string($arg['session_key']);
		$db->exec("DELETE FROM `{pr}sessions` WHERE `session_id` = '$session_key'");
		Encore::cacheDelete('session.userdata.'.$session_key);
	}
	
	public function getErrorMessage() {
		$ret = $this->error_message;
		$this->error_message = null;
		return $ret;
	}
	
	public function getUserData() {
		return $this->current_user_data;
	}
	
	public function initUserData ( $arg ) {		
		$session_key = $_COOKIE['session_key'];
		$this->current_user_data = Encore::cacheGet('session.userdata.'.$session_key);
		if ( $this->current_user_data == null ) {
			$db = Encore::getModule('DB');
			$session_id = $db->real_escape_string($session_key);
			$expire = date('Y-m-d H:i:s', time()+Encore::getOption('session_expire',3600));
			$sres = $db->select("SELECT `user` FROM `{pr}sessions` WHERE `session_id` = '$session_id' LIMIT 1");
			if ( $sres == null ) {
				$arg['flag'] = false;
			} else {
				$db->exec("UPDATE `{pr}sessions` SET `expire`='$expire' WHERE `session_id` = '$session_id'");				
				$ret = $db->select("SELECT `id`,`login`,`name`,`email` FROM `{pr}users` WHERE `id`='{$sres[0]['user']}' LIMIT 1");
				$this->current_user_data = $ret[0];
				Encore::cacheSet('session.userdata.'.$session_key, $this->current_user_data);
			}
		}
	}
}