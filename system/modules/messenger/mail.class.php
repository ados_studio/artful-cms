<?php

class Mail {
	const MODE_AUTO  = 0;
	const MODE_MAIL  = 1;
	const MODE_SMTP  = 2;
	const TYPE_TEXT  = 0;
	const TYPE_HTML  = 1;
	const INLINE     = 0;
	const ATTACHMENT = 1;
	const HIGHEST    = 1;
	const HIGH       = 2;
	const NORMAL     = 3;
	const LOW        = 4;
	const LOWEST     = 5;

	protected $sendto = array();
	protected $smtpsendto = array();
	protected $acc = array();
	protected $abcc = array();
	protected $aattach = array();
	protected $xheaders = array();
	protected $mode = 0;
	protected $type = 0;
	protected $ctencoding = '8bit';
	protected $checkAddress = true;
	protected $charset = null;
	protected $receipt = null;
	protected $message = '';
	protected $body = '';
	protected $headers = '';
	protected $log = '';

	protected $errors = array();

	public function __construct ($message = '') {
		Encore::requireModules('Mail class','UserManager');
		$this->charset = AString::from(Encore::getOption('charset'))->toLower()->toString();
		if ( $this->charset == 'us-ascii' ) {
			$this->ctencoding = '7bit';
		}
		$this->body = $message;
	}

	public function setSubject($subject) {
		$subject = AString::from($subject)->replace("\n"," ")->replace("\r"," ")->/*urlencode()->*/replace('+','_')->replace('%','=');
		$this->xheaders['subject'] = AString::from('=?%1?B?%2?=')->arg($this->charset)->arg( base64_encode($subject->toString()));
	}

	public function setFrom($from) {
		if( is_string($from) ) {
			$this->xheaders['From'] = $from;
		} else {
			$this->errors[] = 'setFrom: $from must be a string';
		}
	}

	public function setFromUser($id) {
		$user = Encore::getModule('UserManager')->getUserById($id);
		if ( $user != null ) {
			$this->xheaders['From'] = $user['email'];
		} else {
			$this->errors[] = 'setFromUser: can\'t find user by id';
		}
	}

	public function setReplyAddress($addr) {
		$ret = true;
		if( is_string($addr) ) {
			$this->xheaders["Reply-To"] = $addr;
		} else {
			$ret = false;
		}
		return $ret;
	}

	public function setReceipt($val = 1)
	{
		$ret = true;
		if($val == 1 || $val == 0 ) {
			$this->receipt = 1;
		} else {
			$ret = false;
		}
		return $ret;
	}

	public function setAddress( $address ) {
		$ret = true;
		if($this->checkAddress) {
			$ret = $this->verifyAddress($address);
		}
		if($ret) {
			if(is_array($address) ) {
				foreach($address as $addr) {
					$this->sendto[$addr] = $addr;
					$this->smtpsendto[$addr]= $addr;
				}
			} else {
				$this->sendto[$address] = $address;
				$this->smtpsendto[$address]= $address;
			}
		}
		return $ret;
	}

	public function setCc ( $cc ) {
		$ret = true;
		if($this->checkAddress) {
			$ret = $this->verifyAddress($cc);
		}
		if($ret) {
			if(is_array($cc) ) {
				foreach($cc as $addr) {
					$this->sendto[$addr] = $addr;
					$this->smtpsendto[$addr]= $addr;
				}
			} else {
				$this->acc[$cc] = $cc;
				$this->smtpsendto[$cc]= $cc;
			}
		}
		return $ret;
	}

	public function setBcc ( $bcc ) {
		$ret = true;
		if($this->checkAddress) {
			$ret = $this->verifyAddress($bcc);
		}
		if($ret) {
			if(is_array($bcc) ) {
				foreach($bcc as $addr) {
					$this->sendto[$addr] = $addr;
					$this->smtpsendto[$addr]= $addr;
				}
			} else {
				$this->abcc[$bcc] = $bcc;
				$this->smtpsendto[$bcc]= $bcc;
			}
		}
		return $ret;
	}

	public function setBodyType ( $type ) {
		$ret = false;
		if( is_int($type) && ( $type == 0 || $type == 1 ) ) {
			$this->type = $type;
			$ret = true;
		}
		return $ret;
	}

	public function setMessageBody ( $body ) {
		$this->body = $body;
	}

	public function setMode ( $mode ) {
		$ret = false;
		if ( is_int($mode) && $mode > -1 && $mode < 3 ) {
			$this->mode = $mode;
			$ret = true;
		}
		return $ret;
	}

	public function setOrganization( $org ) {
		if( AString::from($org)->simplified()->toString() != "" ) {
			$this->xheaders['Organization'] = $org;
		}
	}

	public function setPriority( $pr ) {
		$ret = false;
		if( is_int($pr) && $pr > 0 && $pr < 6 ) {
			$prtypes = array( '1 (Highest)', '2 (High)', '3 (Normal)', '4 (Low)', '5 (Lowest)' );
			$this->xheaders["X-Priority"] = $prtypes[$pr - 1];
			$ret = true;
		}
		return $ret;
	}

	public function attach ( $filename, $inmailname, $filetype, $dispos = 0 ) {
		$ret = false;
		$filename = AString::from($filename)->simplified()->toString();
		$inmailname = AString::from($inmailname)->simplified()->toString();
		if ( $filename != '' && is_int($dispos) && ($dispos == 0 || $dispos == 1)) {
			if( AString::from($filetype)->simplified()->toString() == '') {
				$filetype = "application/x-unknown-content-type";
			}
			$this->aattach[] = array(
				'filename' => $filename,
				'inmailname' => ($inmailname != '') ? $inmailname : $filename,
				'type' => $filetype,
				'disposition' => $dispos
				);
		}
		return $ret;
	}

	public function send() {

		$ret = $this->buildMail();

		if ($ret ) {
			$ret = false;
			$addrlist = AString::from('')->join($this->sendto,', ');
			$smtp_on = false;
			if( $this->mode == self::MODE_AUTO ) {
				$smtp_on = (Encore::existsOption('smtp_host') && Encore::existsOption('smtp_port') && Encore::existsOption('smtp_login') && Encore::existsOption('smtp_pass'));
			} else {
				$smtp_on = ($this->mode == self::MODE_SMTP);
			}

			if(!$smtp_on) {
				try {
					//error_reporting(E_ALL);
					mail( $addrlist, $this->xheaders['subject'], $this->message, $this->headers );
					//echo('<pre>'.$addrlist.'<-'.$this->headers."\r\n".$this->xheaders['subject']."\r\n".$this->message.'</pre>');
					$ret = true;
				} catch(Exception $e) {
					$this->errors[] = 'send: '.$e->getMessage();
				}
			} else {
				$user_domen = AString::from($this->xheaders['From'])->split('@');
				$this->log = '';
				$errno = null;
				$errstr = null;
				$smtp_conn = fsockopen(Encore::getOption('smtp_host'), Encore::getOption('smtp_port'), $errno, $errstr, (Encore::existsOption('smtp_timeout')) ? (integer)Encore::getOption('smtp_timeout') : 5 );

				if ( $smtp_conn == false ) {
					$this->errors[] = 'send: Error '.$errno.'. '.$errstr;
					fclose($smtp_conn);
				} else {
					if(substr(PHP_OS, 0, 3) != "WIN")
						socket_set_timeout($this->smtp_conn, $tval, 0);

					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";
					fputs($smtp_conn,"EHLO ".$user_domen[0]."\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."Me: EHLO ".$user_domen[0]."\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";
					$code = substr($data,0,3); // получаем код ответа

					if($code != 250) {
						$this->log .= date('d.m.Y h:i:s >> ')."Error in EHLO answer \n"; fclose($smtp_conn); return false;
					}

					fputs($smtp_conn,"AUTH LOGIN\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."Me: AUTH LOGIN\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";
					$code = substr($data,0,3);

					if($code != 334) {
						$this->log .= date('d.m.Y h:i:s >> ')."Auth blocked by server \n"; fclose($smtp_conn); return false;
					}

					fputs($smtp_conn,base64_encode(Encore::getOption('smtp_login')."\r\n"));
					$this->log .= date('d.m.Y h:i:s >> ')."Me: ".base64_encode(Encore::getOption('smtp_login'))."\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn, true)."\n";

					$code = substr($data,0,3);
					if($code != 334) {
						$this->log .= date('d.m.Y h:i:s >> ')."Access denied from this user\n"; fclose($smtp_conn); return false;
					}


					fputs($smtp_conn,base64_encode(Encore::getOption('smtp_pass'))."\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."Me: ". base64_encode(Encore::getOption('smtp_pass'))."\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";

					$code = substr($data,0,3);
					if($code != 235) {
						$this->log .= date('d.m.Y h:i:s >> ')."Wrong password\n"; fclose($smtp_conn); return false;
					}

					fputs($smtp_conn,"MAIL FROM:<".$this->xheaders['From']."> SIZE=".strlen($this->headers."\r\n".$this->message)."\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."Me: MAIL FROM:<".$this->xheaders['From']."> SIZE=".strlen($this->headers."\r\n".$this->message)."\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";

					$code = substr($data,0,3);
					if($code != 250) {
						$this->log .= date('d.m.Y h:i:s >> ')."Server has blocked command MAIL FROM\n"; fclose($smtp_conn); return false;
					}


					foreach ($this->smtpsendto as $keywebi => $valuewebi)
					{
						fputs($smtp_conn,"RCPT TO:<".$keywebi.">\r\n");
						$this->log .= date('d.m.Y h:i:s >> ')."Me: RCPT TO:<".$keywebi.">\n";
						$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";
						$code = substr($data,0,3);
						if($code != 250 AND $code != 251) {
							$this->log .= date('d.m.Y h:i:s >> ')."Server has blocked command RCPT TO\n"; fclose($smtp_conn); return ;
						}
					}

					fputs($smtp_conn,"DATA\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."Me: DATA\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";

					$code = substr($data,0,3);
					if($code != 354) {
						$this->log .= date('d.m.Y h:i:s >> ')."Server has blocked command DATA\n"; fclose($smtp_conn); return ;
					}

					fputs($smtp_conn,$this->headers."\r\n".$this->message."\r\n.\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."Me: ".$this->headers."\r\n".$this->message."\r\n.\r\n";

					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";

					$code = substr($data,0,3);
					if($code != 250) {
						$this->log .= date('d.m.Y h:i:s >> ')."Error on mail sending\n"; fclose($smtp_conn); return ;
					}

					fputs($smtp_conn,"QUIT\r\n");
					$this->log .= date('d.m.Y h:i:s >> ')."QUIT\r\n";
					$this->log .= date('d.m.Y h:i:s >> ').$data = $this->get_data($smtp_conn)."\n";
					fclose($smtp_conn);
					$ret = true;
				}
			}
		}
		return $ret;
	}

	public function getLog() {
		return $this->log;
	}

	protected function buildMail() {
		$ret = false;
		if(count($this->sendto) > 0) {
			$this->xheaders['To'] = AString::join(array_keys($this->sendto),', ')->toString();
			if( count($this->acc) > 0 ) {
				$this->xheaders['CC'] = AString::join(array_keys($this->acc),', ')->toString();
			}
			if( count($this->abcc) > 0 ) {
				$this->xheaders['BCC'] = AString::join(array_keys($this->abcc),', ')->toString();
			}

			if($this->receipt == 1) {
				if( isset($this->xheaders['Reply-To']) ) {
					$this->xheaders["Disposition-Notification-To"] = $this->xheaders["Reply-To"];
				} else {
					$this->xheaders["Disposition-Notification-To"] = $this->xheaders['From'];
				}
			}

			$this->xheaders["Mime-Version"] = "1.0";
			$this->xheaders["Content-Type"] = (($this->type == self::TYPE_TEXT)?'text/plain':'text/html')."; charset=$this->charset";
			$this->xheaders["Content-Transfer-Encoding"] = $this->ctencoding;

			$this->xheaders["X-Mailer"] = "PHP Forbiz Messenger Module";

			if ( count($this->aattach) > 0 ) {
				$this->buildAttachments();
			} else {
				$this->message = $this->body;
			}

			$smtp_on = false;
			if ( $this->mode == self::MODE_AUTO ) {
				$smtp_on = (Encore::existsOption('smtp_host') && Encore::existsOption('smtp_port') && Encore::existsOption('login') && Encore::existsOption('smtp_pass'));
			} else {
				$smtp_on = ($this->mode == self::MODE_SMTP);
			}

			if($smtp_on) {
				$user_domen = AString::from($this->xheaders['From'])->split('@');
				$this->headers = 'Date: '.date('D, j M Y G:i:s')." +0700\r\n";
				$this->headers .= 'Message-ID: <'.rand().'.'.date('YmjHis').'@'.$user_domen[1].">\r\n";
				foreach($this->xheaders as $hdr=>$value) {
					if($hdr != 'BCC') {
						$this->headers .= $hdr.': '.$value."\r\n";
					}
				}
			} else {
				foreach($this->xheaders as $hdr=>$value) {
					if($hdr != 'Subject' && $hdr != 'To') {
						$this->headers .= $hdr.': '.$value."\r\n";
					}
				}
			}
			$ret = true;
		}
		return $ret;
	}

	protected function get_data($smtp_conn, $flag = false) {
		$data="";
		while($str = fgets($smtp_conn,515))
		{
			$data .= $str;
			if(substr($str,3,1) == " ") { break; }
			if($flag) return;
		}
		return $data;
	}

	protected function buildAttachments() {
		$this->xheaders['Content-Type'] = "multipart/mixed;\n boundary=\"$this->boundary\"";

		$this->message = "This is a multi-part message in MIME format.\n--$this->boundary\n";
		$this->message .= "Content-Type: ".($this->type == self::TYPE_TEXT)?'text/plain':'text/html'."; charset=$this->charset\nContent-Transfer-Encoding: $this->ctencoding\n\n" . $this->body ."\n";

		$sep= chr(13) . chr(10);

		$ata= array();
		$k=0;

		for( $i=0; $i < count( $this->aattach); $i++ ) {

			$filename = $this->aattach[$i]['filename'];

			$webi_filename =$this->aattach[$i]['inmailname'];
			if(strlen($webi_filename)) $basename=basename($webi_filename); // если есть другое имя файла, то оно будет таким
			else $basename = basename($filename); // а если нет другого имени файла, то имя будет выдернуто из самого загружаемого файла

			$ctype = $this->aattach[$i]['type'];	// content-type
			$disposition = $this->aattach[$i]['disposition'];

			if( ! file_exists( $filename) ) {
				$this->errors[] =  "Error by attach file : File '$filename' not found";
			} else {
				$subhdr= "--$this->boundary\nContent-type: $ctype;\n name=\"$basename\"\nContent-Transfer-Encoding: base64\nContent-Disposition: $disposition;\n  filename=\"$basename\"\n";
				$ata[$k++] = $subhdr;
				$linesz= filesize( $filename)+1;
				$fp= fopen( $filename, 'r' );
				$ata[$k++] = chunk_split(base64_encode(fread( $fp, $linesz)));
				fclose($fp);
			}
		}
		$this->fullBody .= implode($sep, $ata);
	}

	protected function verifyAddress ( $addr ) {
		$ret = true;
		if(is_array($addr) ) {
			foreach($addr as $ad) {
				$ret = Validator::is_email($ad);
				if(!ret) break;
			}
		} elseif(is_string($addr)) {
			$ret = Validator::is_email($addr);
		} else {
			$ret = false;
		}
		return $ret;
	}

}