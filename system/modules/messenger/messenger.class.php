<?php

class Messenger extends EncoreModule {

	public function __construct() {
		Encore::requireModules('Messenger','UserManager');
	}

	public function init() {

	}

	public static function mail($address, $title, $message, $sender=null){
		/*$charset = Encore::getOption('charset');
		 *		$to = htmlspecialchars($address);
		 *		$subject = htmlspecialchars($title);
		 *		$msg = $message;
		 *		$from = $sender;
		 *		if ( $sender == null || !is_string($sender) ) {
		 *			$userdata = Encore::getModule('UserAuth')->getUserData();
		 *			$from = ($userdata == null) ? Encore::getOption('default_email') : $userdata['email'];
	}
	return mail($to, $subject, $msg, 'From:'.$from. "\r\n" . "MIME-Version: 1.0\r\nContent-type: text/html; charset=".$charset);*/
		$from = $sender;
		if ( $sender == null || !is_string($sender) || get_class($sender) == 'AString') {
			//$userdata = Encore::getModule('UserAuth')->getUserData();
			//$from = ($userdata == null) ? Encore::getOption('default_email') : $userdata['email'];
			$from = Encore::getOption('default_email');
		}

		require_once ('mail.class.php');

		$mail = new Mail($message);
		$mail->setAddress($address);
		$mail->setBodyType(Mail::TYPE_HTML);
		$mail->setFrom(Encore::getOption('default_email'));
		$mail->setSubject($title);
		$mail->send();
		//echo($from.'->'.$address);
		Encore::log('Messenger module','mail function','<pre>'.$mail->getLog().'</pre>',2);
	}

	public static function mailToUser($id, $title, $message, $sender=null) {
		$user = Encore::getModule('UserManager')->getUser($id);
		if($user != null) {
			self::mail($user['email'], $title, $message, $sender);
		} else {
			Encore::log('Messenger module','mailToUser function','Undefined user ID',1);
		}
	}

	public static function createMessage($message) {
		$ret = false;
		if(!isset($message['readflug'])) {
			$message['readflug'] = false;
		}
		if(is_array($message) && Encore::isSetKeys($message, array('from','to','theme','message'))) {
			$db = Encore::getModule('DB');
			$ret = $db->exec('INSERT INTO `{pr}messages`(`from`,`to`,`theme`,`message`,`readflug`)'
			."VALUE ('".(integer)$message['from']."','".(integer)$message['to']."','".$db->real_escape_string($message['theme'])."','".$db->real_escape_string($message['message'])."','".(integer)$message['readflug']."')");
		}
		return $ret;
	}

	public static function getMessage($id) {
		$ret = Encore::getModule('DB')->select('SELECT * FROM `{pr}messages` WHERE `id` = '.(integer)$id);
		if($ret != null) {
			$ret = $ret[0];
		}
		return $ret;
	}

	public static function readMessage($id) {
		$msg = self::getMessage($id);
		$msg['readflug'] = true;
		self::updateMessage($msg);
		return $msg;
	}

	public static function getMessages($filter = null) {
		$where = (AString::from($filter)->isEmpty())? '' : ' WHERE '.$filter ;
		return Encore::getModule('DB')->select('SELECT * FROM `{pr}messages` '.$where);
	}

	public static function updateMessage($message, $id = null) {
		if($id!=null && is_integer($id) ) {
			$message['id'] = $id;
		}
		return Encore::getModule('DB')->exec("UPDATE `{pr}messages` SET `from` = '{$message['from']}', `to` = '{$message['to']}', `theme` = '{$message['theme']}', `message` = '{$message['message']}', `readflug` = '{$message['readflug']}' WHERE `id` = ".(integer)$message['id']);
	}

	public static function deleteMessage($id) {
		return Encore::getModule('DB')->exec('DELETE FROM `{pr}messages` WHERE `id` = '.(integer)$id);
	}

	public static function deleteMesages($idlist) {
		if(is_array($idlist)) {
			$idlist = AString::from('')->join($idlist,',');
		}

		return Encore::getModule('DB')->exec('DELETE FROM `{pr}messages` WHERE `id` IN ('.$idlist.')');
	}
}
