<?php

class Scheduler extends EncoreModule {
	protected $mode = null;
	protected $config = null;
	protected $configpath = null;

	public function init() {
		$this->mode = Encore::getOption('MODE','php','SCHEDULER');
		$this->configpath = Encore::getOption('CONFIG_PATH', CONFIGS_PATH.'scheduler.json','SCHEDULER');
		$this->readConfig();

	
	}

	public function addTask($name,$task) {
		// TODO
	}

	public function updateTask($name,$task) {
		// TODO	
	}

	public function deleteTask($taskname) {
		// TODO
	}

	public function runTask() {
		
	}

	public function cronSync() {
		// TODO
	}

	protected function readConfig() {
		if ( file_exists($this->configpath) ) {
			$content = fgets($this->configpath);
			$this->config = (array)Encore::fromJson($content);
		} else {
			$this->config = array();
			$this->writeConfig();
		}
	}

	protected function writeConfig() {
		$content = Encore::toJson($this->config);
		//fputs($this->configpath,$content,);
	}

	
}

?>