<?php


class JSTranslator extends EncoreModule {

	public function init() {
		Encore::requireModules('JSTranslator', 'Render');
		Encore::connect('afterInitModule', array($this,'connectorInit'));
	}

	public function connectorInit($arg) {
		if($arg['name']!='Render') return;
		$render = $arg['object'];
		$render->connect('beforeRender', array($this,'registerJS'));
	}

	public function registerJS($arg) {
		$render = Encore::getModule('Render');
		$op = array();
		$contexts = Encore::getTranslationContexts();

		foreach($contexts as $cname) {
			$langs = Encore::getTranslationContextLangs($cname);
			foreach($langs as $lname) {
				$op[] = "$cname-$lname";
			}
		}

		$render->registerJavaScript(htmlspecialchars('/translator.js.php?localize='.Encore::getOption('localize').'&dict=' . AString::from('')->join($op,'_')));
	}

}