<?php

require_once("lessc.inc.php");

class Less extends EncoreModule {
	public function init() {
		$this->registerSignals(array("beforeCompile", "afterCompile", "beforeCompileAll", "afterCompileAll"));
		
		$this->less_service = new lessc;
		
		$this->compileAll();
	}
	
	public function compileAll() {
		if ($this->less_service == null)
			throw(new AException(Encore::tr("Error! Less module was not initialized."), 0, __FILE__, __LINE__));
		
		$compileList = Encore::getOption("COMPILE_LIST", null, "LESS");
		
		$allowFlag = true;
		$this->emit("beforeCompileAll", array(&$allowFlag, $compileList));
		if ($allowFlag == true) {
			
			if (is_array($compileList))
			{
				foreach($compileList as $compileItem) {
					$this->compile($compileItem['source'], $compileItem['destination']);
				}
				$this->emit("afterCompileAll", array($compileList));
			}
			else
			{
				throw(new AException(Encore::tr("Error! Invalid LESS.COMPILE_LIST option."), 0, __FILE__, __LINE__));
			}
		}
	}
	
	public function compile($source, $destination) {
		if ($this->less_service == null)
			throw(new AException(Encore::tr("Error! Less module was not initialized."), 0, __FILE__, __LINE__));
		
		$allowFlag = true;
		$this->emit("beforeCompile", array(&$allowFlag, $source, $destination));
		if ($allowFlag == true) {
			if ($this->less_service->checkedCompile($source, $destination))
				$this->emit("afterCompile", array($source, $destination));
		}
	}
	
	public function getLessService() {
		return $this->less_service;
	}
	
	protected $less_service = null;
}
