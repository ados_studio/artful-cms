<?php
//


class ArtTranslator {
	protected $srcdir;
	protected $renderdir;
	protected $artchars;
	protected $meta;
	protected $errors;
	protected $filename;

	public function __construct ( $inpath, $outpath ) {
		$this->srcdir = $inpath;
		$this->renderdir = $outpath;
		$this->artchars = AString::from(' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_');
		$this->init_data();
	}

	public function init_data() {
		$this->meta = array(
			'code' => array(),
			'vars' => array()
			);
			$this->errors = array();
	}

	public function translatefile ($name) {
		$this->init_data();
		$this->filename = $name;
		$artfilepath = $this->srcdir.$name.'.art';
		$phpfilepath = $this->renderdir.$name.'.php';
		if(file_exists($artfilepath)) {
			if(!file_exists($phpfilepath) || filemtime($phpfilepath) < filemtime($artfilepath) ) {
				$artfilepath = $this->srcdir.$name.'.art';
				$phpfilepath = $this->renderdir.$name.'.php';
				$src = AString::from(file_get_contents($artfilepath));
				$php = $this->translate($src->toString());
				file_put_contents($phpfilepath,$php);
			}
		} else {
			Encore::log('ArtTranslator Class','translate method','file \''.$artfilepath.'\' exist',2);
		}
	}

	public function translate ($artsrc ) {
		$ret = new AString();
		$src = AString::from($artsrc);
		$buffer = new AString();
		$max = $src->length();
		$mode = 'html';
		$line = 1;

		for($i=0;$i<$max;$i++) {
			$char = $src->at($i);
			switch($char) {
				case '{':
					switch($mode) {
						case 'html':
							$mode = 'wait_{';
							break;
						case 'wait_{':
							$mode = 'art';
							break;
					}
					break;
				case '}':
					switch($mode) {
						case 'art':
							$mode = 'wait_}';
							break;
						case 'wait_}';
							$mode = 'html';
							$conv = $this->convert($buffer);
							if($conv == null) {
								$error = (isset($this->errors[0]))? $this->errors[0]: 'unknown in '.$buffer;
								throw new AException('TRANSLATOR ERROR: '.$error,0,$this->srcdir.$this->filename.'.art', $line);
							}
							$ret = $ret->append(AString::from($conv)->prepend('<?php echo($')->append('); ?>'));
							$buffer->clear();
							break;
						case 'html':
							$ret = $ret->append('}');
					}
					break;
				case "\n":
					$line++;
					$ret = $ret->append("\n");
					break;
				default:
					switch($mode) {
						case 'html':
							$ret = $ret->append($buffer->append($src->at($i)));
							$buffer->clear();
							break;
						case 'wait_{':
							$mode = 'html';
							$ret = $ret->append('{')->append($src->at($i));
							$buffer->clear();
							break;
						case 'wait_}':
							$mode = 'art';
							$ret = $ret->append('}')->append($src->at($i));
							$buffer->clear();
							break;
						case 'art':
							$buffer = $buffer->append($src->at($i));
					}
			}
		}
		$code = AString::from("<?php\r\n");
		$maxi = count($this->meta['code']);

		for($i=0;$i<$maxi;$i++) {
			$code = $code->append($this->meta['code'][$i])->append("\r\n");
		}
		$code = $code->append("?>\r\n");
		return $ret->prepend($code)->append("\r\n")->toString();
	}

	protected function convert($expresion, &$code_array = null) {
		$ret = AString::from(''); // повертаєм вставку на вивід значення зміної;
		                          // а обрахунок результату зміної пишем в масив (з кінця на початок)

		$expdata = array(
			'type' => null,
			'context' => '',
			'varname' => '',
			'args' => array(),
			'args_code' => array()
		);


		$mode = 'wait';
		$buffer = new AString();
		$argbuffer = new AString();
		$exp = AString::from($expresion)->append(' ');
		$maxi = $exp->length();
		$split_flag = false;


		for($i = 0; $i<$maxi; $i++) {
			if($mode == 'break') {
				break;
			}
			$char = $exp->at($i);
			if($char=="\r" || $char=="\n" || $char=="\t" ) {
				$char->set(' ');
			}
			if($mode == 'snippet_args_wait_value' && $char != ' ') {
				$mode = 'snippet_args_value';
			}
			$ec = false;
			if($mode == 'snippet_args_double_quote_value'  && $char != '"') {
				if($char == '\\' && !$ec) {
					$ec = true;
					continue;
				}
				$ec = false;
				$buffer = $buffer->append($char);
				continue;
			}
			if($mode == 'snippet_args_quote_value' && $char != '\'') {
				if($char == '\\' && !$ec) {
					$ec = true;
					continue;
				}
				$ec = false;
				$buffer = $buffer->append($char);
				continue;
			}
			switch($char) {
				case '\'':
					switch($mode) {
						case 'snippet_args_value':
							$mode = 'snippet_args_quote_value';
							break;
						case 'snippet_args_quote_value':
							if($ec) {
								$buffer = $buffer->append('\''.$char);
							} else {
								$mode = 'snippet_args_value';
							}
							break;
						default:
							$this->errors[] = 'Symbol \' is not expected';
					}
					break;
				case '"':
					switch($mode) {
						case 'snippet_args_value':
							$mode = 'snippet_args_double_quote_value';
							break;
						case 'snippet_args_double_quote_value':
							if($ec) {
								$buffer = $buffer->append('\"'.$char);
							} else {
								$mode = 'snippet_args_value';
							}
							break;
						default:
							$this->errors[] = 'Symbol " is not expected';
					}
					break;
				case '(':

					switch($mode) {
						case 'snippet':
							$mode = 'snippet_args_wait';
							$expdata['snippet'] = $buffer->toString();
							$expdata['varname'] = $buffer->append( count($this->meta['vars']) )->replace('/','')->toString();
							$buffer->clear();
							break;
						default:
							$this->errors[] = 'Symbol `(` is not expected';
							return null;
					}
					break;
				case ')':
					if($split_flag) {
						$this->errors[] = 'Symbol `,` is not expected';
						return null;
					}
					switch($mode) {
						case 'snippet_args_wait':
							$mode = 'break';
							break;
						case 'snippet_args_value':
							$mode = 'break';
							$expdata['args'][$argbuffer->toString()] = $buffer->toString();
							$argbuffer->clear();
							$buffer->clear();
							$split_falg = false;
							break;
						case 'snippet_args_wait_spliter':
							if(!$split_flag) {
								$mode = 'break';
								break;
							}
						default:
							$this->errors[] = 'Symbol `)` is not expected';
							return null;
					}
					break;
				case ',':
					switch($mode) {
						case 'snippet_args_wait_spliter':
							$mode = 'snippet_args_wait';
							$split_flag = true;
							break;
						case 'snippet_args_value':
							$mode = 'snippet_args_wait';
							$expdata['args'][$argbuffer->toString()] = $buffer->toString();
							$argbuffer->clear();
							$buffer->clear();
							$split_flag = true;
							break;
						default:
							$this->errors[] = 'Symbol `)` is not expected';
							return null;
					}
					break;
				case '`':
					switch($mode) {
						case 'snippet_args_wait':
							$mode = 'snippet_args_open';
							$split_flag = false;
							break;
						case 'snippet_args_open':
							$mode = 'snippet_args_wait_=';
							$argbuffer->set($buffer);
							$buffer->clear();
							break;
						default:
							$this->errors[] = 'Symbol ` is not expected';
							return null;
					}
					break;
				case '=':
					switch($mode) {
						case 'snippet_args_wait_=':
							$mode = 'snippet_args_wait_value';
							break;
						default:
							$this->errors[] = 'Symbol `=` is not expected';
							return null;
					}
					break;
				case ':':
					switch($mode) {
						case 'wait':
							$mode = 'snippet';
							$expdata['type'] = 'snippet';
							break;
						case 'snippet_args_value':
							$buffer = $buffer->append($char);
							break;
						default:
							$this->errors[] = 'Symbol `:` is not expected';
							return null;
					}
					break;
				case '%':
					switch($mode) {
						case 'wait':
							$mode = 'translate';
							$expdata['type'] = 'translate';
							break;
						case 'snippet_args_value':
							$buffer = $buffer->append($char);
							break;
						default:
							$this->errors[] = 'Symbol `%` is not expected';
							return null;
					}
					break;
				case ' ':
					switch($mode) {
						case 'placeholder':
							$expdata['placeholder'] = $buffer->toString();
							$expdata['varname'] = $buffer->append( count($this->meta['vars']) )->toString();
							$mode = 'break';
							break;
						case 'chunk':
							$expdata['chunk'] = $buffer->toString();
							$expdata['varname'] = $buffer->append( count($this->meta['vars']) )->replace('/','')->toString();
							$mode = 'break';
							break;
						case 'translate':
							$expdata['translate'] = $buffer->toString();
							$expdata['varname'] = $buffer->append( count($this->meta['vars']) )->toString();
							$mode = 'break';
							break;
						case 'snippet':
							$expdata['snippet'] = $buffer->toString();
							$expdata['varname'] = $buffer->append( count($this->meta['vars']) )->toString();
							$mode = 'break';
							break;
						case 'snippet_args_wait':
							break;
						case 'snippet_args_wait_=':
							break;
						case 'snippet_args_wait_value':
							break;
						case 'snippet_args_wait_spliter':
							break;
						case 'snippet_args_value':
							$mode = 'snippet_args_wait_spliter';
							$expdata['args'][$argbuffer->toString()] = $buffer->toString();
							$argbuffer->clear();
							$buffer->clear();
							$split_flag = false;
							break;
						default:
							$this->errors[] = 'Symbol space is not expected';
							return null;
					}
					break;
				case '$':
					switch($mode) {
						case 'wait':
							$mode = 'placeholder';
							$expdata['type'] = 'placeholder';
							break;
						case 'snippet_args_value':
							$buffer = $buffer->append($char);
							break;
						default:
							$this->errors[] = 'Symbol `$` is not expected';
							return null;
					}
					break;
				case '.':
					if(in_array($mode,array('placeholder','translate'))) {
						if($expdata['context'] == '') {
							$expdata['context'] = $buffer->toString();
							$buffer->clear();
						} else {
							$this->errors[] = 'Too many contexts';
							return null;
						}
					} elseif($mode = 'snippet_args_value') {
						$buffer = $buffer->append($char);
					} else {
						$this->errors[] = 'Symbol `.` is not expected';
						return null;
					}
					break;
				case '#':
					switch($mode) {
						case 'wait':
							$mode = 'chunk';
							$expdata['type'] = 'chunk';
							break;
						case 'snippet_args_value':
							$buffer = $buffer->append($char);
							break;
						default:
							$this->errors[] = 'Symbol `#` is not expected';
							return null;
					}
					break;
				case '/':
					switch($mode) {
						case 'chunk':
							$buffer = $buffer->append($char);
							break;
						case 'snippet':
							$buffer = $buffer->append($char);
							break;
						case 'snippet_args_value':
							$buffer = $buffer->append($char);
							break;
						case 'snippet_args_open':
							$buffer = $buffer->append($char);
							break;
						default:
							$this->errors[] = 'Symbol `/` is not expected';
							return null;
					}
					break;
				default:
					switch($mode) {
						case 'wait':
							$this->errors[] = 'Not the expected symbol';
							return null;
							break;

						default:
							if(in_array($mode, array('placeholder','chunk','translate','snippet','snippet_args_open','snippet_args_value'))) {
								$buffer = $buffer->append($char);
							} else {
								$this->errors[] = 'Not the expected symbol';
								return null;
							}
					}

			}
		}

		$exp = new AString();
		switch ($expdata['type']) {
			case 'placeholder':
				$exp = $exp->append('$')->append($expdata['varname'])->append('=$this->getPlaceholder(\'')->append($expdata['placeholder']);
				if($expdata['context'] != '') {
					$exp = $exp->append('\',\'')->append($expdata['context']);
				}
				$exp = $exp->append('\');');
				break;
			case 'chunk':
				$exp = $exp->append('$')->append($expdata['varname'])->append('=$this->getChunk(\'');
				$exp = $exp->append(AString::from($expdata['chunk'])->replace('/','\'.DS.\''))->append('\');');
				break;
			case 'translate':
				$exp = $exp->append('$')->append($expdata['varname'])->append('=Encore::tr(\'')->append($expdata['translate']);
				$context = ($expdata['context'] != '') ? $expdata['context']: 'frontend';
				$exp = $exp->append('\',\'')->append($context)->append('\');');
				break;
			case 'snippet':
				$exp = $exp->append('$')->append($expdata['varname'])->append('=$this->getSnippet(\'');
				$exp = $exp->append(AString::from($expdata['snippet'])->replace('/','\'.DS.\''))->append('\',array(');

				$vars = array();

				foreach($expdata['args'] as $key=>$value) {
					if(AString::from($key)->isEmpty()){
						$this->errors[] = 'Argument is empty';
						return null;
					}
					$val = AString::from($value);
					if($val->isEmpty()){
						$this->errors[] = 'Value is empty';
						return null;
					}
					$exp_call_flag = false;
					if( in_array($val->at(0)->toString(),array('$','#','%',':'))) {
						$exp_call_flag = true;
						$val = '$'.$this->convert($val->toString(), $expdata['args_code']);
					}
					$vars[] = '\''.$key.'\' => ' . (($exp_call_flag) ? $val : '\''.$val.'\'');					
				}
				
				$exp = $exp->append(AString::from('')->join($vars,','))->append('));');
				break;
			default:
				$this->errors[] = 'Unknow expresion type';
				return null;
		}

		$this->meta['vars'][] = $expdata['varname'];
		
		if(is_array($code_array)) {
			array_unshift($code_array, $exp->toString());
			$code_array = array_merge($expdata['args_code'],$code_array);
		} else {
			array_unshift($this->meta['code'], $exp->toString());
			$this->meta['code'] = array_merge($expdata['args_code'],$this->meta['code']);
		}

		
		//echo('<pre>');
		//var_dump($this->meta);
		//echo('</pre>');

		return ($expdata['type'] == null ) ? null : $expdata['varname'];
	}
}