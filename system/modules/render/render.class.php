<?php

class Render extends EncoreModule {

	protected $placeholders = array( 'PAGE' => array());
	protected $htmlheadlinks = array();

	protected $render_path = null;
	protected $chunks_path = null;
	protected $source_path = null;
	protected $snippets_path = null;
	protected $render_context = 'PAGE';
	protected $translator = null;

	public function __construct() {
		$this->registerSignals(array('beforeRender'));
	}

	public function init() {
		require_once('translator.class.php');
		$this->render_path = Encore::getOption('RENDER_PATH', dirname(dirname(dirname(__FILE__))).DS.'templates'.DS.'render'.DS, 'RENDER');
		$this->snippets_path = Encore::getOption('SNIPPETS_PATH', dirname(dirname(dirname(__FILE__))).DS.'templates'.DS.'snippets'.DS, 'RENDER');
		$this->source_path = Encore::getOption('SOURCE_PATH', dirname(dirname(dirname(__FILE__))).DS.'templates'.DS.'src'.DS, 'RENDER');
		$this->translator = new ArtTranslator($this->source_path, $this->render_path);
		$this->chunks_path = $this->render_path.'chunks'.DS;
		$this->translator = new ArtTranslator($this->source_path, $this->render_path);
	}

	/**
	 * Згенерувати код сторінки
	 *
	 * @param array $page
	 * @param array $options
	 * @throws AException
	 */
	public function renderPage($page, $options = array()) {
		if($page == null) return;
               
		$tpl_filename = $this->render_path . $page['template'].'.php';

		$this->translator->translatefile($page['template']);

		if(!file_exists($tpl_filename)) {
			throw new AException(AString::from(Encore::tr('ER_TEMPLATE_FILE_EXISTS'))->arg($tpl_filename));
		}
 
		$this->emit('beforeRender');

		if ( isset($page['executable']) && $page['executable']) {
			$page['content'] = $this->executePage($page);
		}

		$plcs = $page;
		unset($page['accessable']);
		unset($page['executable']);

		$this->placeholders[$this->render_context] = array_merge($this->placeholders[$this->render_context],$plcs);
		if(is_array($options)) {
			$this->placeholders[$this->render_context] = array_merge($this->placeholders[$this->render_context],$options);
		}

		foreach($page['vars'] as $name=>$value) {
			$this->placeholders[$this->render_context]['var_'.$name] = $value;
		}
		
		require_once($tpl_filename);
	}

	/**
	 * Транслувати всі шаблони
	 *
	 * @param bool $recurce
	 */
	public function translateAllTemplates($recurce = false, $path = null) {
		$targetpath = ($path == null)? $this->source_path : $path;
		$dir = opendir ($targetpath);
		while ( $file_name = readdir ($dir)) {
			if($file_name == '.' || $file_name == '..') continue;
			if($recurce && is_dir($file_name)) {
				translateAllTemplates(true, $path.DS.$file_name);
			}
			if(is_file($targetpath.$file_name)) {
				$this->translator->translatefile(AString::from( (($path!='')?$path.DS:'').$file_name)->replace($this->source_path,''));
			}
		}
	}

	/**
	 * Отримати значення плейсхолдера
	 *
	 * @param string $name
	 */
	public function getPlaceholder($name,$context = null) {
		$rc = ($context==null) ? $this->render_context : $context;
		return (isset($this->placeholders[$rc]) && isset($this->placeholders[$rc][$name])) ? $this->placeholders[$rc][$name] : null;
	}

	/**
	 * Задати значення плейсхолдера
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function setPlaceholder($name, $value, $context = null) {
		$rc = (empty($context)) ? $this->render_context : $context;
		if(!isset($this->placeholders[$rc])) {
			$this->placeholders[$rc] = array();
		}
		$this->placeholders[$rc][$name] = $value;
	}

	/**
	 * Задати плейсхолдери з хешу
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function setPlaceholders($arg, $context = null) {
		if(is_array($arg)) {
			foreach($arg as $name=>$value) {
			    $this->setPlaceholder($name,$value,$context);
			}
		}
	}

	/**
	 * Отримати обчислений код чанка
	 *
	 * @param string $name
	 * @throws AException
	 */
	public function getChunk($name) {
		$this->translator->translatefile('chunks'.DS.$name);
		$chunk_filename = $this->chunks_path . $name .'.php';
		if(!file_exists($chunk_filename)) {
			throw new AException(AString::from(Encore::tr('ER_CHUNK_FILE_EXISTS'))->arg($chunk_filename));
		}

		//$rc = $this->render_context;
		//$this->render_context = 'CHUNK:'.$name;
		ob_start();
		require ($chunk_filename);
		$ret = ob_get_contents();
		ob_end_clean();
		//$this->render_context = $rc;

		return $ret;
	}

	/**
	 * Отримати результат виконання сніпета
	 *
	 * @param string $name
	 * @param array $args
	 * @throws AException
	 */
	public function getSnippet($name, $args=array(), $safe_context = true) {
		$snippets_filename = $this->snippets_path . $name .'.php';
		if(!file_exists($snippets_filename)) {
			throw new AException(AString::from(Encore::tr('ER_SNIPPET_FILE_EXISTS'))->arg($snippets_filename));
		}
		$rc = $this->render_context;

		if($safe_context) {
			$this->render_context = 'SNIPPET:'.$name;
		}

		$ret = require ($snippets_filename);

		$this->render_context = $rc;

		return $ret;
	}

	/**
	 * Отримати код харезервованих записів блока <HEAD>
	 *
	 */
	public function getHtmlHeadRecords() {
		$ret = '';
		foreach( $this->htmlheadlinks as $link=>$type ) {
			switch($type) {
				case 'css':
					$ret .= '<link rel="stylesheet" type="text/css" href="'.$link.'">'."\r\n";
					break;
				case 'javascript':
					$ret .= '<script type="text/javascript" src="'.$link.'"></script>'."\r\n";
					break;
			}
		}
		return $ret;
	}

	/**
	 * Зарезервувати CSS файл для заголовка
	 *
	 * @param string $filename
	 */
	public function registerCss($filename) {
		$this->htmlheadlinks[$filename] = 'css';
	}

	/**
	 * Зарезервувати JavaScript файл для заголовка
	 *
	 * @param string $filename
	 */
	public function registerJavaScript($filename) {
		$this->htmlheadlinks[$filename] = 'javascript';
	}

	//public function registerMetatag($tagname) {}

	public function executePage($page) {
		$epage_path = Encore::getOption('EXECUTABLE_PAGES_PATH', null, 'RENDER');
		$ret = '';
		if($epage_path != null && is_array($page)) {
			$md5 = md5($page['id']);
			$fl = array( $md5{0} , $md5{1} , $md5{2} , $md5{3} );
		 	$epage_filename = $epage_path . AString::from('').join($fl,DS) . DS. $md5 . '.php';
		 	if ( !file_exists($epage_filename) || filemtime($epage_filename) < Encore::getModule('DB')->parseTime($page['updatedon']) ) {
		 		$dpath = $epage_path;
		 		foreach ( $fl as $dname ) {
		 			$dpath .= $dname . DS;
		 			if(!file_exists($dpath)) {
		 				if (!mkdir($dpath, 0775)) {
		 					throw new AException(AString::from('ER_CREATE_DIR')->arg($dpath));
		 				}
		 			}
		 		}
		 		$phppage = $this->translator->translate($page['content']);
		 		$f = fopen($epage_filename, 'w+');
				if (flock($f, LOCK_EX)) {
					fwrite($f, $phppage);
					flock($f, LOCK_UN);
				}
				fclose($f);
		 	}

		 	ob_start();
			require ($epage_filename);
			$ret = ob_get_contents();
			ob_end_clean();

		} else {
			throw new AException(Encore::tr('ER_INVALID_PAGE'));
		}

		return $ret;
	}

	public function getTemplatesList() {
		$this->translateAllTemplates();
		$ret = array();
		if ($dh = opendir($this->render_path)) {
      		while (($file = readdir($dh)) !== false) {
          		if ($file !== '.' AND $file !== '..') {
             		$current_file = "{$this->render_path}/{$file}";
             		if (is_file($current_file)) {
                		$ret[] = AString::from($file)->replace('.php','');
             		}
          		}
      		}
   		}
   		return $ret;
	}

}