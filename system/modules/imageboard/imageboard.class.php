<?php

class Imageboard extends EncoreModule
{
	public function init()
	{
		date_default_timezone_set("Europe/Moscow");
		Encore::requireModules("Imageboard", "DB, UserManager");
	}
	
	private function getStatus($user_id)
	{
		$sql = "SELECT * 
				FROM {pr}sessions 
				WHERE user='".$user_id."' AND expire > NOW()";
		$result = Encore::getModule("DB")->select($sql);
		if ($result) {
			return "online";
		} else {
			return "offline";
		}
	}
	
	public function getContacts($offline=true)
	{
		$sql = "SELECT 
					u.name AS name,
					u.id AS id
				FROM {pr}users u
				WHERE id NOT IN (2,3)";
		$result = Encore::getModule("DB")->select($sql);
		foreach ($result as &$record) {
			$record["status"] = $this->getStatus($record["id"]);
		}
		return $result ? $result : array();
	}
	
	public function getMessages($timestamp=null)
	{
		$db = Encore::getModule("DB");
		$sql = "SELECT * FROM imageboard";
		if (!$timestamp) {
			//return last 10 messages
			$sql .= " ORDER BY time DESC LIMIT 10";
		} else {
			//return messages newer than passed timestamp
			$sql .= " WHERE time > '".$db->real_escape_string($timestamp)."' AND sender <> ".$this->getUserId();
		}
		return $db->select($sql);
	}
	
	protected function getUserId()
	{
		$userdata = Encore::getModule('UserAuth')->getUserData();
		if (!$userdata) {
			return null;
		}
		return $userdata["id"];
	}
	
	public function statusKey($key=null)
	{
		$cm = Encore::getCacheManager();
		if ($key) {
			$cm->set("statuskey", $key);
			return; 
		}
		return intval($cm->get("statuskey"));
	}
	
	private function generateStatusKey()
	{
		return md5(dechex(mt_rand(0, 0x7fffffff)));
	}
	
	public function getState()
	{
		//init response
		$response = array(
			"status" => "success"
		);
		
		try {
			//do appropriate changes first
			//update user status if it was changed
			$updatestatus = (isset($_POST["updatestatus"]) && $_POST["updatestatus"]);
			$statuskeychanged = (isset($_POST["statuskey"]) && $_POST["statuskey"] != ($newStatusKey = $this->statusKey()));
			if ($updatestatus || $statuskeychanged) {
				
					if ($updatestatus) {
						Encore::getModule("UserManager")->setUserAttribute($this->getUserId(), "status", $_POST["updatestatus"]);
						$newStatusKey = $this->generateStatusKey();
						$this->statusKey($newStatusKey);
					}
					
					if ($statuskeychanged) {
						$newStatusKey = $this->statusKey();
						
						//all user info with statuses
						$response["users"] = $this->getContacts();
					}
					
					$response["statuskey"] = $newStatusKey;
			} else {
				$response = $this->statusKey();
			}
			
			if (isset($_POST["firstCall"]) && $_POST["firstCall"]) {
				$response["users"] = $this->getContacts();
			}
			
			//all messages from requested timestamp
			$response["messages"] = $this->getMessages(isset($_POST["time"]) ? intval($_POST["time"]) : null);
			
		} catch (Exception $e) {
			//if something wrong
			$response["status"] = "failure";
			$response["description"] = $e->getMessage();
		}
		return $response;
	}
	
	public function sendMessage()
	{
		$return = array('status' => "success");
		try {
			$db = Encore::getModule("DB");
			$message = isset($_POST["message"]) ? $_POST['message'] : null;
			if (!$message) {
				throw new Exception("Message can't be empty");
			}
			$id_sender = $this->getUserId();
			if (!$id_sender) {
				throw new Exception("Can't define sender");
			}
			$db->exec("INSERT INTO imageboard(sender, content, time) VALUES('".$id_sender."', '".$db->real_escape_string($message)."', '".time()."')");
			$return["id"] = $db->last_id();
		} catch (Exception $e) {
			$return["status"] = "failure";
			$return["description"] = $e->getMessage();
		}
		return $return;
	}
}
