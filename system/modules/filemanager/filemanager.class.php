<?php


class FileManager extends EncoreModule {
	protected $base_path = null;
	protected $start_path = null;
	protected $mdl = null;

	public function __construct() {
		Encore::requireModules('FileManager', 'Mandator, UserAuth');

		$this->mdl = Encore::getModule('Mandator');
		$this->base_path = dirname(SYSTEM_PATH).DS;

		$this->setStartPath(Encore::getOption('START_PATH',dirname(SYSTEM_PATH).DS,'FILEMANAGER'));
	}

	public function init(){


	}

	public function getStartPath(){
		return $this->start_path;
	}

	public function setStartPath($path) {
		$ret = false;
		if(!$this->mdl->hasPermission('readfiles',$path) && $this->start_path == null ) {
			$uperms = $this->mdl->getCurrentUserPermissions('readfiles');
			if ( $uperms != null ) {
				$min = 99999;
				foreach ($uperms as $uperm) {
					if ( $min > count(AString::from($uperm['path'])->split('/')) ) {
						$this->start_path = $this->toBase($uperm['path']);
					}
				}
			} else {
				$this->start_path = null;
			}
		} else {
			$this->start_path = $this->toBase($path);
			$ret = true;
		}

		return $ret;
	}

	public function getDirList($spath,$base=false) {
		$path = ($base) ? $this->fromBase($spath) : $spath;
		if(!$this->mdl->hasPermission('readfiles',$path)) return null;
		$ret = array();

		if ($dh = opendir($path)) {
    		while (($file = readdir($dh)) !== false) {
      			if ($file !== '.' AND $file !== '..') {
           			$current_file = $path.DS.$file;
           			if (is_dir($current_file)) {
              			$ret[] = array(
              				'filename' => $file,
              				'folder' => true,
              				'fullpath' => ($base) ? $this->toBase($current_file) : $current_file
           				);
           			}
       			}
    		}
   		}

		return $ret;
	}

	public function getFileList($spath,$base=false) {
		$path = ($base) ? $this->fromBase($spath) : $spath;
		if(!$this->mdl->hasPermission('readfiles',$path)) return null;
		$ret = array();

		if ($dh = opendir($path)) {
    		while (($file = readdir($dh)) !== false) {
      			if ($file !== '.' AND $file !== '..') {
           			$current_file = $path.DS.$file;
           			$ret[] = array(
              			'filename' => $file,
              			'folder' => is_dir($current_file),
           				'fullpath' => ($base) ? $this->toBase($current_file) : $current_file
           			);
       			}
    		}
   		}

		return $ret;
	}

	public function getBasePath() {
		return $this->base_path;
	}

	public function toBase($path) {
		return AString::from($path)->replace($this->base_path, '')->prepend(DS)->replace(DS.DS, DS)->toString();
	}

	public function fromBase($path) {
		return AString::from($path)->prepend($this->base_path)->replace(DS.DS, DS)->toString();
	}

	public function upload($varname, $path, $automakedir = false) {
		$ret = false;
		$target = AString::from($_SERVER['DOCUMENT_ROOT'])->append($path)->replace('\\','/')->replace('//','/');
		if($automakedir) {
			$this->makedir($target);
		}
		if(is_dir($target) && isset($_FILES[$varname])) {
			try {
				$ret = move_uploaded_file($_FILES[$varname]['tmp_name'], $target->append(DS)->append($_FILES[$varname]['name'])->replace('//','/'));
			} catch (Exception $e) {}
		}
		return $ret;
	}

	public function rename($oldfilename, $newfilename) {}

	public function copy($filename, $copyfilename) {}

	public function uploadImage($varname, $basepath, $width=null, $height=null) {
		$ret = null;

		if ( preg_match('/\.jpeg$|\.jpg$|\.png$|\.gif$|\.bmp$/',$_FILES[$varname]['name']) ) {
			$image_name = Encore::generateUniqueKey('4');
			$path = $_SERVER['DOCUMENT_ROOT'].DS.$basepath;
			$upload_dir = AString::from($image_name)->left(2)->append(DS)->prepend(DS)->prepend($path)->replace(DS.DS,DS);
			if(!is_dir($upload_dir)) {
				mkdir($upload_dir);
			}
			$upload_file = $upload_dir . basename($image_name.'_'.$_FILES[$varname]['name']);
			$ret = $basepath.'/'.AString::from($image_name)->left(2).'/'.basename($upload_file);
			$this->image_jpegresizer ($_FILES[$varname]['tmp_name'],$width,$upload_file);
		}
		return $ret;
	}

	public function image_jpegresizer($photo_src, $size, $name, $increase = true){
		$width = 0;
		if(is_array($size) && isset($size['width'])) {
			$width = $size['width'];
		} else {
			$width = $size;
		}
		$parametr = getimagesize($photo_src);
		list($width_orig, $height_orig) = getimagesize($photo_src);
		$new_width = $width_orig;
		$new_height = $height_orig;

		if( ($width != 0 && $width !=null) &&( $increase || $width < $new_width)) {
			$ratio_orig = $width_orig/$height_orig;
			$new_width = $width;
			if(is_array($size) && isset($size['height'])) {
				$new_height = $size['height'];
			} else {
				$new_height = $width / $ratio_orig;
			}
		}
		$newpic = imagecreatetruecolor($new_width, $new_height);
		switch ( $parametr[2] ) {
  			case 1:
				$image = imagecreatefromgif($photo_src);
  				break;
  			case 2:
				$image = imagecreatefromjpeg($photo_src);
  				break;
  			case 3:
				$image = imagecreatefrompng($photo_src);
  				break;
		}
		imagecopyresampled($newpic, $image, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
		imagejpeg($newpic, $name, 100);
		return true;
	}

}