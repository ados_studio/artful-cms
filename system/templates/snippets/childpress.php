<?php

if( !isset($args['root']) || !isset($args['tpl']) ) return;
$activeflag = (isset($args['active']));
$active_item = ($activeflag) ? (integer)$args['active'] : 0;
$active_class = (isset($args['activeclass'])) ? $args['activeclass']: 'active';
$firstflag = true;

$pm = Encore::getModule('PageManager');
$up = Encore::getModule('UrlParser');

$pagelimit = Encore::getOption('PAGINATION_LIMIT',10);
$sa = $up->getScriptArguments();
$pagenum = (isset($sa['page'])) ? $sa['page'] : 1;
$vars = (isset($args['pagevars'])) ? ($args['pagevars']==true): false;
$items = $pm->getPages($args['root'],1,'`pages`.`published` = \'1\'','  `pages`.`publishedon` DESC',(($pagelimit*$pagenum)-$pagelimit).','.$pagelimit);
$count = $pm->getPagesCount($args['root'],1,'`pages`.`published` = \'1\'');

$ret = '';

$clear_vars = array();

if($items != null) {
	foreach($items as $item) {
		$this->setPlaceholder('title', $item['pagetitle']);
		$this->setPlaceholder('preview', $item['preview']);
		$this->setPlaceholder('publishedon', date('d.m.Y', Encore::getModule('DB')->parseTime($item['publishedon'])));
		$this->setPlaceholder('link',$up->makeUrl($item['alias']));

		if($activeflag) {
			if($active_item==0) {
				if($firstflag) {
					$this->setPlaceholder('active',$active_class);
					$firstflag = false;
				} else {
					$this->setPlaceholder('active','');
				}
			} else {
				if($item['id']==$active_item) {
					$this->setPlaceholder('active',$active_class);
				} else {
					$this->setPlaceholder('active',$active_class);
				}
			}
		}

		if($vars) {
			$vars = $pm->getPageVars($item['id']);
			if($vars!=null){
				foreach ($vars as $varname=>$value) {
					if(!in_array($varname, $clear_vars)) {
						$clear_vars[] = $varname;
					}
					$this->setPlaceholder('var_'.$varname,$value);
				}
			}
			foreach($args as $name=>$value) {
				$vname = AString::from($name)->replace('def_','')->toString();
				if(AString::from($name)->left(4)=='def_' && !(isset($vars[$vname]) && ($vars[$vname] != '') )) {
					$this->setPlaceholder('var_'.$vname,$value);
				}
			}
		}


		$ret .= $this->getChunk($args['tpl']);

		foreach($clear_vars as $cvar) {
			$this->setPlaceholder('var_'.$cvar, (isset($args['def_'.$cvar]))?$args['def_'.$cvar]:'');
		}

	}
}
$ret .= '<br/><div class="paginator">'.$this->getSnippet('paginator', array('count'=>$count, 'page'=>$pagenum)).'</div>';
return $ret;