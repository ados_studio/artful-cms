<?php
$ret = '';
if(!isset($args['tpl_form'])||!isset($args['tpl_answer'])) return $ret;

if(isset($_COOKIE['manager_contact']) && $_COOKIE['manager_contact'] == 'enabled') {
	$ret = $this->getChunk($args['tpl_answer']);
} else {
	$ret = $this->getChunk($args['tpl_form']);
}

return $ret;