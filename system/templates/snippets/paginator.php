<?php

if(!isset($args['count'])||!isset($args['page'])) return;
$up = Encore::getModule('UrlParser');
$sa = $up->getScriptArguments();
if(isset($args['link_params'])) $sa = array_merge($sa,$args['link_params']);
$html = $sa['html'];
unset($sa['html']);

$ret = '';

$limits = (isset($args['limits']) && is_array($args['limits'])) ? $args['limits'] : array();
$deflimit = Encore::getOption('PAGINATION_LIMIT',15);
$limit = (isset($args['limit'])) ? $args['limit'] : $deflimit;
if(isset($args['limit_control']) && $args['limit_control'] == true ) {
	if(isset($sa['limit']) && in_array($sa['limit'],$limits)) {
		$limit = $sa['limit'];
	}
	if( count($limits) > 0 ) {
		$ret .= '<div class="paginator_limits">';
		$ags = $sa;
		foreach($limits as $lm) {
			$ags['limit'] = $lm;
			if($lm == $deflimit) {
				unset($ags['limit']);
			}
			$class = ($lm == $limit) ? 'class="active"': '';
			$ret .= '<a '.$class.' href="'.$up->makeUrl($html, $ags).'"> ['.$lm.']</a>';
		}
		$ret .='</div> ';
	}
}

$maxpage = ceil(($args['count']/$limit));
if($maxpage<2 && !(isset($args['autohide']) && $args['autohide'] == false)) return '';
$margin = Encore::getOption('PAGINATION_MARGIN',4);
$prevpage = ($args['page'] > 1) ? $args['page']-1: 1;
$nextpage = ($args['page'] < $maxpage) ? $args['page']+1: $maxpage;
$current = (isset($sa['page'])) ? $sa['page'] : 1;





if($prevpage!=1) {
	$sa['page'] = $prevpage;
} else {
	unset($sa['page']);
}

$ret .= '<div class="paginator_pages"><a href="'.$up->makeUrl($html, $sa).'" class="first"></a>';

for($i = 1; $i <= $maxpage; $i++) {
	$mod = $i-$current;
	if($mod < 0) $mod *= -1;
	if($i!=1 && $i!=$maxpage) {
		if( $mod == $margin) $ret .= '...';
		if( $mod >= $margin ) continue;
	}

	$class = ( $mod == 0 ) ? 'class="active"': '';
	$sa['page'] = $i;
	if($i == 1){
		unset($sa['page']);
}
$ret .= '<a href="'.$up->makeUrl($html, $sa).'" '.$class.'>'.$i.'</a>';
}

$sa['page'] = $nextpage;
return $ret . '<a href="'.$up->makeUrl($html, $sa).'" class="last"></a></div>';