<?php
if(!isset($args['targets'])) return;
		  if(!isset($args['tpl'])) return;
		  if(!isset($args['tpl_result'])) return;
		
$args['targets'] = Encore::fromJson($args['targets']);

		  $ret = '';
$sa = Encore::getModule('UrlParser')->getScriptArguments();
$search_expire = 0;//(isset($args['search_expire'])) ? $args['search_expire'] : Encore::getOption('search_expire',10);
$cache = Encore::getCacheManager();
$cache->setexpire($search_expire * 5 );

$search_flag = $cache->get('search_snippet.search_flag');
$cache->defexpire();
$searchwords = array();
if ( ( isset($_POST['query']) && !empty($_POST['query'])) || (isset($sa['query']) && !empty($sa['query'] ))){
	$qrs = (isset($_POST['query'])) ? $_POST['query'] :$sa['query'];
	if($qrs != Encore::tr('search_welcome','frontend')) {
		$this->setPlaceholder('query_string',$qrs);
		$searchwords = AString::from($qrs)->split(' ') ;
		$cache->setexpire(2*60);
		$result = $cache->get('search_snippet.query.'.$qrs);
		if ( $result == null) {
			if($search_flag != null) {
				$this->setPlaceholder('message','З моменту останього пошуку пройшло недостатньо часу, повторіть спробу через декілька хвилин.');
			} else {
				$cache->set('search_snippet.search_flag',1);
				$db = Encore::getModule('DB');
				$result = array();

				foreach( $args['targets'] as $target) {
					$query = 'SELECT `'.$target['field_id'].'` AS `id`,`'.$target['field_title'].'` AS `title` FROM '.$target['tables'].' WHERE ';
					$where = array();
					foreach( $searchwords as $word) {
						$where[] = ' `'.$target['field_title'].'` LIKE "%'.$db->real_escape_string($word).'%" OR `'.$target['field_search'].'` LIKE "%'.$db->real_escape_string($word).'%"';
					}
					$res = $db->select($query . ' ( ' .  AString::from('')->join($where, ' OR ' ) . ' ) ' . ((isset($target['filter'])) ? ' AND '.$target['filter'] : ''));
					if ($res != null ) {
						foreach( $res as $rr) {
							$rr['link'] = AString::from($target['link'])->arg($rr['id'])->toString();
							array_unshift($result, $rr);
						}
					}
				}
				$cache->set('search_snippet.query.'.$qrs, $result);
			}
		}

		$cache->defexpire();

		$output = '';
		if( count($result) > 0) {

			$count = count($result);
			$page = (integer) (isset($sa['page'])) ? $sa['page'] : 1;


			$lm = (isset($args['limit'])) ? $args['limit'] : Encore::getOption('PAGINATION_LIMIT',10);
			if(isset($args['limit'])) {
				$lm = $args['limit'];
			} elseif (isset($sa['limit'])) {
				$lm = $sa['limit'];
			}

			$start = ($lm*$page) - $lm;
			$this->setPlaceholder('paginator', '<br/><br/><div class="paginator">'.$this->getSnippet('paginator', array('limit'=>$lm, 'count'=>$count, 'page'=>$page, 'limit_control'=>true, 'limits'=>array(15,30,45,60), 'autohide'=>true, 'link_params'=> array('query'=>$qrs ))).'</div>');

			$index = 0;
			foreach($result as $item) {
				$index ++;
				if($index <= $start || $index > $start+$lm) continue;
																														$this->setPlaceholder('title', $item['title']);
				$this->setPlaceholder('link', $item['link']);
				$output .= $this->getChunk($args['tpl_result']);
			}

		} else {
			$msg = $this->getPlaceholder('message');
			if(empty($msg)) {
				$this->setPlaceholder('message','Не знайдено жодної сторінки');
			}
		}

		$this->setPlaceholder('result', $output );
	}
	}

	return $this->getChunk($args['tpl']);
				?>