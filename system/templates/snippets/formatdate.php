<?php

$ret = '';

if(isset($args['value'])) {
	$val = AString::from($args['value'])->split(' ');
	if(count($val)>0) {
		$val = AString::from($val[0])->split('-');
		if(count($val)==3) {
			$ret = AString::from("%1.%2.%3")->arg($val[2])->arg($val[1])->arg($val[0]);
		}
	}
}

return $ret;