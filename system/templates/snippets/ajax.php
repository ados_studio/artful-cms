<?php
if(!isset($_POST['action'])) return '';

$prefix = 'ajax'.DS;

$ret = '';
$action = AString::from($_POST['action'])->prepend($prefix)->replace('..','');

if(file_exists($action->prepend($this->snippets_path)->append('.php'))) {
	$ret = $this->getSnippet($action);
}

return $ret;
