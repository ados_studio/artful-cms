<?php
/*
 * сніпет для побудови різноманітних меню, карт сайту
 * версія 0.2
 *
 * ПАРАМЕТРИ СНІПЕТА:
 * - обовязкові
 * 		root	: ід кореневої сторінки з нащадків якої почнеться побудова дерева посилань
 * 		tpl		: шаблон в який буде загортатись кожне посилання 
 *
 * - необовязковы
 * 		level   : глубина вибірки нащадків з кореневої сторінки
 * 		tpl_ct  : шаблон в який загортаються посилання одного рівня
 * 		active_class : чи виділяти класом active активну сторінку та її предків
 * 		active_level : глибина виділення серед предків
 * 		limit        : обмеження на кількість посилань
 * 		empty_msg    : повідомлення у випадку відсутності силок для виводу
 *		published	 : чи вибирати тільки опубліковані сторінки
 *		filter		 : фільтр для запиту
 *
*/
if(!isset($args['root']) || !isset($args['tpl'])) return '';

$ret = new AString();

$active_class = !(isset($args['active_class']) && $args['active_class']==false);
$active_level = (isset($args['active_level']))? $args['active_level'] : 1;
$active_id = $this->getPlaceholder('id','PAGE');
$active_ids = array($active_id);
$published_flag = (isset($args['published']) && ($args['published'] == 0 || $args['published'] == true)) ? 0 : 1;

if($active_class) {
	$res = Encore::getModule('DB')->select('SELECT `parent` FROM `pagestree` WHERE `page_id` = '.(integer)$active_id.' AND `level` < '.(integer)$active_level );
	if($res != null) {
		foreach($res as $val) {
			if( $val['parent'] == 0 ) continue;
		    $active_ids[] = $val['parent'];
		}
	}
}

$level = (isset($args['level']) && is_numeric($args['level'])) ? $args['level'] : 1;
$order = (isset($args['order']) ) ? $args['order'] : '';
$limit = (isset($args['limit']) ) ? $args['limit'] : '';
$filter = (isset($args['filter']) ) ? $args['filter'] : '';
if ($published_flag) {	
	if (!AString::from($filter)->isEmpty())
		$filter .= ' AND';
	$filter .= ' {pr}`pages`.`published` = 1';
}

$pageManager = Encore::getModule('PageManager');
$urlParser = Encore::getModule('UrlParser');
$pages = $pageManager->getPages((integer)$args['root'],$level, $filter, $order, $limit);

if( count($pages) > 0 ) {
	$context = array(
		array(
			'list'     => &$pages,
			'index'    => 0,
			'length'   => count($pages),
			'html'	   => new AString()
		)
	);
	$context_deep = 0;
	$break = false;
	$childs_html = new AString();
	while(!$break) {
		$ct = &$context[$context_deep];
		if($ct['index'] >= $ct['length']) {

			if($context_deep <= 0) {
				$break = true;
				continue;
			} else {
				$childs_html = $ct['html'];
				unset($context[$context_deep--]);
				$ct = &$context[$context_deep];
			}
		}

		if($childs_html!='') {
			if(isset($args['tpl_ct'])) {
				$this->setPlaceholder('content',$childs_html);
				$childs_html->set($this->getChunk($args['tpl_ct']));
			}
		}

		$keys = array_keys($ct['list']);
		$page = &$ct['list'][$keys[$ct['index']++]];

		if(isset($page['childs']) &&is_array($page['childs']) && count($page['childs'] > 0) ) {
			$context[] = array(
				'list' => $page['childs'],
				'index' => 0,
				'length' => count($page['childs']),
				'html' => new AString()
				);
		}

		$this->setPlaceholder('title', $page['pagetitle']);
		$this->setPlaceholder('longtitle', $page['longtitle']);
		$this->setPlaceholder('id', $page['id']);
		$this->setPlaceholder('alias', $page['alias']);
		$this->setPlaceholder('link', $urlParser->makeUrl($page['alias']));
		$this->setPlaceholder('childs', $childs_html);
		$this->setPlaceholder('publishedon',$page['publishedon']);
		$this->setPlaceholder('active_class', ($active_class && in_array($page['id'],$active_ids))? ' class="active" ':'' );

		$childs_html->clear();

		$ct['html'] = $ct['html']->append($this->getChunk($args['tpl']));
	}

	$ret = $context[0]['html'];


	if(isset($args['prepend_root']) && $args['prepend_root'] == true) {
		$page = $pageManager->getPageById((integer)$args['root']);
		$this->setPlaceholder('title', $page['pagetitle']);
		$this->setPlaceholder('longtitle', $page['longtitle']);
		$this->setPlaceholder('id', $page['id']);
		$this->setPlaceholder('alias', $page['alias']);
		$this->setPlaceholder('link', $urlParser->makeUrl($page['alias']));
		$this->setPlaceholder('childs', '');
		$this->setPlaceholder('active_class', ($active_class && in_array($page['id'],$active_ids))? ' class="active" ':'' );

		$ret = $ret->prepend($this->getChunk($args['tpl']));
	}

	if(isset($args['tpl_ct'])) {
		$this->setPlaceholder('content',$ret);
		$ret->set($this->getChunk($args['tpl_ct']));
	}
} else {
	$ret->set( (isset($args['empty_msg']))?$args['empty_msg']:'' );
}


return $ret->toString();


