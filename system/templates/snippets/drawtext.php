<?php
$upload_path = dirname(dirname(dirname(dirname(__FILE__)))) . DS . 'assets' . DS . 'uploads' . DS;

$ret = '';
if(isset($args['value']) && (string)($args['value'] ) != '') {
	$size = (isset($args['size']))? $args['size']: 3;
	$class = (isset($args['class']))? $args['class']: '';
	// TODO color;
	$text = (string)$args['value'];


	$md_5 = AString::from(md5($text));
	$filename = $upload_path . $md_5->left(1) . DS . $md_5->left(2)->right(1) . DS . $md_5 . '.png';
	$src = DS .'assets' . DS . 'uploads' . DS . $md_5->left(1) . DS . $md_5->left(2)->right(1) . DS . $md_5 . '.png';
	if(is_file($filename) ) {
		$info = getimagesize($filename);
		$ret = AString::from('<img class="%1" src="%2" %3/>');
		$ret = $ret->arg($class)
		->arg($src)
		->arg($info[3]);
	} else {
		$upload_dir = $upload_path . $md_5->left(1);
		if(!is_dir($upload_dir)) {
			mkdir($upload_dir);
		}
		$upload_dir = $upload_path . $md_5->left(1) . DS . $md_5->left(2)->right(1);
		if(!is_dir($upload_dir)) {
			mkdir($upload_dir);
		}

		$width = imagefontwidth($size)*strlen($text);
		$height = imagefontheight($size);
		$im = imagecreate($width, $height);

		$bg = imagecolorallocate($im, 255, 255, 255);
		$black = imagecolorallocate($im, 0x00, 0x00, 0x00);
		imagecolortransparent($im,$bg);
		imagestring($im, $size, 0, 0, $text, $black);
		imagepng($im, $filename);

		$ret = AString::from('<img class="%1" src="%2" width="%3" height="%4"/>');
		$ret = $ret->arg($class)
				->arg($src)
				->arg($width)
				->arg($height);

	}
}

return $ret;