<?php

$ret = '';
if(!isset($args['tpl'])) {
	$ret = "\t<url>\r\n\t\t<loc>{{$loc}}</loc>\r\n\t\t<lastmod>{{$lastmod}}</lastmod>\r\n\t\t</url>\r\n";
}

$ret = AString::from('<?xml version="1.0" encoding="UTF-8"?>')->append("\r\n")->append('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">')->append("\r\n");

$root = (isset($args['root']))? $args['root']: 0;
$up = Encore::getModule('UrlParser');
$db = Encore::getModule('DB');

$ids = $db->select('SELECT `page_id` FROM `{pr}pagestree` WHERE `parent` = '.(integer)$root);
if(is_array($ids) && count($ids)>0) {
	$where = array();
	foreach($ids as $id) {
		$where[] = $id['page_id'];
	}
	$pages = $db->select('SELECT `alias`,`updatedon` FROM `{pr}pages` WHERE `published` = 1 AND `id` IN ('.AString::from('')->join($where,',').')');

	if(is_array($pages) && count($pages) > 0 ) {
	//$pages = Encore::getModule('PageManager')->getPagesList($root, ' `{pr}pages`.`published` = 1');

		foreach($pages as $page) {
			$lastmod = AString::from($page['updatedon'])->split(' ');
			$this->setPlaceholder('loc', $up->makeUrl($page['alias'], array()));
			$this->setPlaceholder('lastmod', $lastmod[0]);

			$ret = $ret->append($this->getChunk($args['tpl']));
		}
	}
}
return $ret->append("</urlset>\r\n");
