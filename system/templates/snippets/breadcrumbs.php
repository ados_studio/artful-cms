<?php
/* 
 * сніпет для побудови крошок
 * версія 0.1
 * 
 * ПАРАМЕТРИ СНІПЕТА
 * - обовязкові
 *		tpl			: string	: шаблон в який буде загортатись кожне посилання 
 *
 * - необовязкові
 *		root			: integer	: коренева сторінка, межа відліку 
 *		root_substitute	: integer	: підміна корення сторінкою, корінь повинен бути для неї прямим батьком
 *		root_visible	: boolean	: чи показувати корінь в крошках 
 *		self_tpl		: string	: шаблон в який буде загорнуто поточну сторінку
 *      self_class		: string	: класс який буде передано через плейсхолдер $class для пточної сторінки
 *      parent_class	: string	: класс який буде передано через плейсхолдер $class для батьківских сторінок
*/

if (!isset($args['tpl'])) return '';

$tpl = $args['tpl'];
$self_tpl = (isset($args['self_tpl'])) ? $args['self_tpl'] : null;
$self_class = (isset($args['self_class'])) ? $args['self_class'] : 'active';
$parent_class = (isset($args['parent_class'])) ? $args['parent_class'] : '';
$root = (isset($args['root'])) ? $args['root'] : 0;
$root_substitute = (isset($args['root_substitute'])) ? $args['root_substitute'] : null;
$root_visible = (isset($args['root_visible']) && $args['root_visible'] == '1');
$localize = Encore::getOption('localize');

/** @var $urlparser UrlParser */
$urlparser = Encore::getModule('UrlParser');

/** @var $db DB_MYSQLI */
$db = Encore::getModule('DB');
$page_id = $db->real_escape_string($this->getPlaceholder('id','PAGE'));
$parents = $db->select("SELECT `page_id`,`parent`,`level`,`{pr}pages`.`id`,`alias`,`pagetitle`,`longtitle` FROM `{pr}pagestree`,`{pr}pages`,`{pr}content`" .
	" WHERE `page_id`='$page_id' AND `localize` = '".$localize."' AND `{pr}pagestree`.`parent` = `{pr}pages`.`id` AND `{pr}pages`.`id` = `{pr}content`.`page` ORDER BY `level` DESC");

$root_data = current($parents);
$ret = '';

if ($root_visible) {	
	if ($root_substitute != null) {
		$root_data = $db->select("SELECT `{pr}pages`.`id`,`alias`,`pagetitle`,`longtitle` FROM `{pr}pages`,`{pr}content` WHERE " . 
			"`{pr}pages`.`id` = ".$db->real_escape_string($root_substitute)." AND `{pr}content`.`localize` = '" .$localize. "' AND `{pr}pages`.`id` = `{pr}content`.`page` LIMIT 1");
	} 
		
	if (is_array($root_data)) {
		$root_data = $root_data[0];
		$this->setPlaceholder('title', $root_data['pagetitle']);
		$this->setPlaceholder('longtitle', $root_data['longtitle']);
		$this->setPlaceholder('class', $parent_class); 
		$this->setPlaceholder('id', $root_data['id']);
		$this->setPlaceholder('alias', $root_data['alias']);
		$this->setPlaceholder('link', $urlparser->makeUrl($root_data['alias']));
		$ret .= $this->getChunk($tpl);
	}
}

foreach( $parents as $crumb_item) {
	if ($crumb_item['id'] == $root_data['id'] || $crumb_item['id'] == $root)
		continue;
	$this->setPlaceholder('title', $crumb_item['pagetitle']);
	$this->setPlaceholder('longtitle', $crumb_item['longtitle']);
	$this->setPlaceholder('class', $parent_class); 
	$this->setPlaceholder('id', $crumb_item['id']);
	$this->setPlaceholder('alias', $crumb_item['alias']);
	$this->setPlaceholder('link', $urlparser->makeUrl($crumb_item['alias']));
	$ret .= $this->getChunk($tpl);
}

$this->setPlaceholder('title', $this->getPlaceholder('pagetitle','PAGE'));
$this->setPlaceholder('longtitle', $this->getPlaceholder('longtitle','PAGE'));
$this->setPlaceholder('class', $self_class); 
$this->setPlaceholder('id', $page_id);
$page_alias = $this->getPlaceholder('alias','PAGE');
$this->setPlaceholder('alias', $page_alias);
$this->setPlaceholder('link', $urlparser->makeUrl($page_alias));
$ret .= $this->getChunk(($self_tpl != null) ? $self_tpl : $tpl);

return $ret;