<?php

$ret = '';
$pm = Encore::getModule('PageManager');
$cid = $this->getPlaceholder('id','PAGE');
if($cid != null) {
	$parents = $pm->getPageParents($cid);
	if($parents != null && count($parents) > 0){
		$parents = array_keys($parents);
		if( $parents[0] != 0 ) {
			$parentpage = $pm->getPageById($parents[0]);
			$ret = Encore::getModule('UrlParser')->makeUrl($parentpage['alias']);
		}
	}
}
return $ret;
