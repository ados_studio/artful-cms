<?php
if (!isset($args['value']) || $args['value'] == '' || $args['value'] == '0' || !isset($args['tpl'])) return '';
	
$this->setPlaceholder('value', $args['value']);
return $this->getChunk($args['tpl']);
