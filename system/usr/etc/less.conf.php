<?php

$less_dir = dirname(SYSTEM_PATH).DS.'assets'.DS.'bootstrap'.DS.'less'.DS;
$css_dir = dirname(SYSTEM_PATH).DS.'assets'.DS.'bootstrap'.DS.'css'.DS;
$skins_dir = dirname(SYSTEM_PATH).DS.'assets'.DS.'skins'.DS;

Encore::setOption(
	"COMPILE_LIST",
	array(
			array(
				'source' => $less_dir.'bootstrap.less',
				'destination' => $css_dir.'bootstrap_compiled.css'
				),
			array(
				'source' => $skins_dir.'main'.DS.'less'.DS.'style.less',
				'destination' => $skins_dir.'main'.DS.'css'.DS.'style.css'
				)
	),
	'LESS'
);

