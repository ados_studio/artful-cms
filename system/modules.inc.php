<?php

Encore::setOption('log_blocking',true , 'logdisplay');
Encore::registerModule('logdisplay', 'logDisplay', MODULES_PATH.'logdisplay'.DS.'logdisplay.class.php', true);//

// DataBase driver adapter
Encore::registerModule('DB', 'DB_MYSQLI', MODULES_PATH.'dbmysqli'.DS.'dbmysqli.class.php');

// UserAuth module
Encore::setOption('PARANOID',false,'USERAUTH');
Encore::setOption('MULTIAUTH',true,'USERAUTH');

Encore::registerModule('UserAuth', 'UserAuth', MODULES_PATH.'userauth'.DS.'userauth.class.php', true);

// UrlParser module (Frendly URL)
Encore::registerModule('UrlParser', 'UrlParser', MODULES_PATH.'urlparser'.DS.'urlparser.class.php', true);

// PageManager module
Encore::registerModule('PageManager', 'PageManager', MODULES_PATH.'pagemanager'.DS.'pagemanager.class.php');

// Render module
Encore::setOption('RENDER_PATH',TEMPLATES_PATH.'render'.DS,'RENDER');
Encore::setOption('SOURCE_PATH',TEMPLATES_PATH.'src'.DS,'RENDER');
Encore::setOption('SNIPPETS_PATH',TEMPLATES_PATH.'snippets'.DS,'RENDER');
Encore::setOption('EXECUTABLE_PAGES_PATH', TEMPLATES_PATH.'epages'.DS, 'RENDER');

Encore::registerModule('Render', 'Render', MODULES_PATH.'render'.DS.'render.class.php');

// Mandator module
Encore::registerModule('Mandator', 'Mandator', MODULES_PATH.'mandator'.DS.'mandator.class.php');

// JSTranslator
Encore::registerModule('JSTranslator', 'JSTranslator', MODULES_PATH.'jstranslator'.DS.'jstranslator.class.php', true);

// UserManager
Encore::registerModule('UserManager', 'UserManager', MODULES_PATH.'usermanager'.DS.'usermanager.class.php');

// Messenger
Encore::registerModule('Messenger', 'Messenger', MODULES_PATH.'messenger'.DS.'messenger.class.php');

// FileManager
Encore::setOption('START_PATH',dirname(SYSTEM_PATH).DS.'assets'.DS,'FILEMANAGER');

Encore::registerModule('FileManager', 'FileManager', MODULES_PATH.'filemanager'.DS.'filemanager.class.php', true);

// Scheduler
Encore::setOption('MODE','php','SCHEDULER');

Encore::registerModule('Scheduler','Scheduler', MODULES_PATH.'scheduler'.DS.'scheduler.class.php');

// Less
include_once(SYSTEM_PATH.'usr'.DS.'etc'.DS.'less.conf.php');
Encore::registerModule('Less','Less', MODULES_PATH.'less'.DS.'less.class.php', true);

// Userlogic

//ImageBoard
//Encore::registerModule('Imageboard','Imageboard', MODULES_PATH.'imageboard'.DS.'imageboard.class.php');


