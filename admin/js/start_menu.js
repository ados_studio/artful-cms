Forbiz.start_menu = {	
	btn_in       : { opacity: { to: 1 } },
	btn_out      : { opacity: { to: 0 } },
	menu_in      : { opacity: { to: 1 } },	
	menu_out     : { opacity: { to: 0 }	},
	menu_visible : false,
	lf_engine	 : false,
	init         : function() {
		// animations
		var start_btn = Ext.get("start_btn_hover");
		start_btn.hover( 
			function(){
				var btn_anim = Forbiz.start_menu.btn_in;
				if( isset(btn_anim.anim) && btn_anim.anim.isAnimated() ) {
					btn_anim.anim.stop();
				}
				Ext.get("start_btn_hover").animate(btn_anim, Forbiz.config.desktop.duration);
			}, 
			function(){
				var btn_anim = Forbiz.start_menu.btn_out;
				if( isset(btn_anim.anim) && btn_anim.anim.isAnimated() ) {
					btn_anim.anim.stop();
				}
				Ext.get("start_btn_hover").animate(btn_anim, Forbiz.config.desktop.duration);
			}
		);
		Ext.get('cat1').on('click', Forbiz.start_menu.showFavorites);
		Ext.get('cat2').on('click', Forbiz.start_menu.showApplications);
		Ext.get('cat3').on('click', Forbiz.start_menu.showRecentDocs);
		Forbiz.start_menu.showFavorites();
		
		Ext.get("start_btn_hover").animate( Forbiz.start_menu.btn_out,0.1);
		var start_ct = Ext.get("start_menu_ct");
		start_ct.animate( Forbiz.start_menu.menu_out,0.1);		
		start_ct.blur(Forbiz.start_menu.hide);
		start_ct.on('mouseenter', function() {Forbiz.start_menu.lf_engine = false;});
		start_ct.on('mouseleave', function() {
			Forbiz.start_menu.lf_engine = true;
			Ext.get('start_menu_focus').focus();
		});
		start_btn.on('click', Forbiz.start_menu.show);
		Ext.get('start_menu_focus').on('blur',Forbiz.start_menu.lostfocus);
		Ext.get("start_menu_ct").setStyle({ visibility: 'visible' });
	},
	show         : function() {				
		var menu_visible = Forbiz.start_menu.menu_visible;		
		if( menu_visible ) return;
		var menu_anim = Forbiz.start_menu.menu_in;
		Ext.get("start_menu_ct").setStyle({ visibility: 'visible' });
		Ext.get("start_menu_ct").animate( menu_anim , Forbiz.config.desktop.duration );	
		Forbiz.start_menu.menu_visible = !menu_visible;
		Forbiz.start_menu.lf_engine = true;
		var qinput = Ext.get('start_menu_focus')
		qinput.dom.value = '';
		qinput.focus();
		
	},
	hide         : function() {
		var menu_visible = Forbiz.start_menu.menu_visible;	
		if (!menu_visible ) return;
		var menu_anim = Forbiz.start_menu.menu_out;
		Ext.get("start_menu_ct").animate( menu_anim , Forbiz.config.desktop.duration, function() {
			Forbiz.start_menu.menu_visible = false;
			Ext.get("start_menu_ct").setStyle({ visibility: 'hidden' });
		} );		
	},
	lostfocus    : function() {
		if( Forbiz.start_menu.menu_visible && Forbiz.start_menu.lf_engine ) {
			Forbiz.start_menu.hide();
		}
	},
	rendermenu   : function(menuitems) {
		var html = '';
		for(var i=0; i< menuitems.length; i++) {
			html += '<div class="start_menu_item" onclick=" Forbiz.launch(\'' + menuitems[i].id
			+'\');"><img src="' + menuitems[i].icon
			+'"><span class="title">' + menuitems[i].title
			+'</span><span class="description">' + menuitems[i].description
			+'</span></div>';
		}
		Ext.get('start_menu_items').update(html);
	},
	showFavorites: function() {
		Ext.select("div.start_menu_category").removeClass('active');
		Ext.get('cat1').addClass('active');
		var menuitems = [];
		var items = Forbiz.config.start_menu.favorites;
		for( var i = 0; i < items.length; i++){
			menuitems[menuitems.length] = Forbiz.config.app_registry[items[i]];
			menuitems[menuitems.length-1].id = items[i];
		}
		Forbiz.start_menu.rendermenu(menuitems);
	},
	showApplications: function() {
		Ext.select("div.start_menu_category").removeClass('active');
		Ext.get('cat2').addClass('active');
		var menuitems = [];
		var items = Forbiz.config.start_menu.applications;
		for( var i = 0; i < items.length; i++){
			menuitems[menuitems.length] = Forbiz.config.app_registry[items[i]];
			menuitems[menuitems.length-1].id = items[i];
		}
		Forbiz.start_menu.rendermenu(menuitems);
	},
	showRecentDocs: function() {
		Ext.select("div.start_menu_category").removeClass('active');
		Ext.get('cat3').addClass('active');
		var menuitems = [];
		var items = Forbiz.config.start_menu.recent_documents;
		for( var i = 0; i < items.length; i++){
			menuitems[menuitems.length] = Forbiz.config.app_registry[items[i]];
			menuitems[menuitems.length-1].id = items[i];
		}
		Forbiz.start_menu.rendermenu(menuitems);
	}
}

Forbiz.start_menu.init();

