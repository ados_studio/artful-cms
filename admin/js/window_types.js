Ext.namespace('Forbiz.wintypes');

Forbiz.protowindow = Ext.extend(Ext.Window, {
	width: 800,
	height: 500,
	maximizable: true,
	minimizable: true,
	layout: 'fit',
	renderTo: 'workspace',
	constrain: true,
	//collapsed: true,
	initComponent: function() {
		this.on('destroy', this.win_destroy );
		this.on('activate', this.win_activate );
		this.on('minimize', this.win_minimize );
		Forbiz.protowindow.superclass.initComponent.call(this);

	},
	style: 'top: 100px; left: 100px; visiblaty: visible;',
	win_destroy: function() {
		var id = this.id.split('win').join('');
		Forbiz.winmanager.close(id);
	},
	win_activate: function() {
		var id = this.id.split('win').join('');
		Forbiz.winmanager.setActive(id);
	},
	win_minimize: function() {
		this.hide();
		Forbiz.winmanager.setActiveByZIndex();
	}
});

Forbiz.wintypes.webpage = Ext.extend(Forbiz.protowindow, {
	//layout: 'border',
	initComponent: function() {
		Ext.applyIf(this, {
			items: [
			    {
			    	xtype: 'panel',
			    	//region: 'center',
			    	layout: 'fit',
			    	frame: false,
			    	border: false,
			    	items: [ { html: '<iframe src="'+this.initaction+'" style="width: 100%; height: 100%; border: none;"></iframe>' } ]
			    }
			]
		});

		Forbiz.wintypes.webpage.superclass.initComponent.call(this);

	}
});

Ext.reg('forbiz-window-webpage', Forbiz.wintypes.webpage);

Forbiz.wintypes.mixed = Ext.extend(Forbiz.protowindow, {
	//layout: 'border',
	initComponent: function() {
		Ext.applyIf(this, {
			items: [
			    {
			    	xtype: 'panel',
			    	//region: 'center',
			    	layout: 'fit',
			    	frame: false,
			    	border: false,
			    	items: [ { html: '<iframe src="'+this.initaction+'" style="width: 100%; height: 100%; border: none;"></iframe>' } ]
			    }
			]
		});

		Forbiz.wintypes.mixed.superclass.initComponent.call(this);

	}
});

Ext.reg('forbiz-window-mixed', Forbiz.wintypes.mixed);

Ext.namespace('Forbiz.dlgtypes');

Forbiz.dlgtypes.fileDialog = Ext.extend(Ext.Window, {
	initComponent: function() {
		Ext.applyIf(this, {
			minimizable: false,
			maximizable: true,
			width: 750,
			height: 500,
			modal: true,
			layout: 'border',
			dir: null,
			items: [
				{
					xtype: 'treepanel',
					id: 'fd_dirtree',
					region: 'west',
					useArrows: true,
		    		autoScroll: true,
		 		    animate: true,
		  			border: true,
		    		autoScroll: true,
		    		height: '100%',
		    		width: 250,
		    		split: true,
		    		title: _('Navigation','admin'),
		    		rootVisible: false,
		    		loader: new Ext.tree.TreeLoader({
			    		baseParms: {
				    		action: 'loadnode'
				    	},
				    	dataUrl: '/admin/index.php',
				    	listeners: {
				    		beforeload: function(tl, node){
				    			tl.baseParams.action = 'getuserdirs';
				    		},
				    		load: function(tl, node) {
				    			if(node.id=='[root]') {
				    				node.item(0).select();
				    				node.item(0).expand();
				    				Ext.getCmp('fd_path').setValue(node.item(0).text);

				    				this.refreshFileList();
				    			}
				    		},
				    		scope: this
					    }
			   		}),
				    root: {
				        nodeType: 'async',
				        text: '/',
				        draggable: false,
				    	id: '[root]'
				    },
				    listeners: {
				    	click: function(node, event) {
				    		Ext.getCmp('fd_path').setValue(node.id);
							node.expand();
				    		this.refreshFileList(node);
				    	} ,
				    	scope: this
				    }
				},
				{
					xtype: 'panel',
					region: 'center',
					layout: 'border',
					//height: '100%',
					items: [
						{
							xtype: 'panel',
							region: 'north',
							autoHeight: true,
							items: [
								{
									xtype: 'form',
									border: false,
									frame: true,
									fileUpload: true,
									layout: 'hbox',
									id: 'fd_upload_form',
									margins: {top:0, right:0, bottom:0, left:0},
									items: [
										{
											xtype: 'field',
											inputType: 'file',
											id: 'fd_file_upload_field',
											name: 'upfile'
										},
										{
											xtype: 'button',
											icon: '/admin/img/icons/16/document-save.png',
											text: _('Upload File','admin'),
											handler: function() {
												Ext.getCmp('fd_win').disable();
												var form = Ext.getCmp('fd_upload_form').getForm();

												if( typeof(form.el.dom.upfile.files[0]) != "undefined" && form.el.dom.upfile.files[0].name.length > 0 ) {
													form.submit({
														waitMsg: _('Waiting...','admin'),
														waitTitle: _('Upload File','admin'),
														params: {
															action: "uploadfile",
															path: Ext.get('fd_path').dom.value
														},
														success: function(form, action) {
															Ext.getCmp('fd_win').enable();
															var result = action.result;
															if(result.success == true) {
																Ext.getCmp('fd_win').refreshFileList();
																Ext.Msg.alert('Success', 'File uploaded successfully.');
																Ext.getCmp('fd_upload_form').getForm().reset();
															} else {
																Ext.Msg.alert('Failure', result.message);
															}

														},
														failure: function(form, action) {
															Ext.Msg.alert('Failure', 'Ups :(! Something wrong.');
															Ext.getCmp('fd_win').enable();
														}
													});
												} else {
													Ext.getCmp('fd_win').enable();
													Ext.Msg.alert('Warning', 'File not found');
												}
											}
										}
									]
								},
								{
									xtype: 'textfield',
									width: '100%',
									id: 'fd_path',
									readOnly: true
								}
							]
						},
						{
							xtype: 'container',
							region: 'center',
							layout: 'fit',
							border: true,
							frame: false,
							items: [
								{
									border: true,
									frame: false,
									html: '<div id="fd_filelist" class="fdv_viewtable" style="width: 100%; height: 100%; background: #fff; overflow: auto;"></div>'
								}
							]
						},
						{
							xtype: 'container',
							region: 'south',
							layout: 'border',
							height: 62,
							defaults: { margins: '5 5 5 0' },
							items: [
								{
									xtype: 'form',
									layout: 'form',
									bodyStyle: 'background: transparent;',
									region: 'center',
									height: 52,
									border: false,
									frame: false,
									width: '100%',
									labelWidth: 1,
									items: [
										{

											xtype: 'textfield',
											name: 'fileName',
											id: 'fd_filename',
											anchor: '100%'
										}/*,
										{
											name: 'filterType',
											id: 'fd_filterType',
											xtype: 'combo',
											anchor: 'right -30%'
										}*/
									]
								},
								{
									xtype: 'container',
									region: 'east',
									layout: 'vbox',
									width: 90,
									defaults: { margins: '0 0 5 0' },
									items: [
										{
											xtype:'button',
											text: _('Ok','admin'),
											width: 90,
											icon: '/admin/img/icons/16/dialog-ok-apply.png',
											handler: function() {
												Ext.getCmp('fd_win').close();
											}
										},
										{
											xtype:'button',
											text: _('Cancel','admin'),
											width: 90,
											icon: '/admin/img/icons/16/dialog-cancel.png',
											handler: function() {
												Ext.getCmp('fd_win').close();
											}
										}
									]
								}
							]
						}
					],
					tbar: [
						{
							icon: '/admin/img/icons/16/view-refresh.png',
							text: _('Refresh','admin'),
							handler: function() {
								this.refreshFileList() ;
							},
							scope: this
						},
						{
							icon: '/admin/img/icons/16/go-parent-folder.png',
							text: _('Parent Folder','admin'),
							handler: function() {
								var selmodel = Ext.getCmp('fd_dirtree').getSelectionModel();
								var parent = selmodel.getSelectedNode().parentNode;
								if( typeof(parent) != "undefined" && parent != null && parent.id != '[root]') {
									selmodel.select(parent);
									this.refreshFileList(parent);
								}
							},
							scope: this
						},'->',
						{
							icon: '/admin/img/icons/16/view-list-icons.png',
							text: _('Grid','admin'),
							enableToggle: true,
							pressed: true,
							toggleGroup: 'fd_view',
							toggleHandler: function(b,s) {
								if(s) {
									Ext.get('fd_filelist').removeClass('fdv_viewlist');
									Ext.get('fd_filelist').addClass('fdv_viewtable');
								}
							}
						},
						{
							icon: '/admin/img/icons/16/view-list-details.png',
							text: _('List','admin'),
							enableToggle: true,
							toggleGroup: 'fd_view',
							toggleHandler: function(b,s) {
								if(s) {
									Ext.get('fd_filelist').removeClass('fdv_viewtable');
									Ext.get('fd_filelist').addClass('fdv_viewlist');
								}
							}
						}
					]

				}
			]
		});

		Forbiz.dlgtypes.fileDialog.superclass.initComponent.call(this);


	},
	refreshFileList: function(node) {
		var view = Ext.get('fd_filelist');
		view.addClass('load');
		view.dom.innerHTML = '';
		var nodepath = ''
		if(typeof(node)=="undefined") {
			nodepath = Ext.getCmp('fd_dirtree').getSelectionModel().getSelectedNode().id;
		} else {
			nodepath = node.id;
		}


		Ext.Ajax.request({
			url: '/admin/index.php',
			method: 'POST',
			params: {
				action: 'getuserfiles',
				path: nodepath
			},
			callback: function(success, response, request ) {
				var view = Ext.get('fd_filelist');
				if(success) {
					var ret = request.responseText;
					var htmlfiles = '';
					var htmldirs = '';
					if(typeof(ret) == 'undefined') {
						alert(_('failure'));
					} else {
						var files = Ext.decode(ret);
						for(var i=0; i<files.length; i++) {
							var file = files[i]
							var classes = 'fileitem '+file.mime;
							var src = '/admin/img/icons/mime/'+file.mime+'.png';
							if(file.mime == 'image') {
								 src = file.link;
							}
							var fitem = '<div id="'+file.md5+'" class="'+classes+'" onclick="Ext.getCmp(\'fd_win\').selectFile(\''+file.md5+'\',\''+file.text+'\'); Forbiz.dialog.data.fileName=\''+file.link+'\';" ondblclick="Ext.getCmp(\'fd_win\').changeFile(\''+file.text+'\');"><img src="'+src+'"/><span>'+file.text+'</span></div>';
							if (file.mime=='folder') {
								htmldirs += fitem;
							} else {
								htmlfiles += fitem;
							}
						}
						view.dom.innerHTML = htmldirs+htmlfiles;
					}
				} else {
					alert(_('failure'));
				}
				view.removeClass('load');
			}
		});
	},
	selectFile: function(id, filename) {
		Ext.getCmp('fd_filename').setValue(filename);
		Ext.select('#fd_filelist div.fileitem').removeClass('active');
		Ext.get(id).addClass('active');
	},
	changeFile: function(filename) {
		var node = Ext.getCmp('fd_dirtree').getSelectionModel().getSelectedNode();
		node = node.findChild('text',filename);
		if(node != null) {
			node.expand();
			Ext.getCmp('fd_dirtree').getSelectionModel().select(node);
			this.refreshFileList(node);
			Ext.getCmp('fd_path').setValue(node.id);
		}
	}
});

Ext.reg('forbiz-dialog-file',Forbiz.dlgtypes.fileDialog);

Forbiz.dialog = {
	data: {
		fileName: null
	},
	openFile: function(parent,title,filename,mask,callback){
		Forbiz.dialog.data.fileName = filename;

		var dlg = new Forbiz.dlgtypes.fileDialog({
			title: title,
			mask: mask,
			id: 'fd_win',
		});
		if(typeof(callback)=="function") {
			dlg.on('close', callback);
		}
		dlg.show();
	}
}



