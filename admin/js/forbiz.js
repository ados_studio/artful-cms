function isset(arg) {
	return (typeof(arg)!='undefined');
};

Forbiz = {
    // ATRIBUTES
    config: {},
    loadlist: null,


    // METHODS
    construct: function() {
    	this.loadlist = [
    	    {
    	        id: 'extjs',
    	        title: 'ExtJS Framework...',
    	        link: 'ext/ext-all.js',
    	        icon: 'img/icons/ext-js.png'
    	    },
    	    {
    	        id: 'systemtray',
    	        title: 'Services...',
    	        link: 'js/systemtray.js',
    	        icon: 'img/icons/services.png'
    	    },
    	    {
    	        id: 'windows',
    	        title: 'Window System...',
    	        link: 'js/window_types.js',
				icon: 'img/icons/windows.png'
    	    },
    	    {
    	        id: 'start_menu',
    	        title: 'Start Menu...',
    	        link: 'js/start_menu.js',
    	        icon: 'img/icons/menu.png'
    	    },
    	    {
    	        id: 'winmanager',
    	        title: 'Task Manager...',
    	        link: 'js/winmanager.js',
    	        icon: 'img/icons/winmanager.png'
    	    }
    	];

    	if(typeof(Forbiz.config.desktop)!='undefined') {
    		var ct = Ext.get('login_form_container');
            ct.child('form').animate(
            	{
            		opacity: {to: 0, from: 1},
            		marginTop: {to: -25, from: 0}
            	},
            	0.35
            );
    		Forbiz.load();
    	}
    },

    start: function(){
        var frm = Ext.get('forbiz_login_form').dom;
        var ct = Ext.get('login_form_container');
        ct.child('form').animate(
        	{
        		opacity: {to: 0, from: 1},
        		marginTop: {to: -25, from: 0}
        	},
        	0.35
        );

        Ext.Ajax.request( {
           url: '/admin/index.php',          // <-------- login and config URL
           params: {
               login: frm.login.value,
               password: frm.password.value
           },
           method: 'POST',
           callback: function(success, response, request) {
        	   var fal = true;
               if (response) {
                   Forbiz.config = Ext.decode(request.responseText);
                   if(Forbiz.config.status == 'success') {
                	   Forbiz.config = Forbiz.config.result;
                	   Forbiz.load();
                	   fal = false;
                   }
               }
               if(fal) {
            	   var ct = Ext.get('login_form_container');
            	   var dh = Ext.DomHelper;


            	   alert('Помилка авторизації!');

                   ct.child('form').animate(
                       	{
                       		opacity: {to: 1, from: 0},
                       		marginTop: {to: 0, from: 25}
                       	},
                       	0.35
                   );
               }
           }

        });
    },

    load: function () {
        var ct = Ext.get('login_form_container');
        var dh = Ext.DomHelper;
        if(typeof(Forbiz.config.desktop.wallpaper)!="undefined") {
        	dh.append(Ext.get('wallpaper'), { tag: 'img', style: 'opacity: 0;', src: Forbiz.config.desktop.wallpaper, id: 'user_wall'} );
        }
        dh.append(ct, [ { tag: 'br'},{ tag: 'br'},{ tag: 'span', id: 'load_text'} ] );
        for(var i=0;i<Forbiz.loadlist.length;i++) {
        	dh.append(ct,{ tag: 'div', cls: 'load_detector_cont', id: 'load_'+Forbiz.loadlist[i].id+'_cont'} );
        }
        if(this.config)
        Forbiz.moduleloader();
    },

    moduleloader: function() {
        if(Forbiz.loadlist.length<=0) {
        	setTimeout(Forbiz.render,1000);
        	return;
        }
        var module = Forbiz.loadlist.shift();
        var dh = Ext.DomHelper;
        dh.append(Ext.get('load_'+module.id+'_cont'), { tag: 'img', src: module.icon});
        var icon = Ext.get('load_'+module.id+'_cont').child('img');
        Ext.get('load_text').update(module.title);
        icon.animate(
        	{
        		width: {to: 32, from: 0 },
        		height: {to: 32, from: 0 },
        		opacity: {to: 1, from: 0 },
        		marginTop: {to: 20, from: 80 },
        		marginLeft: {to: 4, from: 16 }
        	},
        	Forbiz.config.desktop.duration*2);
        Forbiz.include(module.link,Forbiz.moduleloader);
    },

    include: function(link,callback) {
    	Ext.Ajax.request({
    		url: link,
    		callback: function(success, response, request) {
    			if (response) {
    				eval(request.responseText);
    				callback.call();
    			} else {
    				Ext.Msg.show( {
             		    title: 'Помилка',
             		    msg: 'Модуль не завантажено!', //Ext.decode(request.responseText).message;
             		    buttons: Ext.Msg.OK,
             		    modal: true,
             		    animEl: Ext.get('forbiz_login'),
             		    fn: function() {
             		    	window.location.href = unescape(window.location.pathname);
             		    }
             	    });
                }
    		}
    	});
    },
    render: function() {
    	Ext.get('forbiz_login').animate({
    		opacity: {to: 0, from: 1}
    	},Forbiz.config.desktop.duration);
    	Ext.get('user_wall').animate({
    		opacity: {to: 1, from: 0}
    	},Forbiz.config.desktop.duration*2);
    	Ext.get('viewport').animate({
    	    opacity: {to: 1, from: 0}
    	},Forbiz.config.desktop.duration)


    },
    launch: function(appid){
    	Forbiz.start_menu.hide();
    	var appconf = Forbiz.config.app_registry[appid];
    	if(isset(appid)) {
    		Forbiz.winmanager.exec(appconf);
    	} else {
    		alert('Error! Unknown application!');
    	}
    }

};

Ext.onReady( function() {
    Forbiz.construct();
});