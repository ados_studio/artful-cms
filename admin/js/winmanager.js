Forbiz.winmanager = {
	winincrement: 1,
	windows: {},
	exec   : function (appconf) {
		if ( appconf.type != "webpage" && appconf.type != 'mixed' ) return;
		if ( isset(appconf.single) && appconf.single ) {
			var winid = Forbiz.winmanager.getWindowIdByAppId(appconf.id);
			if( winid!=null ) {
				Forbiz.winmanager.show(winid);
				return;
			}
		}
		var xt = 'forbiz-window-' + appconf.type;
		Forbiz.winmanager.winincrement++;
		var title = '<table><tr><td><img src="'+appconf.icon+'" width="16" height="16"/> </td><td class="window_title"> '+appconf.title+'</td></tr></table>';
		Forbiz.winmanager.windows[Forbiz.winmanager.winincrement] = {
			win: Ext.create({
				xtype: xt,
				id: 'win'+ Forbiz.winmanager.winincrement,
				title: title,
				initaction: appconf.action,
				mode: appconf.mode,
				width: (typeof(appconf.width)!='undefined') ? appconf.width : 800,
				height:(typeof(appconf.height)!='undefined') ? appconf.height : 600,
				maximized: (typeof(appconf.maximized)!='undefined') ? appconf.maximized : false
			}),
			id: Forbiz.winmanager.winincrement,
			config: appconf,
			visible: false
		};
		Forbiz.winmanager.renderPanel();
		Forbiz.winmanager.show(Forbiz.winmanager.winincrement);
	},
	renderPanel : function () {
		var html = '';
		for( var i in Forbiz.winmanager.windows) {
			if (Forbiz.winmanager.windows[i]==null) continue;
			html += '<div class="win_btn" id="winbtn_'+i
			     +'" onclick=" Forbiz.winmanager.show(\''+i
			     +'\');"><img src="'+Forbiz.winmanager.windows[i].config.icon
			     +'" width="32" height="32"/></div>';
		}
		Ext.get('win_manager').update(html);
	},
	setActive: function(winid) {
		if(isset(Forbiz.winmanager.windows[winid])) {
			Ext.select('.win_btn').removeClass('active');
			if (Forbiz.winmanager.windows[winid].win.getEl().getStyle('visibility')=='visible') {
				Ext.get('winbtn_'+winid).addClass('active');
			}
		}
	},
	show: function(winid) {
		if(isset(Forbiz.winmanager.windows[winid])) {
			Forbiz.winmanager.windows[winid].win.show();
			Forbiz.winmanager.setActive(winid);
		}
	},
	close: function(winid) {
		if(isset(Forbiz.winmanager.windows[winid])) {
			if(typeof(Forbiz.winmanager.windows[winid].win)!='undefined'
				    && Forbiz.winmanager.windows[winid]!=null
				    && Forbiz.winmanager.windows[winid].win.destroyed) {
				//Forbiz.winmanager.windows[winid].win.close();
				Forbiz.winmanager.windows[winid].win.destroy();
			}
			Forbiz.winmanager.windows[winid] = null;
			delete Forbiz.winmanager.windows[winid];
		}
		Forbiz.winmanager.renderPanel();
		Forbiz.winmanager.setActiveByZIndex();
	},
	setActiveByZIndex: function(){
		var winid = null;
		var maxzindex = -1;
		for( var i in Forbiz.winmanager.windows) {
			if (Forbiz.winmanager.windows[i]==null) continue;
			if (Forbiz.winmanager.windows[i].win.getEl().getStyle('z-index') > maxzindex
					&& Forbiz.winmanager.windows[i].win.getEl().getStyle('visibility') == 'visible') {
				winid = i;
				maxzindex = Forbiz.winmanager.windows[i].win.getEl().getStyle('z-index');
			}
		}
		Ext.select('.win_btn').removeClass('active');
		if(winid != null ) Forbiz.winmanager.setActive(winid);
	},
	getWindowIdByAppId: function(appid) {
		var winid = null;
		for( var i in Forbiz.winmanager.windows) {
			if (Forbiz.winmanager.windows[i]==null) continue;
			if (Forbiz.winmanager.windows[i].config.id == appid) {
				winid = i;
			}
		}
		return winid;
	}
}