Ext.namespace('Forbiz.widgets.logout');

Forbiz.widgets.logout = {
		btn_in       : { opacity: { to: 1 } },
		btn_out      : { opacity: { to: 0 } },
		init         : function() {
			var logout_btn = Ext.get('logout_btn_hover');
			logout_btn.update('<form id="logout_form" method="POST"><input type="hidden" name="logout"/></form>');
			logout_btn.animate( Forbiz.widgets.logout.btn_out, 0.01);
			logout_btn.on('click',Forbiz.widgets.logout.run );
			logout_btn.hover(
				function(){
					var btn_anim = Forbiz.start_menu.btn_in;
					if( isset(btn_anim.anim) && btn_anim.anim.isAnimated() ) {
						btn_anim.anim.stop();
					}
					Ext.get("logout_btn_hover").animate(btn_anim, Forbiz.config.desktop.duration);
				},
				function(){
					var btn_anim = Forbiz.start_menu.btn_out;
					if( isset(btn_anim.anim) && btn_anim.anim.isAnimated() ) {
						btn_anim.anim.stop();
					}
					Ext.get("logout_btn_hover").animate(btn_anim, Forbiz.config.desktop.duration);
				}
			);
			Ext.get('login_name').update(Forbiz.config.desktop.username);
		},
		run          : function() {
			Ext.get('logout_form').dom.submit();
		}
		
};

Forbiz.widgets.logout.init();