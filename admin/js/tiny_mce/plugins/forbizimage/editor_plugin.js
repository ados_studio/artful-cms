
(function() {
	tinymce.create('tinymce.plugins.ForbizImage', {
		init: function(ed, url) {
			var t = this;
			t.editor = ed;
			
			ed.addCommand('mceInsertForbizImage',function(){
				var dlg = (typeof(window.parent)=="undefined") ? Forbiz.dialog : window.parent.Forbiz.dialog ;
				dlg.data.editor = ed;
				dlg.openFile(null,'TinyMCE Forbiz Image',null,null, function(){
					var dlg = (typeof(window.parent)=="undefined") ? Forbiz.dialog : window.parent.Forbiz.dialog ;
					var str = '<img src="'+dlg.data.fileName+'"/>' 
					dlg.data.editor.execCommand('mceInsertContent', false, str);
				});				
				
			});
			
			ed.addButton('forbizimage', {title : 'Forbiz Image Dialog', cmd : 'mceInsertForbizImage'});
		},
		getInfo: function() {
			return {
				longname : 'Insert image by Forbiz dialog',
				author : 'Chvyl Andriy [Forbiz]',
				authorurl : 'http://4biz.te.ua',
				infourl : 'http://4biz.te.ua',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
		// Private methods
	});
	tinymce.PluginManager.add('forbizimage', tinymce.plugins.ForbizImage);
})();