/****
 * Original codeprotect by Tijmen Schep, Holland, 9-10-2005
 * Updated for Tinymce 3.x by Greg Smith, UK, 19-02-2008
 *
 * Support for JSP added by David Quinn-Jacobs, 6-3-2009.
 *    Recommended tinyMCE settings to support taglib and other special tags (e.g., <fo:include/>, <my:customTag/>:
 *             valid_elements: '*:*[*|*|*]'
 ****/

(function() {
          
    // Load plugin specific language pack
    //tinymce.PluginManager.requireLangPack('codeprotect');

    tinymce.create('tinymce.plugins.CodeprotectPlugin', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
            
            ed.onBeforeSetContent.add(function(ed, o) {
                o.content = o.content.replace(/<\?/gi, "<!--PHPCODE");
                o.content = o.content.replace(/\?>/gi, "PHPCODE-->");

                o.content = o.content.replace(/<\%/gi, "<!--JSPCODE");
                o.content = o.content.replace(/\%>/gi, "JSPCODE-->");
                
                //Fixes URL Encoding---
                //code protect html source fix
                o.content = o.content.replace(/<\?/gi, "&lt;!--PHPCODE");
                o.content = o.content.replace(/\?>/gi, "PHPCODE--&gt;");

                o.content = o.content.replace(/<\%/gi, "&lt;!--JSPCODE");
                o.content = o.content.replace(/\%>/gi, "JSPCODE--&gt;");

                //firefox fix
                o.content = o.content.replace(/&amp;quot;mceNonEditable&amp;quot;/gi, "mceNonEditable");
                //url encoding fix
                o.content = o.content.replace(/'/gi, "'");
                o.content = o.content.replace(/&quot;/gi, '"');
                //End Fixes URL Encoding---
                
                
            });
           
            ed.onPostProcess.add(function(ed, o) {
                if (o.get) {
                    o.content = o.content.replace(/<!--PHPCODE/gi, "<?");
                    o.content = o.content.replace(/PHPCODE-->/gi, "?>");

                    o.content = o.content.replace(/<!--JSPCODE/gi, "<%");
                    o.content = o.content.replace(/JSPCODE-->/gi, "%>");
                    
                    //Fixes URL Encoding---
                    //code protect html source fix
                    o.content = o.content.replace(/&lt;!--PHPCODE/gi, "<?");
                    o.content = o.content.replace(/PHPCODE--&gt;/gi, "?>");
                    o.content = o.content.replace(/&lt;\?/gi, "<?");
                    o.content = o.content.replace(/\?&gt;/gi, "?>");

                    o.content = o.content.replace(/&lt;!--JSPCODE/gi, "<%");
                    o.content = o.content.replace(/JSPCODE--&gt;/gi, "%>");
                    o.content = o.content.replace(/&lt;\%/gi, "<%");
                    o.content = o.content.replace(/\%&gt;/gi, "%>");

                    //firefox javascript mceNonEditable insert fix
                    o.content = o.content.replace(/&amp;quot;mceNonEditable&amp;quot;/gi, "mceNonEditable");
                    //url encoding fix
                    o.content = o.content.replace(/'/gi, "'");
                    o.content = o.content.replace(/&quot;/gi, '"');
                    //End Fixes URL Encoding---
                    
                    
                }
            });
           
        },

        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                longname : 'CodeProtect plugin',
                author : 'Greg Smith (Updated from original by Tijmen Schep)',
                authorurl : 'http://www.hotpebble.com',
                infourl : '',
                version : "1.1"
            };
        }
                
    });

    // Register plugin
    tinymce.PluginManager.add('codeprotect', tinymce.plugins.CodeprotectPlugin);
    
})();