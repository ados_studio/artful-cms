ABOUT

This simple plugin protects any code in the html you are editing.
It turns start- and end-tag into comment tags when the editor is started.

<? becomes <!--CODE
?> becomes CODE-->

And changes the tags back to normal when you save your html.

* Updated for Tinymce 3.x by Greg Smith, UK, 19-02-2008
*
* Support for JSP added by David Quinn-Jacobs, 6-3-2009.


INSTALL

To install, copy the codeprotect folder to the plugins directory 
>> \data\tweaks\tinymce\jscripts\tiny_mce\plugins

Now you just add the word "codeprotect" to the list of plugins in the "init" bit of your tinyMCE.php 
>> \data\tweaks\tinyMCE.php

Like this:
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "codeprotect,style,layer,table,save,...