<?php
$php_os = (strtoupper(substr(PHP_OS,0,3))==='WIN');
if(!defined('PATH_SEPARATOR')) define('PATH_SEPARATOR',($php_os)?';':':');
if(!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR',($php_os)?'\\':'/');
if(!defined('DIR_SEPARATOR')) define('DIR_SEPARATOR',DIRECTORY_SEPARATOR);
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
unset ($php_os);

require_once( dirname(dirname(__FILE__)).DS.'system'.DS.'config.inc.php' );
require_once( dirname(dirname(__FILE__)).DS.'system'.DS.'admin'.DS.'config.inc.php' );

Encore::registerModule('init', 'initAdminModule', MODULES_PATH.'initadmin'.DS.'initadmin.class.php');

Encore::init();


