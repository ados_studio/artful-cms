useradmin = {
	data: {
		uv_flag: true,
		uv_update: false
	},
	api: {
  		pageUsers: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'userslist'
				}
			}); 
		},
		pageGroups: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'groupslist'
				}
			}); 
		},
		welcomePage: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'welcomepage'
				}
			}); 
		},
		createUser: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'usereditor',
					id: 0
				}
			}); 
		},
		createGroup: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'groupeditor',
					id: 0
				}
			}); 
		},
		editUser: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'usereditor',
					id:  Ext.get('pagetypes').dom.value
				}
			});
		},
		editGroup: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'groupeditor',
					id:  Ext.get('pagetypes').dom.value
				}
			});
		},
		saveGroup: function() {
			Ext.Ajax.request({
				url: '/admin/applications/useradmin/index.php',
				method: 'POST',
				params: {
					action: 'savegroup',
					id: Ext.get('input_id').dom.value,
					name: Ext.get('input_groupname').dom.value,
					schema: Ext.get('schema').dom.value,
					privileges: Ext.get('privileges').dom.value
				},
				callback: function(success, response, request ) {
					if(success) {
						var ret = request.responseText;
						if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
							alert('failure');
						}
					} else {
						alert('failure');
					}
					useradmin.api.pageGroups();
				}
			});
		},
		saveUser: function() {
			Ext.Ajax.request({
				url: '/admin/applications/useradmin/index.php',
				method: 'POST',
				params: {
					action: 'saveuser',
					id: Ext.get('input_id').dom.value,
					login: Ext.get('input_login').dom.value,
					name: Ext.get('input_name').dom.value,
					email: Ext.get('input_email').dom.value,
					groups: Ext.encode ( useradmin.api.getOptionsArray( Ext.get('input_groups').dom.options ) ),
					vars: useradmin.api.parseUserVars(),
					privileges: Ext.get('user_privileges').dom.value
				},
				callback: function(success, response, request ) {
					var pass = false;
					if(success) {
						var ret = request.responseText;
						if(typeof(ret) == 'undefined') {
							var res = Ext.decode(ret);
							if(ret.success != true) {
								alert(res.result);
							} else {
								console.log(res);
							}
						}
					} else {
						alert('failure');
					}
					useradmin.api.pageUsers();
				}
			});
		},
		deleteUser: function() {
			Ext.Ajax.request({
				url: '/admin/applications/useradmin/index.php',
				method: 'POST',
				params: {
					action: 'deleteuser',
					id: Ext.get('pagetypes').dom.value
				},
				callback: function(success, response, request ) {
					if(success) {
						var ret = request.responseText;
						if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
							alert('failure');
						}
					} else {
						alert('failure');
					}
					useradmin.api.pageUsers();
				}
			});
		},
		deleteGroup: function() {
			Ext.Ajax.request({
				url: '/admin/applications/useradmin/index.php',
				method: 'POST',
				params: {
					action: 'deletegroup',
					id: Ext.get('pagetypes').dom.value
				},
				callback: function(success, response, request ) {
					if(success) {
						var ret = request.responseText;
						if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
							alert('failure');
						}
					} else {
						alert('failure');
					}
					useradmin.api.pageGroups();
				}
			});
		},
		changePassword: function() {
			Ext.get('viewport').load({
				url: '/admin/applications/useradmin/index.php',
				params: {
					action: 'getpage',
					page: 'password',
					id:  Ext.get('pagetypes').dom.value
				}
			});
		},
		savePassword: function() {
			if(Ext.get('userpassword').dom.value != Ext.get('userpasswordprotect').dom.value) {
				Ext.get('userpassword').dom.value = '';
				Ext.get('userpasswordprotect').dom.value = '';
				alert('Паролі не співпадають, повторіть спробу.');
				return;
			}
			Ext.Ajax.request({
				url: '/admin/applications/useradmin/index.php',
				method: 'POST',
				params: {
					action: 'savepassword',
					userid: Ext.get('userid').dom.value,
					userpassword: Ext.get('userpassword').dom.value,
					useremail: Ext.get('emailmessage').dom.checked
				},
				callback: function(success, response, request ) {
					if(success) {
						var ret = request.responseText;
						if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
							alert('failure');
						}
					} else {
						alert('failure');
					}
					useradmin.api.pageUsers();
				}
			});
		},
		updateUserVars: function() {
			if ( useradmin.data.uv_flag ) {
				Ext.get('uservars_ct').load({
					url: '/admin/applications/useradmin/index.php',
					params: {
						action: 'getuservarsform',						
						id:  Ext.get('input_id').dom.value,
						groups: Ext.encode( useradmin.api.getOptionsArray( Ext.get('input_groups').dom.options ) )
					}
				});
			} else {
				useradmin.data.uv_update = true;
			}
		},
		getOptionsArray: function(ops) {
			var ret = new Array();
			for(var i=0; i< ops.length; i++) {
				if (ops[i].selected || ops[i].checked)	ret.push(ops[i].value);				
			}			
			return ret;
		},
		parseUserVars: function() {
			var ret = {};
			var els = Ext.select('#uservars_ct .default').elements;				
			for (var i = 0; i< els.length; i++) {				
				if (els[i].id != '') {					
					ret[els[i].id.substr(10,els[i].id.length-10)] = els[i].value;
				}				
			}			
			return Ext.encode(ret);
		}
	}
}






Ext.onReady( function() {
	
});