

Ext.QuickTips.init();

Ext.namespace('Forbiz.app.forbizfilemanager');

Forbiz.app.forbizfilemanager = {
	api: {
		
	},
	data: {
		
	}
}


Ext.onReady( function() {
	Ext.create({
		xtype: 'container',
		layout: 'border',
		renderTo: 'viewport',
		items: [
			{
				xtype: 'treepanel',
				region: 'west',
				useArrows: true,
	    		autoScroll: true,
	 		    animate: true,
	  			border: true,
	    		autoScroll: true,
	    		height: '100%' ,
	    		width: 250,
	    		split: true,
	    		title: _('Navigation','admin'),
	    		rootVisible: false,
	    		loader: new Ext.tree.TreeLoader({
		    		baseParms: {
			    		action: 'loadnode'
			    	},
			    	dataUrl: '/admin/index.php',
			    	listeners: {
			    		beforeload: function(tl, node){
			    			tl.baseParams.action = 'getuserdirs';
			    		},
			    		load: function(tl, node) {
			    			if(node.id=='[root]') {
			    				node.item(0).select();	
			    				node.item(0).expand();
			    						
			    				
			    				this.refreshFileList();
			    			} 
			    		},				    		
			    		scope: this
				    }
		   		}),		    
			    root: {
			        nodeType: 'async',
			        text: '/',
			        draggable: false,
			    	id: '[root]'
			    },
			    listeners: {
			    	click: function(node, event) {
			    					
			    	} ,
			    	scope: this
			    }
			},{
				xtype: 'container'
			}
		]
		
	});
});