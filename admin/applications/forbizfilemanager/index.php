<?php

$php_os = (strtoupper(substr(PHP_OS,0,3))==='WIN');
if(!defined('PATH_SEPARATOR')) define('PATH_SEPARATOR',($php_os)?';':':');
if(!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR',($php_os)?'\\':'/');
if(!defined('DIR_SEPARATOR')) define('DIR_SEPARATOR',DIRECTORY_SEPARATOR);
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
unset ($php_os);

define('APP_PATH', dirname(__FILE__).DS);

$cms_path = dirname(dirname(dirname(dirname(__FILE__)))).DS.'system'.DS;
require_once( $cms_path.'config.inc.php' );
require_once( $cms_path.'admin'.DS.'config.inc.php' );

Encore::registerModule('init', 'ForbizFileManager', $cms_path.'admin'.DS.'applications'.DS.'forbizfilemanager'.DS.'forbizfilemanager.class.php');

Encore::init();
