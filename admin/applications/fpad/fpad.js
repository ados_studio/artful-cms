Ext.namespace('Forbiz.fpad');

Forbiz.fpad = {
	data: {
		opened: false,
		saved: true,
		editor: null		
	},
	api: {
		init: function() {			
			
		},
		loadFile: function() {
			window.parent.Forbiz.dialog.openFile(null,_('Open File'),Forbiz.fpad.data.filename, null, function() {
				Forbiz.fpad.data.filename = window.parent.Forbiz.dialog.data.fileName;
				Ext.get('filename_text').dom.innerHTML = ' '+Forbiz.fpad.data.filename+' ';
				Ext.Ajax.request( {
					url: '/admin/applications/fpad/index.php',
					method: 'POST',
					params: {
						action: 'loadfile',
						filename: Forbiz.fpad.data.filename,						
					},
					callback: function(success, response, request ) {
						if(success) {
							var ret = request.responseText;								
							if(typeof(ret) == 'undefined' || ret=='{"success":false}') {					
								alert('failure');
								Ext.get('filename_text').dom.innerHTML = '';
							} else {
								Ext.getCmp('content').setValue(ret);		
								Forbiz.fpad.data.opened = true;						
							}				
						} else {
							alert('failure');
							Ext.get('filename_text').dom.innerHTML = '';
						}
					}
				});
			} );			
		},
		saveFile: function() {
			Ext.getCmp('ctv').disabled = true;
			Ext.Ajax.request( {
				url: '/admin/applications/fpad/index.php',
				method: 'POST',
				params: {
					action: 'savefile',
					filename: Forbiz.fpad.data.filename,	
					content: Ext.getCmp('content').getValue()
				},
				callback: function(success, response, request ){
					if(success) {
						var ret = request.responseText;								
						if(typeof(ret) == 'undefined' || ret == '{"success":false}') {
							alert('failure');
						} else {
							Ext.getCmp('ctv').disabled = false;	
							Forbiz.fpad.data.saved = true;
							Ext.getCmp('menuSave').setDisabled(true);
							Ext.getCmp('tbtnSave').setDisabled(true);							
						}	
					} else {
						alert('failure');
					}					
				}
			} );
		},
		changeContent: function() {
			Forbiz.fpad.data.saved = false;
			Ext.getCmp('menuSave').setDisabled(false);
			Ext.getCmp('tbtnSave').setDisabled(false);
		}
	}	
}

Ext.onReady( function() {
	Ext.create({
		xtype: 'container',		
		id: 'ctv',
		layout: 'border',
		width: '100%',
		height: '100%',
		border: true,
		frame: true,
		renderTo: 'viewport',		
		items: [
			{
				xtype: 'panel',
				layout: 'hbox',
				region: 'north',
				height: 1,
				tbar: [
					{
						xtype: 'tbbutton',
						text: _('File'),
						defaults: { xtype: 'menuitem'},
						menu: [
							/*{
								text: _('New'),
								icon: '/admin/img/icons/16/document-new.png'
							},*/
							{
								text: _('Open file ...'),
								icon: '/admin/img/icons/16/document-open.png',
								handler: Forbiz.fpad.api.loadFile
							},
							{								
								text: _('Save file'),
								id: 'menuSave',
								disabled: true,
								icon: '/admin/img/icons/16/document-save.png',
								handler: Forbiz.fpad.api.saveFile
							},'-',							
							{
								text: _('Exit'),
								handler: function() {
									
								}
							}
						]
					},
					{
						xtype: 'tbbutton',
						text: _('Edit'),
						defaults: { xtype: 'menuitem'},
						menu: [
							{
								text: _('Cut'),
								icon: '/admin/img/icons/16/edit-cut.png'
							},
							{
								text: _('Copy'),
								icon: '/admin/img/icons/16/edit-copy.png'
							},
							{
								text: _('Paste'),
								icon: '/admin/img/icons/16/edit-paste.png'
							},
							{
								text: _('Delete'),
								icon: '/admin/img/icons/16/edit-delete.png',
								handler: function() { 
									Forbiz.fpad.data.editor.execComand('inserthtml',false,'');
								}
							},'-',
							{
								text: _('Select All'),
								icon: '/admin/img/icons/16/edit-select-all.png',
								handler: function() {
									Ext.getCmp('content').selectText();
								}
							}							
						]
					},'->',{						
						html: ' <div id="filename_text"></div> '						
					}
				],				
				bbar: [					
					/*{
						xtype: 'tbbutton',
						icon: '/admin/img/icons/16/document-new.png'
					},*/
					{
						xtype: 'tbbutton',
						icon: '/admin/img/icons/16/document-open.png',
						handler: Forbiz.fpad.api.loadFile	
					},
					{
						id: 'tbtnSave',
						xtype: 'tbbutton',
						icon: '/admin/img/icons/16/document-save.png',
						disabled: true,
						handler: Forbiz.fpad.api.saveFile						
					},'-',
					{
						xtype: 'tbbutton',
						icon: '/admin/img/icons/16/edit-cut.png'
					},
					{
						xtype: 'tbbutton',
						icon: '/admin/img/icons/16/edit-copy.png'
					},
					{
						xtype: 'tbbutton',
						icon: '/admin/img/icons/16/edit-paste.png'
					}
				]
			},
			{
				xtype: 'panel',
				region: 'center',
				layout: 'fit',			
				items: [
					{
						xtype: 'textarea',
						id: 'content',
						width: '100%',
						height: '100%',
						enableKeyEvents: true,
						listeners: {
							keyup: Forbiz.fpad.api.changeContent,
							change: Forbiz.fpad.api.changeContent
						}
					}
				]				
			}			
		]
	});
	window.onresize = function() {
		var ctv = Ext.getCmp('ctv');
		console.log(window.innerWidth)
		ctv.setWidth( window.innerWidth);
		ctv.setHeight( window.innerHeight);		
	};
	Forbiz.fpad.api.init();
});