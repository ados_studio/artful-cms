<?php

$php_os = (strtoupper(substr(PHP_OS,0,3))==='WIN');
if(!defined('PATH_SEPARATOR')) define('PATH_SEPARATOR',($php_os)?';':':');
if(!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR',($php_os)?'\\':'/');
if(!defined('DIR_SEPARATOR')) define('DIR_SEPARATOR',DIRECTORY_SEPARATOR);
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
unset ($php_os);

define('APP_PATH',  $_SERVER['DOCUMENT_ROOT'].DS.$_SERVER['PHP_SELF'] );

$cms_path = $_SERVER['DOCUMENT_ROOT'].DS.'system'.DS;

require_once( $cms_path.'config.inc.php' );
require_once( $cms_path.'admin'.DS.'config.inc.php' );

Encore::registerModule('init', 'FPad', $cms_path.'admin'.DS.'applications'.DS.'fpad'.DS.'fpad.class.php');

Encore::init();
