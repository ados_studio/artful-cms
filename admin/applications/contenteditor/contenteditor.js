/**
 *
 */
Ext.QuickTips.init();

Ext.namespace('Forbiz.app.contenteditor.classes.doctree');

Forbiz.app.contenteditor.classes.doctree = Ext.extend(Ext.tree.TreePanel,{
	initComponent: function(config) {
		this.config = config | {};
		Ext.applyIf(this,{
			useArrows: true,
		    autoScroll: true,
		    animate: true,
		    border: false,
		    autoScroll: true,
		    loader: new Ext.tree.TreeLoader({
		    	baseParms: {
			    	action: 'loadnode'
			    },
			    dataUrl: '/admin/applications/contenteditor/index.php',
			    listeners: {
			    	beforeload: function(tl, node){
			    		tl.baseParams.action = 'getchilds';
			    	},
			    	scope: this
			    }
		    }),
		    root: {
		        nodeType: 'async',
		        text: _('Pages','admin')+' <b>[0]</b>',
		        draggable: false,
		        id: '0'
		    }

		});
		Forbiz.app.contenteditor.classes.doctree.superclass.initComponent.call(this,config);
	}
});

Ext.reg('doc-tree',Forbiz.app.contenteditor.classes.doctree);

Forbiz.app.contenteditor.api = {
	initTinyMCEEditor: function() {
		tinyMCE.init({
						theme : "advanced",
						//language:"ua",
						mode: "specific_textareas",
        				plugins : "codeprotect,autolink,lists,pagebreak,style,layer,table,advhr,forbizimage,advlink,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist",
        				content_css: "/assets/skins/editor.css",
                        editor_selector :"rich_html_editor",

						theme_advanced_buttons1 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,forbizimage,cleanup,help,code,|,undo,redo",
						theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage,|,insertdate,inserttime,preview,|,sub,sup,|,styleprops,spellchecker",

       					theme_advanced_toolbar_location : "top",
       					theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
        				skin : "o2k7",
						skin_variant : "silver",
        				width: "100%",

        				//apply_source_formatting:false,
        				apply_source_formatting: true,
        				preformatted: true

					});
	},
	welcomePage: function(){
		var doctr = Ext.getCmp('cmpdoctree');
		doctr.setDisabled(false);
		var rootn = doctr.getRootNode();
		rootn.expand();
		rootn.select();
		Ext.get('ctcontent').load({
			url: '/admin/applications/contenteditor/index.php',
			//scripts: true,
			params: {
				page: 'welcome'
			}
		});
	},
	switchLocalize: function() {
		var localize = Ext.get('input_localize').dom.value;
		Forbiz.app.contenteditor.api.editPage(localize);
	},
	createPage: function(){
		var doctr = Ext.getCmp('cmpdoctree');

		var parent_node = doctr.getSelectionModel().getSelectedNode();
		if(parent_node.isLeaf()) {
			alert(_('ALERT_NODE_IS_LEAF','admin'));
		} else {
			doctr.setDisabled(true);
			Ext.get('ctcontent').load({
				url: '/admin/applications/contenteditor/index.php',
				//scripts: true,
				params: {
					page: 'editor',
					parent: parent_node.id,
					id: 0
				},
				callback : function(el, success, responce, options) {
					Forbiz.app.contenteditor.api.initTinyMCEEditor();
				}
			});
		}
	},
	editPage: function(localize) {

		if(typeof(localize)!="string") {
			localize = JSTranslator.default_localize;
		}
		var doctr = Ext.getCmp('cmpdoctree');

		var node = doctr.getSelectionModel().getSelectedNode();
		if(node.id==0)return;
		doctr.setDisabled(true);
		Ext.get('ctcontent').load({
			url: '/admin/applications/contenteditor/index.php',
			//scripts: true,
			params: {
				page: 'editor',
				parent: node.parentNode.id,
				localize: localize,
				id: node.id
			},
			callback : function(el, success, responce, options) {
				Forbiz.app.contenteditor.api.initTinyMCEEditor();
			}
		});
	},
	savePage: function(){
		var dform = Ext.get('editor_form').dom;

		tinyMCE.triggerSave();

		Ext.Ajax.request({
			url: '/admin/applications/contenteditor/index.php',
			method: 'POST',
			params: {
				action: 'save',
				id: Ext.get('input_id').dom.value,
				'parent': Ext.get('input_parent').dom.value,
				localize: Ext.get('input_localize').dom.value,
				alias: Ext.get('input_alias').dom.value,
				pagetitle: Ext.get('input_pagetitle').dom.value,
				longtitle: Ext.get('input_longtitle').dom.value,
				seotitle: Ext.get('input_seotitle').dom.value,
				description: Ext.get('input_description').dom.value,
				keys: Ext.get('input_keys').dom.value,
				'type': Ext.get('input_type').dom.value,
				published: Ext.get('input_published').dom.checked,
				folder: Ext.get('input_folder').dom.checked,
				accessable: Ext.get('input_accessable').dom.checked,
				executable: Ext.get('input_executable').dom.checked,
				preview: dform.preview.value,
				content: dform.content.value,
				sort: dform.sort.value,
				vars: Ext.get('input_pagevars').dom.value
			},
			callback: function(success, response, request ) {
				if(success) {
					var ret = request.responseText;
					if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
						alert('failure');
					}
				} else {
					alert('failure');
				}
				window.location.href = '/admin/applications/contenteditor/index.php';

			}

		});
	},
	deletePage: function() {
		if(confirm('Ви справді бажаєте зробити видалення?')) {
			var doctr = Ext.getCmp('cmpdoctree');
			var node = doctr.getSelectionModel().getSelectedNode();
			if(node.id==0) return;
			doctr.setDisabled(true);
			Ext.Ajax.request({
				url: '/admin/applications/contenteditor/index.php',
				method: 'POST',
				params: {
					action: 'delete',
					id: node.id
				},
				callback: function(success, response, request ) {
					if(success) {
						var ret = request.responseText;
						if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
							alert(_('failure'));
						}
					} else {
						alert(_('failure'));
					}
					window.location.href = '/admin/applications/contenteditor/index.php';
				}
			});
		}
	},
	pageTypes: function(){
		var doctr = Ext.getCmp('cmpdoctree');
		doctr.setDisabled(false);
		var rootn = doctr.getRootNode();
		rootn.expand();
		rootn.select();
		Ext.get('ctcontent').load({
			url: '/admin/applications/contenteditor/index.php',
			//scripts: true,
			params: {
				page: 'types'
			}
		});
	},
	createType: function() {
		Ext.get('ctcontent').load({
			url: '/admin/applications/contenteditor/index.php',
			//scripts: true,
			params: {
				page: 'edittype',
				id: 0
			}
		});
	},
	editType: function() {
		Ext.get('ctcontent').load({
			url: '/admin/applications/contenteditor/index.php',
			scripts: true,
			params: {
				page: 'edittype',
				id: Ext.get('pagetypes').dom.value
			}
		});
	},
	saveType: function() {
		Ext.Ajax.request({
			url: '/admin/applications/contenteditor/index.php',
			method: 'POST',
			params: {
				action: 'saveType',
				id: Ext.get('input_id').dom.value,
				'name' : Ext.get('name').dom.value,
				'template' : Ext.get('template').dom.value,
				'schema' : Ext.get('schema').dom.value
			},
			callback: function(success, response, request ) {
				if(success) {
					var ret = request.responseText;
					if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
						alert(_('failure'));
					}
				} else {
					alert(_('failure'));
				}

				Forbiz.app.contenteditor.api.pageTypes();
			}

		});
	},
	deleteType: function() {
		if(confirm('Ви справді бажаєте зробити видалення?')) {

			Ext.Ajax.request({
				url: '/admin/applications/contenteditor/index.php',
				method: 'POST',
				params: {
					action: 'deleteType',
					id: Ext.get('pagetypes').dom.value
				},
			callback: function(success, response, request ) {
				if(success) {
					var ret = request.responseText;
					if(typeof(ret) == 'undefined' || ret!='{"success":true}') {
						alert(_('failure'));
					}
				} else {
					alert(_('failure'));
				}
				Forbiz.app.contenteditor.api.pageTypes();
			}
		});
		}
	},
	updateVars: function() {
		Ext.get('ctpagevars').load({
			url: '/admin/applications/contenteditor/index.php',
			//scripts: true,
			params: {
				page: 'pageVars',
				id: Ext.get('input_id').dom.value,
				localize: Ext.get('input_localize').dom.value,
				'type': Ext.get('input_type').dom.value
			}
		});
	},
	parseVars: function(){
		Forbiz.app.contenteditor.vars = {};
		Ext.select('#ctpagevars .default').each(function(el,c,idx) {
			if(el.id!='') {
				Forbiz.app.contenteditor.vars[el.id.substr(9,el.id.length-9)] = el.dom.value;
			}
		});

		Ext.get('input_pagevars').dom.value = Ext.encode(Forbiz.app.contenteditor.vars);
	}
}



Ext.onReady( function() {
	Ext.create({
		xtype: 'panel',
		renderTo: 'ctdoctree',
		style: 'background: #fff; height: 100%;',
		border: false,
		frame: false,
		items: [
		    {
		    	xtype: 'doc-tree',
		    	id: 'cmpdoctree'
		    }
		],
		tbar: [
		    {
		    	text: _('Create','admin'),
		    	icon: '/admin/img/icons/16/docnew.png',
		    	handler: Forbiz.app.contenteditor.api.createPage
		    },'-',
		    {
		    	text: _('Edit','admin'),
		    	icon: '/admin/img/icons/16/docedit.png',
		    	handler: Forbiz.app.contenteditor.api.editPage
		    },'->',
		    {
		    	text: _('Delete','admin'),
		    	icon: '/admin/img/icons/16/docdel.png',
		    	handler: Forbiz.app.contenteditor.api.deletePage
		    }
		]
	});

	var rootn = Ext.getCmp('cmpdoctree').getRootNode();
	rootn.expand();
	rootn.select();
});
