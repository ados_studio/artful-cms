<?php
if(!isset($_GET['dict'])) return;

$php_os = (strtoupper(substr(PHP_OS,0,3))==='WIN');
if(!defined('PATH_SEPARATOR')) define('PATH_SEPARATOR',($php_os)?';':':');
if(!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR',($php_os)?'\\':'/');
if(!defined('DIR_SEPARATOR')) define('DIR_SEPARATOR',DIRECTORY_SEPARATOR);
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
unset ($php_os);

require_once( dirname(__FILE__).DS.'system'.DS.'config.inc.php' );

$adict = AString::from($_GET['dict'])->split('_');
$dict_array = array();
foreach ($adict as $ditem) {
	$dvals = AString::from($ditem)->split('-');
	$dvals[0] = AString::from($dvals[0])->replace('..', '')->replace(DS, '')->toString();
	$dvals[1] = AString::from($dvals[1])->replace('..', '')->replace(DS, '')->toString();
	//Encore::initDictionary($dvals[0],$dvals[1]);
	if(!isset($dict_array[$dvals[0]])) {
		$dict_array[$dvals[0]] = array();
	}
	$dict_array[$dvals[0]][] = '"'.$dvals[1].'":'.file_get_contents(Encore::getOption('localize_path') . $dvals[0] .DS. $dvals[1].'.json');
}
$dict_json = array();
foreach($dict_array as $context=>$jdata) {
	$dict_json[] = '"'.$context.'": {'. AString::from('')->join($jdata,',').'}';
}

$dict_json = '{'.AString::from('')->join($dict_json,',').'}';

?>

JSTranslator = {
	dict: <?php echo($dict_json);?>,
	localize: '<?php echo(Encore::getOption('localize'));?>'
};

function _( val, context, lang) {
	if ( typeof(context) == 'undefined' || context == null ) {
		context = 'core';
	}
	if ( typeof(lang) == 'undefined' || lang == null ) {
		lang = JSTranslator.localize;
	}
	var ret = val;
	if ( typeof(JSTranslator.dict[context]) != 'undefined'
			&&  typeof(JSTranslator.dict[context][lang]) != 'undefined'
			&& typeof(JSTranslator.dict[context][lang][val]) != 'undefined' ) {

		ret = JSTranslator.dict[context][lang][val];
	}
	return ret;
}


